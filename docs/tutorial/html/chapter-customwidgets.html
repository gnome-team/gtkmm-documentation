<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="highlight.min.css">
<script src="highlight.min.js"></script><script>
      hljs.configure({languages: ['cpp']});
      hljs.highlightAll();
    </script><title>Chapter 28. Custom Widgets</title>
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="Programming with gtkmm 4">
<link rel="up" href="index.html" title="Programming with gtkmm 4">
<link rel="prev" href="sec-i18n-getting-help-with-translations.html" title="Getting help with translations">
<link rel="next" href="sec-custom-widgets.html" title="Custom Widgets">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<div class="navheader">
<table width="100%" summary="Navigation header">
<tr><th colspan="3" align="center">Chapter 28. Custom Widgets</th></tr>
<tr>
<td width="20%" align="left">
<a accesskey="p" href="sec-i18n-getting-help-with-translations.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<th width="60%" align="center"> </th>
<td width="20%" align="right"> <a accesskey="n" href="sec-custom-widgets.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
</table>
<hr>
</div>
<div class="chapter">
<div class="titlepage"><div><div><h1 class="title">
<a name="chapter-customwidgets"></a>Chapter 28. Custom Widgets</h1></div></div></div>
<div class="toc">
<p><b>Table of Contents</b></p>
<ul class="toc">
<li><span class="section"><a href="chapter-customwidgets.html#sec-custom-containers">Custom Containers</a></span></li>
<li><span class="section"><a href="sec-custom-widgets.html">Custom Widgets</a></span></li>
<li><span class="section"><a href="sec-custom-css-names.html">Custom CSS Names</a></span></li>
</ul>
</div>


    <p><span class="application">gtkmm</span> makes it very easy to derive new widgets by inheriting from an
      existing widget class, either by deriving from a container and adding child
      widgets, or by deriving from a single-item widget, and changing its behavior.
      But you might occasionally find that no suitable starting point already exists.
      In this case, you can implement a widget from scratch.</p>

    <div class="section">
<div class="titlepage"><div><div><h2 class="title" style="clear: both">
<a name="sec-custom-containers"></a>Custom Containers</h2></div></div></div>
    

    <p>When deriving a custom container widget directly from <code class="classname">Gtk::Widget</code>,
      you should override the following virtual methods:
    </p>
<div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; ">
<li class="listitem"><p><code class="methodname">get_request_mode_vfunc()</code>: Return what <code class="literal">Gtk::SizeRequestMode</code> is preferred by the container.</p></li>
<li class="listitem"><p><code class="methodname">measure_vfunc()</code>: Calculate the minimum and natural width or height of the container.</p></li>
<li class="listitem"><p><code class="methodname">size_allocate_vfunc()</code>: Position the child widgets, given the height and width that the container has actually been given.</p></li>
</ul></div>
<p>
    </p>

    <p>The <code class="methodname">get_request_mode_vfunc()</code>,
        <code class="methodname">measure_vfunc()</code>, and
        <code class="methodname">size_allocate_vfunc()</code> virtual methods control the
        layout of the child widgets. For instance, if your container has 2
        child widgets, with one below the other, your
        <code class="methodname">get_request_mode_vfunc()</code> might request
        height-for-width layout. Then your
        <code class="methodname">measure_vfunc()</code>
        might report the maximum of the widths of the child widgets when asked
        to report width, and it might report the sum of their heights when asked
        to report height. If you want padding between
        the child widgets then you would add that to the width and height too.
        Your widget's container will use this result to ensure that your widget
        gets enough space, and not less. By examining each widget's parent, and
        its parent, this logic will eventually decide the size of the top-level
        window.</p>

    <p>You are not guaranteed to get the <code class="literal">Gtk::SizeRequestMode</code>
        that you request. Therefore <code class="methodname">measure_vfunc()</code> must
        return sensible values for all reasonable values of its input parameters.
        For a description of <code class="methodname">measure_vfunc()</code>'s parameters see
        also the description of <code class="methodname">Gtk::Widget::measure()</code>, which
        may be better documented than <code class="methodname">measure_vfunc()</code>.</p>

   <p><code class="methodname">size_allocate_vfunc()</code> receives the actual
       height and width that the parent container has decided to give to your
       widget. This might be more than the minimum, or even more than the natural
       size, for instance if the
       top-level window has been expanded. You might choose to ignore the extra
       space and leave a blank area, or you might choose to expand your child
       widgets to fill the space, or you might choose to expand the padding
       between your widgets. It's your container, so you decide.</p>

   <p>Your container must unparent its children before the underlying C object
       (a <code class="classname">gtkmm__GtkWidget</code>) is finalized. If your container
       is used as a managed widget, it shall unparent its children in a
       <code class="methodname">Gtk::Widget::signal_destroy()</code> handler (available
       since <span class="application">gtkmm</span> 4.8). If your container is not managed, that signal handler
       is not called. Instead the children shall be unparented in the C++ destructor.
       If you want your container to be useful both ways, unparent the children
       both in the destructor and in a signal handler. See the example code.</p>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="custom-container-example"></a>Example</h3></div></div></div>


    <p>This example implements a container with child widgets, one above
        the other. Of course, in this case it would be far simpler just to use
        a vertical <code class="classname">Gtk::Box</code> or <code class="classname">Gtk::Grid</code>.</p>

<div class="figure">
<a name="figure-custom-container"></a><p class="title"><b>Figure 28.1. Custom Container</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/custom_container.png" alt="Custom Container"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/custom/custom_container/" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;
#include "mycontainer.h"

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  ~ExampleWindow() override;

protected:
  //Signal handlers:
  void on_button_quit();

  //Child widgets:
  Gtk::Box m_VBox;
  MyContainer m_MyContainer;
  Gtk::Button m_Button_Child;
  Gtk::Label m_Label_Child;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">mycontainer.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_CUSTOM_CONTAINER_MYCONTAINER_H
#define GTKMM_CUSTOM_CONTAINER_MYCONTAINER_H

#include &lt;gtkmm/widget.h&gt;
#include &lt;gtkmm/version.h&gt;

#define HAS_SIGNAL_DESTROY GTKMM_CHECK_VERSION(4,7,1)

class MyContainer : public Gtk::Widget
{
public:
  MyContainer();
  ~MyContainer() override;

  void append(Gtk::Widget&amp; child);
  void prepend(Gtk::Widget&amp; child);
  void remove(Gtk::Widget&amp; child);

protected:
  int get_nvis_children() const;

  //Overrides:
  Gtk::SizeRequestMode get_request_mode_vfunc() const override;
  void measure_vfunc(Gtk::Orientation orientation, int for_size, int&amp; minimum, int&amp; natural,
    int&amp; minimum_baseline, int&amp; natural_baseline) const override;
  void size_allocate_vfunc(int width, int height, int baseline) override;

#if HAS_SIGNAL_DESTROY
  // Signal handler:
  void on_container_destroy();
#endif
};

#endif //GTKMM_CUSTOM_CONTAINER_MYCONTAINER_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_VBox(Gtk::Orientation::VERTICAL),
  m_Button_Child("Child button"),
  m_Label_Child("Child label", Gtk::Align::END, Gtk::Align::CENTER),
  m_Button_Quit("Quit")
{
  set_title("Custom Container example");
  set_default_size(400, 200);

  m_VBox.set_margin(6);
  set_child(m_VBox);

  //Add the child widgets to the custom container:
  m_MyContainer.append(m_Button_Child);
  m_MyContainer.append(m_Label_Child);
  m_MyContainer.prepend(*Gtk::make_managed&lt;Gtk::Label&gt;(
    "First line\nSecond line\nThird line"));
  m_MyContainer.set_expand();
  m_VBox.append(m_MyContainer);

#if HAS_SIGNAL_DESTROY
  // A managed custom container.
  auto container = Gtk::make_managed&lt;MyContainer&gt;();
  container-&gt;prepend(*Gtk::make_managed&lt;Gtk::Label&gt;("Second custom container"));
  container-&gt;set_expand();
  m_VBox.append(*container);
#endif
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(6);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect( sigc::mem_fun(*this,
              &amp;ExampleWindow::on_button_quit) );
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
  set_visible(false);
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>
<p>File: <code class="filename">mycontainer.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "mycontainer.h"

#include &lt;algorithm&gt; // std::max()

// This example container is a simplified vertical Box.
//
// It can't be used as a managed widget, managed by another container
// unless Gtk::Widget::signal_destroy() exists.
// It would cause an error like
// Gtk-WARNING **: 08:31:48.137: Finalizing gtkmm__GtkWidget 0x561b777462c0, but it still has children left:

MyContainer::MyContainer()
{
#if HAS_SIGNAL_DESTROY
  signal_destroy().connect(sigc::mem_fun(*this, &amp;MyContainer::on_container_destroy));
#endif
}

MyContainer::~MyContainer()
{
  // If MyContainer is a managed widget, the underlying C object is destructed
  // before this C++ destructor is executed.
  if (!gobj())
    return;

  // If MyContainer is not a managed widget, unparent all children here.
  while (Widget* child = get_first_child())
    child-&gt;unparent();
}

#if HAS_SIGNAL_DESTROY
// This signal handler is called only if MyContainer is a managed widget.
void MyContainer::on_container_destroy()
{
  while (Widget* child = get_first_child())
    child-&gt;unparent();
}
#endif

// Get number of visible children.
int MyContainer::get_nvis_children() const
{
  int nvis_children = 0;
  for (const Widget* child = get_first_child(); child; child = child-&gt;get_next_sibling())
    if (child-&gt;get_visible())
      ++nvis_children;

  return nvis_children;
}

Gtk::SizeRequestMode MyContainer::get_request_mode_vfunc() const
{
  return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
}

// Discover the total amount of minimum space and natural space needed by
// this container and its children.
void MyContainer::measure_vfunc(Gtk::Orientation orientation, int for_size,
  int&amp; minimum, int&amp; natural, int&amp; minimum_baseline, int&amp; natural_baseline) const
{
  // Don't use baseline alignment.
  minimum_baseline = -1;
  natural_baseline = -1;

  minimum = 0;
  natural = 0;

  // Number of visible children.
  const int nvis_children = get_nvis_children();

  if (orientation == Gtk::Orientation::HORIZONTAL)
  {
    // Divide the height equally among the visible children.
    if (for_size &gt; 0 &amp;&amp; nvis_children &gt; 0)
      for_size /= nvis_children;

    // Request a width equal to the width of the widest visible child.
  }

  for (const Widget* child = get_first_child(); child; child = child-&gt;get_next_sibling())
    if (child-&gt;get_visible())
    {
      int child_minimum, child_natural, ignore;
      child-&gt;measure(orientation, for_size, child_minimum, child_natural, ignore, ignore);
      minimum = std::max(minimum, child_minimum);
      natural = std::max(natural, child_natural);
    }

  if (orientation == Gtk::Orientation::VERTICAL)
  {
    // The allocated height will be divided equally among the visible children.
    // Request a height equal to the number of visible children times the height
    // of the highest child.
    minimum *= nvis_children;
    natural *= nvis_children;
  }
}

void MyContainer::size_allocate_vfunc(int width, int height, int baseline)
{
  //Do something with the space that we have actually been given:
  //(We will not be given heights or widths less than we have requested, though
  //we might get more.)

  // Number of visible children.
  const int nvis_children = get_nvis_children();

  if (nvis_children &lt;= 0)
  {
    // No visible child.
    return;
  }

  //Assign space to the children:
  Gtk::Allocation child_allocation;
  const int height_per_child = height / nvis_children;

  //Place the first visible child at the top-left:
  child_allocation.set_x(0);
  child_allocation.set_y(0);

  //Make it take up the full width available:
  child_allocation.set_width(width);
  child_allocation.set_height(height_per_child);

  //Divide the height equally among the visible children.
  for (Widget* child = get_first_child(); child; child = child-&gt;get_next_sibling())
    if (child-&gt;get_visible())
    {
      child-&gt;size_allocate(child_allocation, baseline);
      child_allocation.set_y(child_allocation.get_y() + height_per_child);
    }
}

void MyContainer::append(Gtk::Widget&amp; child)
{
   child.insert_at_end(*this);
}

void MyContainer::prepend(Gtk::Widget&amp; child)
{
   child.insert_at_start(*this);
}

void MyContainer::remove(Gtk::Widget&amp; child)
{
  child.unparent();
}
</code></pre>

</div>

    </div>

    



</div>
<div class="navfooter">
<hr>
<table width="100%" summary="Navigation footer">
<tr>
<td width="40%" align="left">
<a accesskey="p" href="sec-i18n-getting-help-with-translations.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<td width="20%" align="center"> </td>
<td width="40%" align="right"> <a accesskey="n" href="sec-custom-widgets.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">Getting help with translations </td>
<td width="20%" align="center"><a accesskey="h" href="index.html"><img src="icons/home.png" alt="Home"></a></td>
<td width="40%" align="right" valign="top"> Custom Widgets</td>
</tr>
</table>
</div>
</body>
</html>
