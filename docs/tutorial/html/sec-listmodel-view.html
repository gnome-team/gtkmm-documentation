<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="highlight.min.css">
<script src="highlight.min.js"></script><script>
      hljs.configure({languages: ['cpp']});
      hljs.highlightAll();
    </script><title>The View</title>
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="Programming with gtkmm 4">
<link rel="up" href="chapter-listmodel.html" title="Chapter 10. ListView, GridView, ColumnView">
<link rel="prev" href="sec-listmodel-factory.html" title="The Factory">
<link rel="next" href="sec-listmodel-sorting.html" title="Sorting">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<div class="navheader">
<table width="100%" summary="Navigation header">
<tr><th colspan="3" align="center">The View</th></tr>
<tr>
<td width="20%" align="left">
<a accesskey="p" href="sec-listmodel-factory.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<th width="60%" align="center">Chapter 10. ListView, GridView, ColumnView</th>
<td width="20%" align="right"> <a accesskey="n" href="sec-listmodel-sorting.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
</table>
<hr>
</div>
<div class="section">
<div class="titlepage"><div><div><h2 class="title" style="clear: both">
<a name="sec-listmodel-view"></a>The View</h2></div></div></div>


<p>
The View is the widget that displays the model data and allows the user to interact
with it. The View can show all of the model's columns, or just some, and it can show
them in various ways.
</p>

<p>
An important requirement for views (especially views of long lists) is that they need
to know which items are not visible so they can be recycled. Views achieve that by
implementing the <code class="classname">Scrollable</code> interface and expecting
to be placed directly into a <code class="classname">ScrolledWindow</code>.
</p>

<p>
There are different view widgets to choose from.
</p>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-listmodel-listview"></a>ListView</h3></div></div></div>


<p>
The <code class="classname">ListView</code> shows a 1-dimensional list with one column.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1ListView.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="listmodel-listview-example"></a>Example</h4></div></div></div>


<div class="figure">
<a name="figure-listmodel-listview"></a><p class="title"><b>Figure 10.1. ListView</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/listmodel_listview.png" alt="ListView"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/listmodelviews/list_listview" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  ~ExampleWindow() override;

protected:
  // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_name(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::Label m_Heading;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ListView m_ListView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr&lt;Gtk::StringList&gt; m_StringList;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_VBox(Gtk::Orientation::VERTICAL),
  m_Heading("&lt;b&gt;Name&lt;/b&gt;", Gtk::Align::START),
  m_Button_Quit("Quit")
{
  set_title("Gtk::ListView (Gtk::StringList) example");
  set_default_size(300, 200);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ListView, inside a ScrolledWindow, with the heading above
  // and the button underneath.
  m_Heading.set_use_markup();
  m_VBox.append(m_Heading);
  m_ScrolledWindow.set_child(m_ListView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_button_quit));

  // Create the list model:
  m_StringList = Gtk::StringList::create({"Billy Bob", "Joey Jojo", "Rob McRoberts"});

  // Set list model and selection model.
  auto selection_model = Gtk::SingleSelection::create(m_StringList);
  selection_model-&gt;set_autoselect(false);
  selection_model-&gt;set_can_unselect(true);
  m_ListView.set_model(selection_model);
  m_ListView.add_css_class("data-table"); // high density table

  // Add the factory for the ListView's single column.
  auto factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_setup_label));
  factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_name));
  m_ListView.set_factory(factory);
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
  set_visible(false);
}

void ExampleWindow::on_setup_label(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  list_item-&gt;set_child(*Gtk::make_managed&lt;Gtk::Label&gt;("", Gtk::Align::START));
}

void ExampleWindow::on_bind_name(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto pos = list_item-&gt;get_position();
  if (pos == GTK_INVALID_LIST_POSITION)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(list_item-&gt;get_child());
  if (!label)
    return;
  label-&gt;set_text(m_StringList-&gt;get_string(pos));
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char* argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  // Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>


</div>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-listmodel-gridview"></a>GridView</h3></div></div></div>


<p>
The <code class="classname">GridView</code> shows a 2-dimensional grid.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1GridView.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="listmodel-gridview-example"></a>Example</h4></div></div></div>


<div class="figure">
<a name="figure-listmodel-gridview"></a><p class="title"><b>Figure 10.2. GridView</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/listmodel_gridview.png" alt="GridView"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/listmodelviews/gridview" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;
#include &lt;gdkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  ~ExampleWindow() override;

protected:
  class ModelColumns;
  // Signal handlers:
  void on_button_quit();
  void on_item_activated(unsigned int position);
  void on_selection_changed();
  void on_setup_listitem(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_listitem(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  int on_model_sort(const Glib::RefPtr&lt;const ModelColumns&gt;&amp; a, const Glib::RefPtr&lt;const ModelColumns&gt;&amp; b);

  void add_entry(const std::string&amp; filename, const Glib::ustring&amp; description);

  // A Gio::ListStore stores filename, description and texture.
  class ModelColumns : public Glib::Object
  {
  public:
    std::string m_filename;
    Glib::ustring  m_description;
    Glib::RefPtr&lt;Gdk::Texture&gt; m_texture;

    static Glib::RefPtr&lt;ModelColumns&gt; create(const std::string&amp; filename,
      const Glib::ustring&amp; description, const Glib::RefPtr&lt;Gdk::Texture&gt;&amp; texture)
    {
      return Glib::make_refptr_for_instance&lt;ModelColumns&gt;(
        new ModelColumns(filename, description, texture));
    }

  protected:
    ModelColumns(const std::string&amp; filename, const Glib::ustring&amp; description,
      const Glib::RefPtr&lt;Gdk::Texture&gt;&amp; texture)
    : m_filename(filename), m_description(description), m_texture(texture)
    { }
  }; // ModelColumns

  Glib::RefPtr&lt;Gio::ListStore&lt;ModelColumns&gt;&gt; m_data_model;
  Glib::RefPtr&lt;Gtk::SingleSelection&gt; m_selection_model;
  Glib::RefPtr&lt;Gtk::SignalListItemFactory&gt; m_factory;

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::GridView m_GridView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;array&gt;
#include &lt;iostream&gt;

namespace
{
  struct GridEntry
  {
    std::string m_filename;
    Glib::ustring m_description;
  };

  std::array&lt;GridEntry, 8&gt; entries =
  {
    GridEntry{"mozilla-firefox.png", "&lt;b&gt;Mozilla Firefox&lt;/b&gt; Logo"},
    GridEntry{"xmms.xpm",            "&lt;b&gt;XMMS&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-1.svg",    "&lt;b&gt;Gnome Dice 1&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-2.svg",    "&lt;b&gt;Gnome Dice 2&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-3.svg",    "&lt;b&gt;Gnome Dice 3&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-4.svg",    "&lt;b&gt;Gnome Dice 4&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-5.svg",    "&lt;b&gt;Gnome Dice 5&lt;/b&gt; Logo"},
    GridEntry{"gnome-dice-6.svg",    "&lt;b&gt;Gnome Dice 6&lt;/b&gt; Logo"}
  };

} // anonymous namespace

ExampleWindow::ExampleWindow()
: m_VBox(Gtk::Orientation::VERTICAL),
  m_Button_Quit("Quit")
{
  set_title("Gtk::GridView (Gio::ListStore) example");
  set_default_size(400, 400);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the GridView inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_GridView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(6);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_button_quit));

  // Create the data model:
  m_data_model = Gio::ListStore&lt;ModelColumns&gt;::create();
  m_selection_model = Gtk::SingleSelection::create(m_data_model);
  m_selection_model-&gt;set_autoselect(false);

  m_factory = Gtk::SignalListItemFactory::create();
  m_factory-&gt;signal_setup().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_setup_listitem));
  m_factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_listitem));

  // Fill the Gio::ListStore's data model and sort it.
  for (const auto&amp; entry : entries)
    add_entry(entry.m_filename, entry.m_description);
  m_data_model-&gt;sort(sigc::mem_fun(*this, &amp;ExampleWindow::on_model_sort));

  m_GridView.set_model(m_selection_model);
  m_GridView.set_factory(m_factory);

  m_GridView.signal_activate().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_item_activated));
  m_selection_model-&gt;property_selected().signal_changed().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_selection_changed));
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
  set_visible(false);
}

void ExampleWindow::on_setup_listitem(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  // Each ListItem contains a vertical Box with an Image and a Label.
  auto vBox = Gtk::make_managed&lt;Gtk::Box&gt;(Gtk::Orientation::VERTICAL);
  auto picture = Gtk::make_managed&lt;Gtk::Picture&gt;();
  picture-&gt;set_can_shrink(false);
  picture-&gt;set_halign(Gtk::Align::CENTER);
  picture-&gt;set_valign(Gtk::Align::END);
  vBox-&gt;append(*picture);
  vBox-&gt;append(*Gtk::make_managed&lt;Gtk::Label&gt;());
  list_item-&gt;set_child(*vBox);
}

void ExampleWindow::on_bind_listitem(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto col = std::dynamic_pointer_cast&lt;ModelColumns&gt;(list_item-&gt;get_item());
  if (!col)
    return;
  auto vBox = dynamic_cast&lt;Gtk::Box*&gt;(list_item-&gt;get_child());
  if (!vBox)
    return;
  auto picture = dynamic_cast&lt;Gtk::Picture*&gt;(vBox-&gt;get_first_child());
  if (!picture)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(picture-&gt;get_next_sibling());
  if (!label)
    return;
  picture-&gt;set_paintable(col-&gt;m_texture);
  label-&gt;set_markup(col-&gt;m_description);
}

void ExampleWindow::on_item_activated(unsigned int position)
{
  auto col = m_data_model-&gt;get_item(position);
  if (!col)
    return;

  const std::string filename = col-&gt;m_filename;
  const Glib::ustring description = col-&gt;m_description;

  std::cout  &lt;&lt; "Item activated: filename=" &lt;&lt; filename
    &lt;&lt; ", description=" &lt;&lt; description &lt;&lt; std::endl;
}

void ExampleWindow::on_selection_changed()
{
  auto position = m_selection_model-&gt;get_selected();
  auto col = m_data_model-&gt;get_item(position);
  if (!col)
    return;

  const std::string filename = col-&gt;m_filename;
  const Glib::ustring description = col-&gt;m_description;

  std::cout  &lt;&lt; "Selection Changed to: filename=" &lt;&lt; filename
    &lt;&lt; ", description=" &lt;&lt; description &lt;&lt; std::endl;
}

int ExampleWindow::on_model_sort(const Glib::RefPtr&lt;const ModelColumns&gt;&amp; a,
  const Glib::RefPtr&lt;const ModelColumns&gt;&amp; b)
{
  return (a-&gt;m_description &lt; b-&gt;m_description) ? -1 : ((a-&gt;m_description &gt; b-&gt;m_description) ? 1 : 0);
}

void ExampleWindow::add_entry(const std::string&amp; filename,
        const Glib::ustring&amp; description )
{
  try
  {
     auto pixbuf = Gdk::Pixbuf::create_from_file(filename);
     auto texture = Gdk::Texture::create_for_pixbuf(pixbuf);
     m_data_model-&gt;append(ModelColumns::create(filename, description, texture));
  }
  catch (const Gdk::PixbufError&amp; ex)
  {
    std::cerr &lt;&lt; "Gdk::PixbufError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
  }
  catch (const Glib::FileError&amp; ex)
  {
    std::cerr &lt;&lt; "Glib::FileError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
  }
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>


</div>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-listmodel-columnview"></a>ColumnView</h3></div></div></div>


<p>
The <code class="classname">ColumnView</code> shows a 1-dimensional list with one or more columns.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1ColumnView.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="listmodel-columnview-example"></a>Example</h4></div></div></div>


<div class="figure">
<a name="figure-listmodel-columnview"></a><p class="title"><b>Figure 10.3. ColumnView</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/listmodel_columnview.png" alt="ColumnView"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/listmodelviews/list_columnview" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  ~ExampleWindow() override;

protected:
  // Signal handlers:
  void on_button_quit();
  void on_setup_label(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item, Gtk::Align halign);
  void on_setup_progressbar(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_id(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_name(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_number(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_percentage(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);

  // A Gio::ListStore item.
  class ModelColumns : public Glib::Object
  {
  public:
    unsigned int m_col_id;
    Glib::ustring m_col_name;
    short m_col_number;
    int m_col_percentage;

    static Glib::RefPtr&lt;ModelColumns&gt; create(unsigned int col_id,
      const Glib::ustring&amp; col_name, short col_number, int col_percentage)
    {
      return Glib::make_refptr_for_instance&lt;ModelColumns&gt;(
        new ModelColumns(col_id, col_name, col_number, col_percentage));
    }

  protected:
    ModelColumns(unsigned int col_id, const Glib::ustring&amp; col_name,
      short col_number, int col_percentage)
    : m_col_id(col_id), m_col_name(col_name), m_col_number(col_number),
      m_col_percentage(col_percentage)
    {}
  }; // ModelColumns

  // Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ColumnView m_ColumnView;
  Gtk::Box m_ButtonBox;
  Gtk::Button m_Button_Quit;

  Glib::RefPtr&lt;Gio::ListStore&lt;ModelColumns&gt;&gt; m_ListStore;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_VBox(Gtk::Orientation::VERTICAL),
  m_Button_Quit("Quit")
{
  set_title("Gtk::ColumnView (Gio::ListStore) example");
  set_default_size(500, 250);

  m_VBox.set_margin(5);
  set_child(m_VBox);

  // Add the ColumnView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.set_child(m_ColumnView);

  // Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);
  m_VBox.append(m_ButtonBox);

  m_ButtonBox.append(m_Button_Quit);
  m_ButtonBox.set_margin(5);
  m_Button_Quit.set_hexpand(true);
  m_Button_Quit.set_halign(Gtk::Align::END);
  m_Button_Quit.signal_clicked().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_button_quit));

  // Create the List model:
  m_ListStore = Gio::ListStore&lt;ModelColumns&gt;::create();
  m_ListStore-&gt;append(ModelColumns::create(1, "Billy Bob", 10, 15));
  m_ListStore-&gt;append(ModelColumns::create(2, "Joey Jojo", 20, 40));
  m_ListStore-&gt;append(ModelColumns::create(3, "Rob McRoberts", 30, 70));

  // Set list model and selection model.
  auto selection_model = Gtk::SingleSelection::create(m_ListStore);
  selection_model-&gt;set_autoselect(false);
  selection_model-&gt;set_can_unselect(true);
  m_ColumnView.set_model(selection_model);
  m_ColumnView.add_css_class("data-table"); // high density table

  // Make the columns reorderable.
  // This is not necessary, but it's nice to show the feature.
  m_ColumnView.set_reorderable(true);

  // Add the ColumnView's columns:

  // Id column
  auto factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_setup_label), Gtk::Align::END));
  factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_id));
  auto column = Gtk::ColumnViewColumn::create("ID", factory);
  m_ColumnView.append_column(column);

  // Name column
  factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_setup_label), Gtk::Align::START));
  factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_name));
  column = Gtk::ColumnViewColumn::create("Name", factory);
  m_ColumnView.append_column(column);

  // Number column
  factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(sigc::bind(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_setup_label), Gtk::Align::END));
  factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_number));
  column = Gtk::ColumnViewColumn::create("Formatted number", factory);
  m_ColumnView.append_column(column);

  // Percentage column
  factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_setup_progressbar));
  factory-&gt;signal_bind().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_bind_percentage));
  column = Gtk::ColumnViewColumn::create("Some percentage", factory);
  m_ColumnView.append_column(column);
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
  set_visible(false);
}

void ExampleWindow::on_setup_label(
  const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item, Gtk::Align halign)
{
  list_item-&gt;set_child(*Gtk::make_managed&lt;Gtk::Label&gt;("", halign));
}

void ExampleWindow::on_setup_progressbar(
  const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto progressbar = Gtk::make_managed&lt;Gtk::ProgressBar&gt;();
  progressbar-&gt;set_show_text(true);
  list_item-&gt;set_child(*progressbar);
}

void ExampleWindow::on_bind_id(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto col = std::dynamic_pointer_cast&lt;ModelColumns&gt;(list_item-&gt;get_item());
  if (!col)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(list_item-&gt;get_child());
  if (!label)
    return;
  label-&gt;set_text(Glib::ustring::format(col-&gt;m_col_id));
}

void ExampleWindow::on_bind_name(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto col = std::dynamic_pointer_cast&lt;ModelColumns&gt;(list_item-&gt;get_item());
  if (!col)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(list_item-&gt;get_child());
  if (!label)
    return;
  label-&gt;set_text(col-&gt;m_col_name);
}

void ExampleWindow::on_bind_number(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto col = std::dynamic_pointer_cast&lt;ModelColumns&gt;(list_item-&gt;get_item());
  if (!col)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(list_item-&gt;get_child());
  if (!label)
    return;
  // 10 digits, using leading zeroes.
  label-&gt;set_text(Glib::ustring::sprintf("%010d", col-&gt;m_col_number));
}

void ExampleWindow::on_bind_percentage(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto col = std::dynamic_pointer_cast&lt;ModelColumns&gt;(list_item-&gt;get_item());
  if (!col)
    return;
  auto progressbar = dynamic_cast&lt;Gtk::ProgressBar*&gt;(list_item-&gt;get_child());
  if (!progressbar)
    return;
  progressbar-&gt;set_fraction(col-&gt;m_col_percentage * 0.01);
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char* argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  // Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>


</div>
</div>
</div>
<div class="navfooter">
<hr>
<table width="100%" summary="Navigation footer">
<tr>
<td width="40%" align="left">
<a accesskey="p" href="sec-listmodel-factory.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<td width="20%" align="center"><a accesskey="u" href="chapter-listmodel.html"><img src="icons/up.png" alt="Up"></a></td>
<td width="40%" align="right"> <a accesskey="n" href="sec-listmodel-sorting.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">The Factory </td>
<td width="20%" align="center"><a accesskey="h" href="index.html"><img src="icons/home.png" alt="Home"></a></td>
<td width="40%" align="right" valign="top"> Sorting</td>
</tr>
</table>
</div>
</body>
</html>
