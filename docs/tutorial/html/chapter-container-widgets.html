<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="highlight.min.css">
<script src="highlight.min.js"></script><script>
      hljs.configure({languages: ['cpp']});
      hljs.highlightAll();
    </script><title>Chapter 9. Container Widgets</title>
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="Programming with gtkmm 4">
<link rel="up" href="index.html" title="Programming with gtkmm 4">
<link rel="prev" href="sec-tooltips.html" title="Tooltips">
<link rel="next" href="sec-multi-item-containers.html" title="Multiple-item Containers">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<div class="navheader">
<table width="100%" summary="Navigation header">
<tr><th colspan="3" align="center">Chapter 9. Container Widgets</th></tr>
<tr>
<td width="20%" align="left">
<a accesskey="p" href="sec-tooltips.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<th width="60%" align="center"> </th>
<td width="20%" align="right"> <a accesskey="n" href="sec-multi-item-containers.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
</table>
<hr>
</div>
<div class="chapter">
<div class="titlepage"><div><div><h1 class="title">
<a name="chapter-container-widgets"></a>Chapter 9. Container Widgets</h1></div></div></div>
<div class="toc">
<p><b>Table of Contents</b></p>
<ul class="toc">
<li><span class="section"><a href="chapter-container-widgets.html#sec-single-item-containers">Single-item Containers</a></span></li>
<li><span class="section"><a href="sec-multi-item-containers.html">Multiple-item Containers </a></span></li>
</ul>
</div>


<p>
Container widgets, like other widgets, derive from <code class="classname">Gtk::Widget</code>.
Some container widgets, such as <code class="classname">Gtk::Grid</code> can hold many
child widgets, so these typically have more complex interfaces. Others, such as
<code class="classname">Gtk::Frame</code> contain only one child widget.
</p>

<div class="section">
<div class="titlepage"><div><div><h2 class="title" style="clear: both">
<a name="sec-single-item-containers"></a>Single-item Containers</h2></div></div></div>


<p>
Most single-item container widgets have <code class="methodname">set_child()</code>
and <code class="methodname">unset_child()</code> methods for the child widget.
<code class="classname">Gtk::Button</code> and <code class="classname">Gtk::Window</code> are
technically single-item containers, but we have discussed them already elsewhere.
</p>

<p>
We also discuss the <code class="classname">Gtk::Paned</code> widget, which allows you
to divide a window into two separate "panes". This widget actually contains
two child widgets, but the number is fixed so it seems appropriate.
</p>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-frame"></a>Frame</h3></div></div></div>


<p>
Frames can enclose one or a group of widgets within a box, optionally with a
title. For instance, you might place a group of
<code class="classname">ToggleButton</code>s or <code class="classname">CheckButton</code>s in a
<code class="classname">Frame</code>.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1Frame.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="frame-example"></a>Example</h4></div></div></div>


<div class="figure">
<a name="figure-frame"></a><p class="title"><b>Figure 9.1. Frame</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/frame.png" alt="Frame"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/frame" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  virtual ~ExampleWindow();

protected:

  //Child widgets:
  Gtk::Frame m_Frame;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
{
 /* Set some window properties */
  set_title("Frame Example");
  set_size_request(300, 300);

  /* Sets the margin around the frame. */
  m_Frame.set_margin(10);

  set_child(m_Frame);

  /* Set the frames label */
  m_Frame.set_label("Gtk::Frame Widget");

  /* Align the label at the right of the frame */
  m_Frame.set_label_align(Gtk::Align::END);
}

ExampleWindow::~ExampleWindow()
{
}

</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>


</div>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-paned"></a>Paned</h3></div></div></div>


<p>
Panes divide a widget into two halves, separated by a moveable divider.
The two halves (panes) can be oriented either horizontally (side by side) or
vertically (one above the other).
</p>

<p>
Unlike the other widgets in this section, pane widgets contain not one but two
child widgets, one in each pane. Therefore, you should use
<code class="methodname">set_start_child()</code> and <code class="methodname">set_end_child()</code>
instead of a <code class="methodname">set_child()</code> method.
</p>

<p>
You can adjust the position of the divider using the
<code class="methodname">set_position()</code> method, and you will probably need to do
so.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1Paned.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="paned-example"></a>Example</h4></div></div></div>


<div class="figure">
<a name="figure-paned"></a><p class="title"><b>Figure 9.2. Paned</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/paned.png" alt="Paned"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/paned" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include "messageslist.h"
#include "messagetext.h"
#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  virtual ~ExampleWindow();

protected:

  //Child widgets:
  Gtk::Paned m_VPaned;
  MessagesList m_MessagesList;
  MessageText m_MessageText;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">messageslist.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLE_MESSAGESLIST_H
#define GTKMM_EXAMPLE_MESSAGESLIST_H

#include &lt;gtkmm.h&gt;

class MessagesList: public Gtk::ScrolledWindow
{
public:
  MessagesList();
  ~MessagesList() override;

protected:
  // Signal handlers:
  void on_setup_message(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);
  void on_bind_message(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item);

  Glib::RefPtr&lt;Gtk::StringList&gt; m_refStringList; // The List Model.
  Gtk::ListView m_ListView; // The List View.
};
#endif //GTKMM_EXAMPLE_MESSAGESLIST_H
</code></pre>
<p>File: <code class="filename">messagetext.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLE_MESSAGETEXT_H
#define GTKMM_EXAMPLE_MESSAGETEXT_H

#include &lt;gtkmm.h&gt;

class MessageText : public Gtk::ScrolledWindow
{
public:
  MessageText();
  virtual ~MessageText();

  void insert_text();

protected:
  Gtk::TextView m_TextView;
};

#endif //GTKMM_EXAMPLE_MESSAGETEXT_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_VPaned(Gtk::Orientation::VERTICAL)
{
  set_title ("Paned Windows");
  set_default_size(450, 400);
  m_VPaned.set_margin(10);

  /* Add a vpaned widget to our toplevel window */
  set_child(m_VPaned);

  /* Now add the contents of the two halves of the window */
  m_VPaned.set_start_child(m_MessagesList);
  m_VPaned.set_end_child(m_MessageText);
}

ExampleWindow::~ExampleWindow()
{
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>
<p>File: <code class="filename">messageslist.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "messageslist.h"

MessagesList::MessagesList()
{
  /* Create a new scrolled window, with scrollbars only if needed */
  set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);

  set_child(m_ListView);

  /* create list store */
  m_refStringList = Gtk::StringList::create({});
  auto selection_model = Gtk::SingleSelection::create(m_refStringList);
  m_ListView.set_model(selection_model);

  /* Add some messages to the window */
  for (int i = 1; i &lt;= 10; ++i)
    m_refStringList-&gt;append(Glib::ustring::format("message #", i));

  // Create a ListItemFactory to use for populating list items.
  auto factory = Gtk::SignalListItemFactory::create();
  factory-&gt;signal_setup().connect(sigc::mem_fun(*this, &amp;MessagesList::on_setup_message));
  factory-&gt;signal_bind().connect(sigc::mem_fun(*this, &amp;MessagesList::on_bind_message));
  m_ListView.set_factory(factory);
}

MessagesList::~MessagesList()
{
}

void MessagesList::on_setup_message(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto label = Gtk::make_managed&lt;Gtk::Label&gt;();
  label-&gt;set_halign(Gtk::Align::START);
  list_item-&gt;set_child(*label);
}

void MessagesList::on_bind_message(const Glib::RefPtr&lt;Gtk::ListItem&gt;&amp; list_item)
{
  auto pos = list_item-&gt;get_position();
  if (pos == GTK_INVALID_LIST_POSITION)
    return;
  auto label = dynamic_cast&lt;Gtk::Label*&gt;(list_item-&gt;get_child());
  if (!label)
    return;
  label-&gt;set_text(m_refStringList-&gt;get_string(pos));
}
</code></pre>
<p>File: <code class="filename">messagetext.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "messagetext.h"

MessageText::MessageText()
{
  set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);

  set_child(m_TextView);
  insert_text();
}

MessageText::~MessageText()
{
}

void MessageText::insert_text()
{
  auto refTextBuffer = m_TextView.get_buffer();

  auto iter = refTextBuffer-&gt;get_iter_at_offset(0);
  refTextBuffer-&gt;insert(iter,
    "From: pathfinder@nasa.gov\n"
    "To: mom@nasa.gov\n"
    "Subject: Made it!\n"
    "\n"
    "We just got in this morning. The weather has been\n"
    "great - clear but cold, and there are lots of fun sights.\n"
    "Sojourner says hi. See you soon.\n"
    " -Path\n");
}
</code></pre>


</div>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-scrolledwindow"></a>ScrolledWindow</h3></div></div></div>


<p>
<code class="classname">ScrolledWindow</code> widgets create a scrollable
area. You can insert any type of widget into a
<code class="classname">ScrolledWindow</code>, and it will be accessible
regardless of its size by using the scrollbars. Note that
<code class="classname">ScrolledWindow</code> is not a
<code class="classname">Gtk::Window</code> despite the slightly misleading name.
</p>

<p>
Scrolled windows have <span class="emphasis"><em>scrollbar policies</em></span> which determine
whether the <code class="classname">Scrollbar</code>s will be displayed. The policies
can be set with the <code class="methodname">set_policy()</code> method. The policy may be
for instance <code class="literal">Gtk::PolicyType::AUTOMATIC</code> or
<code class="literal">Gtk::PolicyType::ALWAYS</code>.
<code class="literal">Gtk::PolicyType::AUTOMATIC</code> will cause the scrolled window
to display the scrollbar only if the contained widget is larger than the
visible area. <code class="literal">Gtk::PolicyType::ALWAYS</code> will cause the
scrollbar to be displayed always.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1ScrolledWindow.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="scrolledwindow-example"></a>Example</h4></div></div></div>


<p>
Here is a simple example that packs 100 toggle buttons into a ScrolledWindow. Try resizing the window to see the scrollbars react.
</p>

<div class="figure">
<a name="figure-scrolledwindow"></a><p class="title"><b>Figure 9.3. ScrolledWindow</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/scrolledwindow.png" alt="ScrolledWindow"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/scrolledwindow" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  ~ExampleWindow() override;

protected:
  //Signal handlers:
  void on_button_close();

  //Child widgets:
  Gtk::Box m_VBox;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::Grid m_Grid;
  Gtk::Button m_ButtonClose;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;iostream&gt;

ExampleWindow::ExampleWindow()
: m_VBox(Gtk::Orientation::VERTICAL, 10),
  m_ButtonClose("_Close", true)
{
  set_title("Gtk::ScrolledWindow example");
  set_size_request(300, 300);

  set_child(m_VBox);
  m_VBox.set_margin(10);

  /* The policy is one of Gtk::PolicyType::AUTOMATIC, or Gtk::PolicyType::ALWAYS.
   * Gtk::PolicyType::AUTOMATIC will automatically decide whether you need
   * scrollbars, whereas Gtk::PolicyType::ALWAYS will always leave the scrollbars
   * there. The first one is the horizontal scrollbar, the second, the vertical. */
  m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::ALWAYS);
  m_ScrolledWindow.set_expand();

  m_VBox.append(m_ScrolledWindow);

  /* set the spacing to 10 on x and 10 on y */
  m_Grid.set_row_spacing(10);
  m_Grid.set_column_spacing(10);

  /* pack the grid into the scrolled window */
  m_ScrolledWindow.set_child(m_Grid);

  /* this simply creates a grid of toggle buttons
   * to demonstrate the scrolled window. */
  for (int i = 0; i &lt; 10; i++)
  {
    for (int j = 0; j &lt; 10; j++)
    {
      auto s = Glib::ustring::compose("button (%1,%2)", i, j);
      auto pButton = Gtk::make_managed&lt;Gtk::ToggleButton&gt;(s);
      m_Grid.attach(*pButton, i, j);
    }
  }

  /* Add a "close" button to the bottom of the dialog */
  m_VBox.append(m_ButtonClose);
  m_ButtonClose.set_halign(Gtk::Align::END);
  m_ButtonClose.signal_clicked().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_button_close));

  /* Hitting the "Enter" key will cause this button to activate. */
  m_ButtonClose.grab_focus();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_close()
{
  set_visible(false);
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>


</div>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-aspectframe"></a>AspectFrame</h3></div></div></div>


<p>
The <code class="classname">AspectFrame</code> widget looks like a
<code class="classname">Frame</code> widget, but it also enforces the <span class="emphasis"><em>aspect
    ratio</em></span> (the ratio of the width to the height) of the child
widget, adding extra space if necessary. For instance, this would allow you to
display a photograph without allowing the user to distort it horizontally or
vertically while resizing.
</p>

<p><a class="ulink" href="https://gnome.pages.gitlab.gnome.org/gtkmm/classGtk_1_1AspectFrame.html" target="_top">Reference</a></p>

<div class="section">
<div class="titlepage"><div><div><h4 class="title">
<a name="aspectframe-example"></a>Example</h4></div></div></div>


<p>
The following program uses a <code class="classname">Gtk::AspectFrame</code> to present a
drawing area whose aspect ratio will always be 2:1, no matter how the user
resizes the top-level window.
</p>

<div class="figure">
<a name="figure-aspectframe"></a><p class="title"><b>Figure 9.4. AspectFrame</b></p>
<div class="figure-contents">
  
  <div class="screenshot">
    <div class="mediaobject"><img src="figures/aspectframe.png" alt="AspectFrame"></div>
  </div>
</div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/aspectframe" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  virtual ~ExampleWindow();

protected:

  //Child widgets:
  Gtk::AspectFrame m_AspectFrame;
  Gtk::Frame m_Frame;
  Gtk::DrawingArea m_DrawingArea;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_AspectFrame(
    Gtk::Align::CENTER, /* center x */
    Gtk::Align::CENTER, /* center y */
    2.0, /* xsize/ysize = 2 */
    false /* ignore child's aspect */),
  m_Frame("2x1" /* label */)
{
  set_title("Aspect Frame");

  // Set a child widget to the aspect frame */
  // Ask for a 200x200 window, but the AspectFrame will give us a 200x100
  // window since we are forcing a 2x1 aspect ratio */
  m_DrawingArea.set_content_width(200);
  m_DrawingArea.set_content_height(200);
  m_Frame.set_child(m_DrawingArea);
  m_AspectFrame.set_child(m_Frame);
  m_AspectFrame.set_margin(10);

  // Add the aspect frame to our toplevel window:
  set_child(m_AspectFrame);
}

ExampleWindow::~ExampleWindow()
{
}

</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>

</div>

</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-other-single-item-containers"></a>Other Single-item Containers</h3></div></div></div>


<p>
There are other single-item containers. See the reference documentation for a
complete list. Here are links to some example programs that show containers,
which are not mentioned elsewhere in this tutorial.
</p>

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/expander" target="_top">Source Code, Expander</a></p>
<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/popover" target="_top">Source Code, Popover</a></p>

</div>

</div>


</div>
<div class="navfooter">
<hr>
<table width="100%" summary="Navigation footer">
<tr>
<td width="40%" align="left">
<a accesskey="p" href="sec-tooltips.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<td width="20%" align="center"> </td>
<td width="40%" align="right"> <a accesskey="n" href="sec-multi-item-containers.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">Tooltips </td>
<td width="20%" align="center"><a accesskey="h" href="index.html"><img src="icons/home.png" alt="Home"></a></td>
<td width="40%" align="right" valign="top"> Multiple-item Containers </td>
</tr>
</table>
</div>
</body>
</html>
