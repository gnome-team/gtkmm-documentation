<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="highlight.min.css">
<script src="highlight.min.js"></script><script>
      hljs.configure({languages: ['cpp']});
      hljs.highlightAll();
    </script><title>Using derived widgets</title>
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="Programming with gtkmm 4">
<link rel="up" href="chapter-builder.html" title="Chapter 26. Gtk::Builder">
<link rel="prev" href="sec-builder-accessing-widgets.html" title="Accessing widgets">
<link rel="next" href="chapter-internationalization.html" title="Chapter 27. Internationalization and Localization">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<div class="navheader">
<table width="100%" summary="Navigation header">
<tr><th colspan="3" align="center">Using derived widgets</th></tr>
<tr>
<td width="20%" align="left">
<a accesskey="p" href="sec-builder-accessing-widgets.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<th width="60%" align="center">Chapter 26. Gtk::Builder</th>
<td width="20%" align="right"> <a accesskey="n" href="chapter-internationalization.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
</table>
<hr>
</div>
<div class="section">
<div class="titlepage"><div><div><h2 class="title" style="clear: both">
<a name="sec-builder-using-derived-widgets"></a>Using derived widgets</h2></div></div></div>


<p>
You can use <span class="application">Cambalache</span> and
<code class="classname">Gtk::Builder</code> to layout your own custom widgets
derived from <span class="application">gtkmm</span> widget classes. This keeps your code organized and
encapsulated, separating declarative presentation from business logic, avoiding
having most of your source just be setting properties and packing in containers.
</p>

<p>Use <code class="methodname">Gtk::Builder::get_widget_derived()</code> like so:
</p>
<pre class="programlisting"><code class="code">auto pDialog = Gtk::Builder::get_widget_derived&lt;DerivedDialog&gt;(builder, "DialogDerived");
</code></pre>

<p>
Your derived class must have a constructor that takes a pointer to the
underlying C type, and the <code class="classname">Gtk::Builder</code> instance.
All relevant classes of <span class="application">gtkmm</span> typedef their underlying C type as
<code class="classname">BaseObjectType</code> (<code class="classname">Gtk::Window</code>
typedefs <code class="classname">BaseObjectType</code> as <span class="type">GtkWindow</span>, for instance).
</p>
<p>
You must call the base class's constructor in the initialization list, providing the C pointer. For
instance,
</p>
<pre class="programlisting"><code class="code">DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Window(cobject)
{
}
</code></pre>

<p>
You could then encapsulate the manipulation of the child widgets in the
constructor of the derived class, maybe using <code class="methodname">get_widget()</code>
or <code class="methodname">get_widget_derived()</code> again. For instance,
</p>
<pre class="programlisting"><code class="code">DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Window(cobject),
  m_builder(builder),
  // Get the GtkBuilder-instantiated Button, and connect a signal handler:
  m_pButton(m_builder-&gt;get_widget&lt;Gtk::Button&gt;("quit_button"))
{
  if (m_pButton)
  {
    m_pButton-&gt;signal_clicked().connect( sigc::mem_fun(*this, &amp;DerivedDialog::on_button_quit) );
  }
}
</code></pre>

<p>
It's possible to pass additional arguments from
<code class="methodname">get_widget_derived()</code> to the constructor of the derived
widget. For instance, this call to <code class="methodname">get_widget_derived()</code>
</p>
<pre class="programlisting"><code class="code">auto pDialog = Gtk::Builder::get_widget_derived&lt;DerivedDialog&gt;(builder, "DialogDerived", true);
</code></pre>
<p>can invoke this constructor
</p>
<pre class="programlisting"><code class="code">DerivedDialog::DerivedDialog(BaseObjectType* cobject,
  const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder, bool warning)
: Gtk::Window(cobject),
  m_builder(builder),
  m_pButton(m_builder-&gt;get_widget&lt;Gtk::Button&gt;("quit_button"))
{
  // ....
}
</code></pre>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="sec-builder-and-property"></a>Gtk::Builder and Glib::Property</h3></div></div></div>


<p>
If your derived widget uses <code class="classname">Glib::Property</code>, it becomes slightly
more complicated. A derived widget that contains <code class="classname">Glib::Property</code>
members must be registered with its own name in the <span class="type">GType</span> system.
It must be registered before any of the <code class="methodname">create_from_*()</code> or
<code class="methodname">add_from_*()</code> methods are called, meaning that you may have
to create an instance of your derived widget just to have its class registered.
Your derived widget must have a constructor that has the parameters required by
<code class="methodname">get_widget_derived()</code> and calls the <code class="classname">Glib::ObjectBase</code>
constructor to register the <span class="type">GType</span>.
</p>
<pre class="programlisting"><code class="code">DerivedButton::DerivedButton(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Glib::ObjectBase("MyButton"), // The GType name will be gtkmm__CustomObject_MyButton.
  Gtk::Button(cobject),
  prop_ustring(*this, "button-ustring"),
  prop_int(*this, "button-int", 10)
{
  // ....
}
</code></pre>
<p>
It is possible also to specify properties of derived widgets, declared in C++
using <span class="application">gtkmm</span>, within <code class="filename">.ui</code> files and load/set
these using <code class="classname">Gtk::Builder</code>. See the documentation of
<code class="classname">Gtk::Builder</code> for more details on how to achieve this.
<span class="application">Cambalache</span> won’t recognize such properties as-is,
but you should be able to add a few lines of hand-coded XML to the XML code
generated by <span class="application">Cambalache</span>.
</p>
</div>

<div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="builder-example-derived"></a>Example</h3></div></div></div>


<p>
This example shows how to load a <code class="filename">.ui</code> file
at runtime and access the widgets via derived classes.
</p>

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/builder/derived" target="_top">Source Code</a></p>

<p>File: <code class="filename">derivedbutton.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLE_DERIVED_BUTTON_H
#define GTKMM_EXAMPLE_DERIVED_BUTTON_H

#include &lt;gtkmm.h&gt;

class DerivedButton : public Gtk::Button
{
public:
  DerivedButton();
  DerivedButton(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; refBuilder);
  virtual ~DerivedButton();

  // Provide proxies for the properties. The proxy allows you to connect to
  // the 'changed' signal, etc.
  Glib::PropertyProxy&lt;Glib::ustring&gt; property_ustring() { return prop_ustring.get_proxy(); }
  Glib::PropertyProxy&lt;int&gt; property_int() { return prop_int.get_proxy(); }

private:
  Glib::Property&lt;Glib::ustring&gt; prop_ustring;
  Glib::Property&lt;int&gt; prop_int;
};

#endif //GTKMM_EXAMPLE_DERIVED_BUTTON_H
</code></pre>
<p>File: <code class="filename">deriveddialog.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLE_DERIVED_DIALOG_H
#define GTKMM_EXAMPLE_DERIVED_DIALOG_H

#include &lt;gtkmm.h&gt;
#include "derivedbutton.h"

class DerivedDialog : public Gtk::Window
{
public:
  DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; refBuilder);
  DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; refBuilder,
    bool is_glad);
  ~DerivedDialog() override;

protected:
  //Signal handlers:
  void on_button_quit();

  Glib::RefPtr&lt;Gtk::Builder&gt; m_refBuilder;
  DerivedButton* m_pButton;
};

#endif //GTKMM_EXAMPLE_DERIVED_DIALOG_H
</code></pre>
<p>File: <code class="filename">derivedbutton.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "derivedbutton.h"
#include &lt;iostream&gt;

// For creating a dummy object in main.cc.
DerivedButton::DerivedButton()
: Glib::ObjectBase("MyButton"),
  prop_ustring(*this, "button-ustring"),
  prop_int(*this, "button-int", 10)
{
}

DerivedButton::DerivedButton(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; /* refBuilder */)
: // To register custom properties, you must register a custom GType.  If
  // you don't know what that means, don't worry, just remember to add
  // this Glib::ObjectBase constructor call to your class' constructor.
  // The GType name will be gtkmm__CustomObject_MyButton.
  Glib::ObjectBase("MyButton"),
  Gtk::Button(cobject),
  // register the properties with the object and give them names
  prop_ustring(*this, "button-ustring"),
  // this one has a default value
  prop_int(*this, "button-int", 10)
{
  // Register some handlers that will be called when the values of the
  // specified parameters are changed.
  property_ustring().signal_changed().connect(
    [](){ std::cout &lt;&lt; "- ustring property changed!" &lt;&lt; std::endl; }
  );
  property_int().signal_changed().connect(
    [](){ std::cout &lt;&lt; "- int property changed!" &lt;&lt; std::endl; }
  );
}

DerivedButton::~DerivedButton()
{
}
</code></pre>
<p>File: <code class="filename">deriveddialog.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "deriveddialog.h"
#include &lt;iostream&gt;

DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; refBuilder)
: Gtk::Window(cobject),
  m_refBuilder(refBuilder),
  m_pButton(nullptr)
{
  // Get the GtkBuilder-instantiated Button, and connect a signal handler:
  m_pButton = Gtk::Builder::get_widget_derived&lt;DerivedButton&gt;(m_refBuilder, "quit_button");
  if (m_pButton)
  {
    m_pButton-&gt;signal_clicked().connect( sigc::mem_fun(*this, &amp;DerivedDialog::on_button_quit) );
    std::cout &lt;&lt; "ustring, int: " &lt;&lt; m_pButton-&gt;property_ustring()
              &lt;&lt; ", " &lt;&lt; m_pButton-&gt;property_int() &lt;&lt; std::endl;
    m_pButton-&gt;property_int() = 99;
    std::cout &lt;&lt; "ustring, int: " &lt;&lt; m_pButton-&gt;property_ustring()
              &lt;&lt; ", " &lt;&lt; m_pButton-&gt;property_int() &lt;&lt; std::endl;
  }
}

// The first two parameters are mandatory in a constructor that will be called
// from Gtk::Builder::get_widget_derived().
// Additional parameters, if any, correspond to additional arguments in the call
// to Gtk::Builder::get_widget_derived().
DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; refBuilder,
  bool is_glad)
: DerivedDialog(cobject, refBuilder) // Delegate to the other constructor
{
  // Show an icon.
  auto content_area = refBuilder-&gt;get_widget&lt;Gtk::Box&gt;("dialog-content_area");
  if (!content_area)
  {
    std::cerr &lt;&lt; "Could not get the content area" &lt;&lt; std::endl;
    return;
  }
  auto pImage = Gtk::make_managed&lt;Gtk::Image&gt;();
  pImage-&gt;set_from_icon_name(is_glad ? "face-smile" : "face-sad");
  pImage-&gt;set_icon_size(Gtk::IconSize::LARGE);
  pImage-&gt;set_expand();
  content_area-&gt;append(*pImage);
}

DerivedDialog::~DerivedDialog()
{
}

void DerivedDialog::on_button_quit()
{
  set_visible(false); // set_visible(false) will cause Gtk::Application::run() to end.
}
</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "deriveddialog.h"
#include &lt;iostream&gt;
#include &lt;cstring&gt;

namespace
{
bool show_icon = false;
bool is_glad = true;

DerivedDialog* pDialog = nullptr;
Glib::RefPtr&lt;Gtk::Application&gt; app;

void on_app_activate()
{
  // Create a dummy instance before the call to refBuilder-&gt;add_from_file().
  // This creation registers DerivedButton's class in the GType system.
  // This is necessary because DerivedButton contains user-defined properties
  // (Glib::Property) and is created by Gtk::Builder.
  static_cast&lt;void&gt;(DerivedButton());

  // Load the GtkBuilder file and instantiate its widgets:
  auto refBuilder = Gtk::Builder::create();
  try
  {
    refBuilder-&gt;add_from_file("derived.ui");
  }
  catch(const Glib::FileError&amp; ex)
  {
    std::cerr &lt;&lt; "FileError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
    return;
  }
  catch(const Glib::MarkupError&amp; ex)
  {
    std::cerr &lt;&lt; "MarkupError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
    return;
  }
  catch(const Gtk::BuilderError&amp; ex)
  {
    std::cerr &lt;&lt; "BuilderError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
    return;
  }

  // Get the GtkBuilder-instantiated dialog:
  if (show_icon)
    pDialog = Gtk::Builder::get_widget_derived&lt;DerivedDialog&gt;(refBuilder, "DialogDerived", is_glad);
  else
    pDialog = Gtk::Builder::get_widget_derived&lt;DerivedDialog&gt;(refBuilder, "DialogDerived");

  if (!pDialog)
  {
    std::cerr &lt;&lt; "Could not get the dialog" &lt;&lt; std::endl;
    return;
  }

  // It's not possible to delete widgets after app-&gt;run() has returned.
  // Delete the dialog with its child widgets before app-&gt;run() returns.
  pDialog-&gt;signal_hide().connect([] () { delete pDialog; });

  app-&gt;add_window(*pDialog);
  pDialog-&gt;set_visible(true);
}
} // anonymous namespace

int main(int argc, char** argv)
{
  int argc1 = argc;
  if (argc &gt; 1)
  {
    if (std::strcmp(argv[1], "--glad") == 0)
    {
      show_icon = true;
      is_glad = true;
      argc1 = 1; // Don't give the command line arguments to Gtk::Application.
    }
    else if (std::strcmp(argv[1], "--sad") == 0)
    {
      show_icon = true;
      is_glad = false;
      argc1 = 1; // Don't give the command line arguments to Gtk::Application.
    }
  }

  app = Gtk::Application::create("org.gtkmm.example");

  // Instantiate a dialog when the application has been activated.
  // This can only be done after the application has been registered.
  // It's possible to call app-&gt;register_application() explicitly, but
  // usually it's easier to let app-&gt;run() do it for you.
  app-&gt;signal_activate().connect([] () { on_app_activate(); });

  return app-&gt;run(argc1, argv);
}
</code></pre>


</div>

</div>
<div class="navfooter">
<hr>
<table width="100%" summary="Navigation footer">
<tr>
<td width="40%" align="left">
<a accesskey="p" href="sec-builder-accessing-widgets.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<td width="20%" align="center"><a accesskey="u" href="chapter-builder.html"><img src="icons/up.png" alt="Up"></a></td>
<td width="40%" align="right"> <a accesskey="n" href="chapter-internationalization.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">Accessing widgets </td>
<td width="20%" align="center"><a accesskey="h" href="index.html"><img src="icons/home.png" alt="Home"></a></td>
<td width="40%" align="right" valign="top"> Chapter 27. Internationalization and Localization</td>
</tr>
</table>
</div>
</body>
</html>
