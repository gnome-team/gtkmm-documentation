<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="highlight.min.css">
<script src="highlight.min.js"></script><script>
      hljs.configure({languages: ['cpp']});
      hljs.highlightAll();
    </script><title>Chapter 23. Keyboard and Mouse Events</title>
<link rel="stylesheet" type="text/css" href="style.css">
<meta name="generator" content="DocBook XSL Stylesheets Vsnapshot">
<link rel="home" href="index.html" title="Programming with gtkmm 4">
<link rel="up" href="index.html" title="Programming with gtkmm 4">
<link rel="prev" href="sec-filechooser-filedialog.html" title="FileChooser and FileDialog">
<link rel="next" href="sec-keyboardevents-propagation.html" title="Event Propagation">
</head>
<body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
<div class="navheader">
<table width="100%" summary="Navigation header">
<tr><th colspan="3" align="center">Chapter 23. Keyboard and Mouse Events</th></tr>
<tr>
<td width="20%" align="left">
<a accesskey="p" href="sec-filechooser-filedialog.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<th width="60%" align="center"> </th>
<td width="20%" align="right"> <a accesskey="n" href="sec-keyboardevents-propagation.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
</table>
<hr>
</div>
<div class="chapter">
<div class="titlepage"><div><div><h1 class="title">
<a name="chapter-keyboardevents"></a>Chapter 23. Keyboard and Mouse Events</h1></div></div></div>
<div class="toc">
<p><b>Table of Contents</b></p>
<ul class="toc">
<li><span class="section"><a href="chapter-keyboardevents.html#sec-keyboardevents-overview">Overview</a></span></li>
<li><span class="section"><a href="sec-keyboardevents-propagation.html">Event Propagation</a></span></li>
<li><span class="section"><a href="sec-keyboardevents-mouse.html">Mouse Events</a></span></li>
</ul>
</div>

  <p>
    Event signals differ in some ways from other signals. These differences are described
    in the <a class="link" href="sec-eventsignals.html" title="Event signals">Event signals</a> section in
    the appendix. Here we will use keyboard events and mouse events to show how
    events can be used in a program.
  </p>
  <div class="section">
<div class="titlepage"><div><div><h2 class="title" style="clear: both">
<a name="sec-keyboardevents-overview"></a>Overview</h2></div></div></div>
  

    <p>
      Whenever you press or release a key, an event is emitted. You can add an
      event controller and connect a signal handler to handle such events.
    </p>
    <p>
      The event signal handler will receive arguments that depend on the type of event.
      For key press events the arguments are (<span class="type">guint</span> <code class="varname">keyval</code>,
      <span class="type">guint</span> <code class="varname">keycode</code>, <span class="type">Gdk::ModifierType</span> <code class="varname">state</code>).
      As described in the <a class="link" href="sec-eventsignals.html" title="Event signals">appendix</a>,
      the key press event signal handler returns a <span class="type">bool</span> value, to indicate that
      the signal is fully handled (<code class="literal">true</code>) or allow event propagation
      (<code class="literal">false</code>).
    </p>
    <p>
      To determine which key was pressed or released, you read the value of
      the <code class="varname">keyval</code> argument and compare it with a constant in the
      <a class="ulink" href="https://gitlab.gnome.org/GNOME/gtk/tree/main/gdk/gdkkeysyms.h" target="_top">
      <code class="filename">&lt;gdk/gdkkeysyms.h&gt;</code></a> header file. The states of
      modifier keys (shift, ctrl, etc.) are available as bit-flags in
      <code class="varname">state</code>.
    </p>
    <p>
      Here's a simple example:
    </p>
<pre class="programlisting"><code class="code">
bool MyClass::on_key_pressed(guint keyval, guint, Gdk::ModifierType state)
{
  if (keyval == GDK_KEY_1 &amp;&amp;
    (state &amp; (Gdk::ModifierType::SHIFT_MASK | Gdk::ModifierType::CONTROL_MASK |
     Gdk::ModifierType::ALT_MASK)) == Gdk::ModifierType::ALT_MASK)
  {
    handle_alt_press();
    return true;
  }
  return false;
}

// in MyClass constructor
auto controller = Gtk::EventControllerKey::create();
controller-&gt;signal_key_pressed().connect(
  sigc::mem_fun(*this, &amp;MyClass::on_key_pressed), false);
add_controller(controller);
</code></pre>

    <div class="section">
<div class="titlepage"><div><div><h3 class="title">
<a name="keyboardevents-simple-example"></a>Example</h3></div></div></div>
    

      <p>
        In this example there are three keyboard shortcuts:
        <span class="keycap"><strong>Alt</strong></span>+<span class="keycap"><strong>1</strong></span> selects the first radio button,
        <span class="keycap"><strong>Alt</strong></span>+<span class="keycap"><strong>2</strong></span> selects the second one, and the
        <span class="keycap"><strong>Esc</strong></span> key hides (closes) the window.
      </p>

      <div class="figure">
<a name="figure-keyboardevents-simple"></a><p class="title"><b>Figure 23.1. Keyboard Events - Simple</b></p>
<div class="figure-contents">
        
        <div class="screenshot">
          <div class="mediaobject"><img src="figures/events_simple.png" alt="Keyboard Events - Simple"></div>
        </div>
      </div>
</div>
<br class="figure-break">

<p><a class="ulink" href="https://gitlab.gnome.org/GNOME/gtkmm-documentation/tree/master/examples/book/events/keyboard_simple/" target="_top">Source Code</a></p>

<p>File: <code class="filename">examplewindow.h</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

#include &lt;gtkmm.h&gt;

class ExampleWindow : public Gtk::Window
{
public:

  ExampleWindow();
  virtual ~ExampleWindow();

private:
  // Signal handler:
  bool on_window_key_pressed(guint keyval, guint keycode, Gdk::ModifierType state);

  Gtk::Box m_container;
  Gtk::CheckButton m_first;
  Gtk::CheckButton m_second;
};

#endif //GTKMM_EXAMPLEWINDOW_H
</code></pre>
<p>File: <code class="filename">examplewindow.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"

ExampleWindow::ExampleWindow()
{
  set_title("Keyboard Events");
  m_container.set_margin(10);
  set_child(m_container);

  // Radio buttons:
  m_first.set_label("First");
  m_second.set_label("Second");

  m_second.set_group(m_first);
  m_first.set_active();

  // Main Container:
  m_container.set_orientation(Gtk::Orientation::HORIZONTAL);
  m_container.append(m_first);
  m_container.append(m_second);

  // Events.
  auto controller = Gtk::EventControllerKey::create();
  controller-&gt;signal_key_pressed().connect(
    sigc::mem_fun(*this, &amp;ExampleWindow::on_window_key_pressed), false);
  add_controller(controller);
}

bool ExampleWindow::on_window_key_pressed(guint keyval, guint, Gdk::ModifierType state)
{
  //Gdk::ModifierType::ALT_MASK -&gt; the 'Alt' key(mask)
  //GDK_KEY_1 -&gt; the '1' key
  //GDK_KEY_2 -&gt; the '2' key

  //select the first radio button, when we press alt + 1
  if((keyval == GDK_KEY_1) &amp;&amp;
    (state &amp; (Gdk::ModifierType::SHIFT_MASK | Gdk::ModifierType::CONTROL_MASK | Gdk::ModifierType::ALT_MASK)) == Gdk::ModifierType::ALT_MASK)
  {
    m_first.set_active();
    //returning true, cancels the propagation of the event
    return true;
  }
  else if((keyval == GDK_KEY_2) &amp;&amp;
    (state &amp; (Gdk::ModifierType::SHIFT_MASK | Gdk::ModifierType::CONTROL_MASK | Gdk::ModifierType::ALT_MASK)) == Gdk::ModifierType::ALT_MASK)
  {
    //and the second radio button, when we press alt + 2
    m_second.set_active();
    return true;
  }
  else if(keyval == GDK_KEY_Escape)
  {
    //close the window, when the 'esc' key is pressed
    set_visible(false);
    return true;
  }

  //the event has not been handled
  return false;
}

ExampleWindow::~ExampleWindow()
{
}

</code></pre>
<p>File: <code class="filename">main.cc</code> (For use with gtkmm 4)</p>
<pre class="programlisting"><code class="code">#include "examplewindow.h"
#include &lt;gtkmm/application.h&gt;

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create("org.gtkmm.example");

  //Shows the window and returns when it is closed.
  return app-&gt;make_window_and_run&lt;ExampleWindow&gt;(argc, argv);
}
</code></pre>

    </div>
  </div>

  

  
</div>
<div class="navfooter">
<hr>
<table width="100%" summary="Navigation footer">
<tr>
<td width="40%" align="left">
<a accesskey="p" href="sec-filechooser-filedialog.html"><img src="icons/prev.png" alt="Prev"></a> </td>
<td width="20%" align="center"> </td>
<td width="40%" align="right"> <a accesskey="n" href="sec-keyboardevents-propagation.html"><img src="icons/next.png" alt="Next"></a>
</td>
</tr>
<tr>
<td width="40%" align="left" valign="top">FileChooser and FileDialog </td>
<td width="20%" align="center"><a accesskey="h" href="index.html"><img src="icons/home.png" alt="Home"></a></td>
<td width="40%" align="right" valign="top"> Event Propagation</td>
</tr>
</table>
</div>
</body>
</html>
