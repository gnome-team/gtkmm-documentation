��         �    �0      PA  R   QA  e   �A  k   
B  d   vB  q   �B     MC  �   �C  ~   �D  �   E  �   �E  �   �F  u   #G  ]   �G  [   �G  %   SH  U  yH  2  �J  �   L  �   �L  �   �M  >   xN  A   �N  `   �N  I   ZO  �   �O  �  :P  .  R    1T  t   QU  �   �U  �   YV  �   W  p  X  2  �Y    �Z  �   �[  U   H\  �   �\  �  &]  S   �^  3   #_  �   W_  �   �_  }   �`  [   a  p   da  �   �a  �   �b  �   >c  �   d  C   �d  p   �d  c   Re  g   �e  K   f  �   jf  z   g  3  |g  Q   �i  �  j  �   �k  �   ml  E   �l  2   Bm  -   um  S   �m  C   �m  Z   ;n  a   �n  U   �n  q   No  :   �o  �   �o  A   �p  $   q  H   Cq  B   �q  ;   �q  '   r  =   3r  P   qr  .   �r  (   �r  (   s  I   Cs  \   �s     �s  3   �s  �   1t  �  �t  d   �w  4  �w  6  y  �   Vz  �   9{  A   |  �   _|     �|  a   }  �   o}  �   ~  �   
  7   �  r   �  �   r�  P   +�  5   |�  �  ��     ��  �   ��  B  `�  _   ��  E   �     I�  H   Z�     ��  �   ��     F�  
   U�  G   `�  �   ��  �   q�  ]   &�  4   ��  :   ��  *   �  /   �  -   O�  F   }�  \   ċ  Z   !�  =   |�  R   ��  =   �  s   K�  4   ��  2   �  +   '�  h   S�  $   ��  h   �  +   J�  �   v�  q   �  n   ��  �   ��  o   ��  l   �  �   ��  �   �  G   ��  -   ܓ  O   
�  /   Z�  4   ��      ��  ]   ��  ;   >�  �   z�  H   <�     ��  F   ��     ؖ     �     �     ��     �     �  (   �     E�     Q�     c�     r�  `   ~�  8   ߗ     �  	   !�     +�  	   C�     M�     b�     p�  	   |�  &   ��     ��     Ř     ܘ     �  x   �     h�     q�  N   ��     ؙ  2   ޙ     �  	   �     "�     .�  <  6�     s�     z�     ��  ;   ��  -   ϛ     ��      �     5�     L�     h�  
   t�     �     ��     ��     ��     ��     ʜ     ֜     ߜ     �     
�     �  (   /�     X�     o�     ��     ��     ��     ǝ     ٝ     ޝ     �     �     �     9�  �   A�     �     �     *�     8�  �   G�     ޟ     �     �     ��     �  =   �     W�     _�     }�     ��     ��     Ǡ     ՠ     �     ��     �     #�     8�     L�     f�     {�     ��     ��     ��     ơ  !   ١  *   ��  &   &�     M�     \�  0   d�  #   ��  $   ��  :   ޢ  >   �     X�     ^�     o�     ��     ��     ��     ��     ǣ  0   ϣ      �     	�     '�     5�     P�     Y�     e�     w�  9   ��     Ȥ  \   ڤ  i   7�  E   ��  C   �  M   +�     y�     ��  E   ��  J   �  G   7�  B   �  B   §  -   �     3�  e   A�  Y   ��     �     �     �     �     9�     T�     q�     ��     ��  �   ��  k   w�  �  �  X   �    ج  |   ޭ  X   [�  �   ��  m  A�  a   ��  �   �  Y   ۱  �   5�     ��      �  \   '�  �  ��  B  n�     ��  L  ��  )   �  �   /�  ;   Թ  �   �  �   �     ˻     �  3   ��      1�  t   R�  B  Ǿ  <   
�  o   G�  T   ��  S  �  w   `�  m   ��  6   F�     }�     ��  .   ��     ��     ��  /   ��  �   '�  (   �  #   *�  )   N�  4   x�  C   ��  $   ��     �  $   (�  o   M�  !  ��     ��     ��  6   ��  .   +�     Z�  ,   t�     ��     ��     ��  S   ��     �  H   0�  %   y�     ��     ��  :   ��     ��      �     �     �     1�     6�     <�     [�     a�     t�     |�     ��  	   ��     ��     ��  	   ��     ��     ��     �     �     7�     =�     C�     U�     h�     v�     ��     ��     ��     ��  �   ��     ~�  "   ��     ��  (   ��     ��     ��     
�     �  =    �     ^�     t�  G   ��     ��  '   ��     
�  �   !�     ��     ��  "   ��     �  3   �  �   A�     ��  
   ��     ��  +   ��  ,   !�     N�     T�  �  Z�     R�  �   [�     +�     1�     C�     ]�  
   p�     {�     ��     ��     ��     ��     ��     ��     ��     ��     �  3   %�  �   Y�     ��  )   �     5�     C�     O�     ]�     k�     ��  	   ��     ��     ��  	   ��     ��     ��     ��  !   �     %�     7�  	   F�     P�     f�     w�     ��     ��     ��     ��     ��     ��     ��  "   ��     �     /�  �   L�     5�     =�     E�     d�     p�     ��  
   ��     ��     ��     ��     ��     ��     ��     �     �     "�  �   9�  a   ��  
   5�     @�     X�     f�  	   ~�     ��     ��     ��     ��  �   ��     N�     b�  6   r�  4   ��     ��  	   ��  R   ��  �   K�  B   �  =   I�  	   ��  0   ��  �   ��  N   E�  	   ��     ��     ��     ��  3   ��  {   �  D   ��  9   ��  A   
�  7   L�  6   ��  c   ��  	   �  N   )�  9   x�  @   ��  3   ��  D   '�  2   l�  [   ��  w   ��  �   s�  �   �  �   ��     d�     s�     ��     ��     ��     ��     ��     ��     ��     ��  �   �     ��     ��  	   ��     ��     ��     �     /�     D�     b�  	   w�     ��     ��  ?   ��  Y   ��     8�     G�  /   ^�     ��     ��  %   ��  9   ��     �  ,   �  y   D�  9   ��     ��     	�     �     +�      D�  !   e�     ��     ��     ��  �   ��  �   ��  �   8�  �   1�      �     �  -   )�     W�  $   f�     ��     ��     ��     ��     ��     ��     ��     �     �  1   .�  4   `�     ��     ��  )   ��     ��  /   �     7�  )   S�  7   }�  !   ��  3   ��  /   �  7   ;�  3   s�     ��  
   ��  
   ��     ��     ��     ��  4   ��     -�  '   D�     l�  )   {�     ��  :   ��     ��  3   ��  �   -�      ��     ��     ��  �   ��     j�    o�  !  ��  �  ��     ?�     O�     U�      ]�  �   ~�  P  M�  �  ��     1�     9�     F�  �   S�     ��     
�     !�     .�  
   D�  +   O�  .   {�     ��     ��     ��     ��     ��  	   ��  >   ��     ,�     5�  
   <�     G�     O�     ^�     c�     h�     o�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �  �  �  �   �     �    �    �    �    �    � 4   � ;   " %   ^ 9   � a   � 4     e   U    � 2   �    i   
    t )   �    �    �    �    � 
   �    � 
   � D   � >   B �   � p      t 7   x	 x   �	 	   )
    3
    C
    L
 '   h
    �
    �
 	   �
    �
 �   �
 �   � �   U     d   +    � 0  �    � �  �   � �   � Q  g b   � �    �   � �   d �   �    � 0   � q  � R   9 e   � k   � d   ^ q   �    5 �   � ~   | �   � �   � �   w u    ]   � [   � %   ; U  a 2  �! �   �" �   �# �   �$ >   `% A   �% `   �% I   B& �   �& �  "' .  �(   + t   9, �   �, �   A- �    . p  �. 2  l0   �1 �   �2 U   03 �   �3 �  4 S   �5 3   6 �   ?6 �   �6 }   r7 [   �7 p   L8 �   �8 �   �9 �   &: �   �: C   �; p   �; c   :< g   �< K   = �   R= z   �= 3  d> Q   �@ �  �@ �   �B �   UC E   �C 2   *D -   ]D S   �D C   �D Z   #E a   ~E U   �E q   6F :   �F �   �F A   �G $   H H   +H B   tH ;   �H '   �H =   I P   YI .   �I (   �I (   J I   +J \   uJ    �J 3   �J �   K �  �K d   mN 4  �N 6  P �   >Q �   !R A   S �   GS    �S a   �S �   WT �   �T �   �U 7   �V r   �V �   ZW P   X 5   dX �  �X    �Z �   �[ B  H\ _   �] E   �]    1^ H   H^    �^ �   �^    4_ 
   C_ G   N_ �   �_ �   _` ]   a 4   ra C   �a *   �a 3   b 0   Jb L   {b c   �b b   ,c C   �c U   �c >   )d r   hd 9   �d 7   e +   Me y   ye +   �e y   f *   �f �   �f s   dg p   �g �   Ih s   �h p   ]i �   �i �   ]j M   �j -   2k R   `k /   �k 8   �k    l c   ;l 9   �l �   �l L   �m    n F   n    Vn    bn    fn    rn    �n    �n D   �n    �n    �n    o    0o `   <o (   �o    �o 	   �o     �o 	   �o    p     p    2p 	   >p 1   Hp    zp    �p    �p 
   �p �   �p    Lq    Uq R   oq    �q 8   �q    r 	   r    r    r f  %r    �s    �s    �s H   �s 1   �s    %t !   9t    [t    tt    �t    �t    �t    �t    �t    �t    �t    u     u    )u    >u    Su    `u $   yu "   �u    �u    �u    �u    v     v    2v    ;v    Dv #   av    �v    �v �   �v    yw    �w    �w    �w �   �w    �x    �x    �x    �x    �x d   �x    )y &   1y (   Xy    �y %   �y    �y    �y $   �y    �y    z    'z    ?z    Uz    tz    �z    �z    �z    �z    �z "   { +   ){ '   U{    }{    �{ @   �{ /   �{ 1   
| I   <| S   �|    �|    �| 3   }    6}    I}    h}    w}    �} %   �} 	   �}    �} 
   �}    �}    �}    ~    ~ %   #~ F   I~    �~ S   �~ f   �~ @   ] E   � M   �    2�    @� ?   _� D   �� A   � <   &� <   c� 4   ��    Ձ g   � k   K�    ��    ��    ւ    ߂    ��    �     .�    O�    d� �   {� k   <� �  �� X   D�   �� |   �� X    � �   y� m  � a   t� �   ֊ Y   �� �   ��    ��     ˌ \   � �  I� B  3�    v� L  ~� *   ˒ �   �� ;   �� �   ד �   ɔ    ��    �� 3   ė     �� t   � B  �� <   љ o   � T   ~� S  Ӛ w   '� m   �� 6   �    D�    b� 0   ��    ��    �� 3   ̝ �    � +   ڞ 1   � 1   8� @   j� M   �� %   ��    � %   :� o   `� !  Р    �    �� 6   � =   G�    �� 4   ��    ٢    �    � X   ��    W� P   t� '   ţ    ��    �� C   	�    M�    V�    ^�    q�    ��    ��     ��    ��    Ƥ    ݤ #   �    	� 	   �    �    6� 
   M�    X�    l�    �� 1   ��    ť    ˥    إ    �    �    �    $�    -�    ?�    T� �   s�    '� 4   G� "   |� 6   ��    ֧    �     �    � A   �    X�    r� ^   ��    � 5   ��    2� �   J�    �    � *   � 
   @� 3   K� �   �    /�    6�    G� 0   M� 1   ~� 	   ��    ��   ��    ǭ �   ӭ    ��    ��    Ȯ    �    ��    �    �    ,�    B�    \�    y�    ��    ��    ��    �� 5   ¯ �   ��    Ȱ +   ԰     �    �    �    '�    5�    T�    i�    r�    �� 
   ��    �� #   ��    � 0   �    �    0�    ?�    F�    ^�    v�    ��    ��    ��    β    ֲ    �    �� +   �    8�    O�   o�    x� 
   �� &   �� 	   ��    ��     δ 
   �    ��    �    �    �    &� )   ,�    V�    _�    v� �   �� b   /� 
   ��    ��    ��    ȶ 
   �    ��    �    �    %� �   1�    ��    ׷ >   � ;   +�    g�    |� i   �� �   � I   Ĺ B   � 
   Q� ,   \� �   �� d   � 
   ��    ��    ��    �� :   һ �   � J   �� A   � H   1� ;   z� C   �� r   ��    m� ]   y� L   ׾ K   $� ,   p� e   �� 1   � c   5� �   �� �   *� �   �� �   {�    Z�    j�    w�    ��    ��    ��    �� 	   ��    �� 
   �� �   �    ��    �� 	   ��    ��    ��    �    <� %   Q�    w� 	   ��    ��    �� J   �� k   �    o�    �� 9   ��    ��    �� 3   � ?   I�    �� -   �� |   �� N   9�    ��    ��    ��    �� $   �� '   ��    �    ;�    U� �   g� �   H� +  � �   :�    0�    A� /   Y�    �� &   ��    ��    ��    ��    ��    �    �    3�    A�    I� 2   b� 4   ��    ��    �� )   ��    "� -   <�    j� )   �� 5   �� !   �� 1   � -   :� 7   h� 3   ��    �� 
   �� 
   ��    ��    �    � 4   %�    Z� '   q�    �� +   ��    �� :   ��    � 3   (� �   \�     ��    ��    � �   �    ��   �� !  �� �  ��    n�    ��    ��     �� �   �� P  �� �  ��    f�    n�    {� �   ��    -�    ?�    V�    c� 
   y� +   �� .   ��    ��    ��    ��    ��    �� 	   � >   "�    a�    j� 
   q�    |�    ��    ��    ��    ��    ��    ��    ��    ��    ��    ��    �    �    "�    .�    4�    =�    C� �  G� �   �    ��    ��    ��    ��    �    � 4   (� ;   ]� %   �� 9   �� a   �� 4   [� e   ��    �� 2   
�   =� i   E�    �� )   ��    ��    �    �    � 
   �    '� 
   /� D   :� >   � �   �� p   @�   �� 7   �� x   �� 	   f�    p�    ��    �� '   ��    ��    �� 	   ��    �� �   � �   �� �   ��    \� d   h� <   �� 0  
�    ;� �  B�   �� �    � Q  �� b    � �   �� �   I� �   �� �   ]�    �� 0   ��    x  N  �   �  �       �     �  �    _          �  �      �   L   �  �      j    �    �  �                    5  \   �  �  s      �    ^  �  �          h       �      �        o  �   �        �  c  ~  2      �  �      `                 �  >   �   �           *          �   �  �  �  �  �   �  k  3   �    r   E  *   D  �    +      (      �   �      b  T   O  \  �  �   �  �   u   �      �  �  �          y  p           �  j   [   M  U  u  �  /  �   �   (           w      �      �  �   �  �      |          �  �    "   �      <               �   �  �      �  �        �          l      �  o           C  �  �       m           I  V  �  ~   I   m  �   �  �  �       }      �  �       �      �      �     J   e  �  W   �  W  i  h  �      �  �  �   �  �  S  G   ;  �  �   	      K   H  n  �  
  �         �  �  �  �       >  �  b  �       X      =   �    A   )      �  �  v      7              �          �   z       e      �   a   �  s             �   �      )       �      �  �  �   '       �   �       �      �  �  �   F  �   ~     �          �       �  �    �  �   �  �   b           �  C      �       P   �      �           �            �      k       �       �      �  �   �  �  �   E        -   ]      �   0  �       F  c   �           �  Z    +           �   H  �  d  �  x      �    '  c      d    �             ]     Z  �   g  ,        �   �   �           �  �  �               :  
   �   �   �  �     n  �  �     U   B  �   �  �        4          3      1          #   �    <  �  �           4   �           �    �       �   �     {  �   1   �   �  k  [                       @         /   �   ,       �      �   "  �  �          �  �   �  z      s   q   �      �   |   M  �       �  %  G  �  4     �            �      ;   �          �       �      �   �    �   _  .     ,          L  R               t      �  &  �   R  O     g      E  �  �                �  `  l  
  �          f  �  �       �  �  �   @       �  �      ^      �    �   �  -  �  �  �  �  �   �      K          J       D  q  9  K  �         /  +      �      p  Y  y   �  o  �   �   �  =     .  �          |  Z   :  �  W  ;  M   �      �  =  �   �          �  �   �  V  V   �   9  �  �  �   C   �  P  �  �      �          �  &                 �      f   �   �  �   �  �  !      O  �  �        �  �        ^   �  �   >  (      �        x   �             ?  Q  �   �  h  �   0  �  �   �            J  �  �       �  �   �     �   T      �   {          N      i   t  �  B   �  }  �          �   �  8      `   �  g       $  �   �  �  �  �      e  r  �  �  �  u  �  �  �   �  �  9   �  �  #  v           �  �   !       Y   �  �  &  �     ?  �   #              �   �      �  T  �  �   }   �                  2      �          �  8   X   !               �   A  I  6      	      L  �      �  f      �  1      �  _  �   �           N       �  m               �  6   2      �  -  �       8      �          Y  �  z  a  S           B     <  �  �  �   �               �  �   �  	              d             �  �   j      �  �  w      5  �  �  �            *  �  �     �   �  �               �     �  S    U  t     G      �  �   �   �      l       w   X  ]              0   �   �  �       �   �  �  .          �  �  �      "  v  �      y      $      i  �              R  Q   a        �       �  5   \  :        �      �   �  �  �      {         �  �         �      �   �         7      Q  D   q  F               �  '  	  �                      �       %   %  P        @  �  ?   �      $   �  )  �  p  �      �    �  �           �  3      �  �  �        n   7   �  �  [  H   A        6  
  �        �   r  �     
        GtkSizeRequestMode gtk_widget_get_request_mode(GtkWidget* widget);
       
        GtkToolItem* gtk_tool_button_new(GtkWidget* icon_widget, const gchar*
        label);
       
        _INITIALIZATION(`Gdk::Rectangle&amp;',`GdkRectangle', `$3 =
        Glib::wrap(&amp;($4))')
       
        _INITIALIZATION(`SizeRequestMode&amp;',`GtkSizeRequestMode',`$3 =
        ($1)($4)')
       
        _INITIALIZATION(`SizeRequestMode&amp;',`GtkSizeRequestMode',`$3 =
        (SizeRequestMode)($4)')
       
        _WRAP_CTOR(ToolButton(Widget&amp; icon_widget, const Glib::ustring&amp;
        label{?}), gtk_tool_button_new)
       
        _WRAP_METHOD(bool get_cell_rect(const TreeModel::Path&amp; path, const
        CellRenderer&amp; cell, Gdk::Rectangle&amp; rect{&gt;&gt;}) const,
        gtk_icon_view_get_cell_rect)
       
        _WRAP_METHOD(void get_request_mode(SizeRequestMode&amp; mode{OUT})
        const, gtk_widget_get_request_mode)
       
        _WRAP_METHOD(void set_device_events(Gdk::EventMask events{.}, const
        Glib::RefPtr&lt;const Gdk::Device&gt;&amp; device{.}),
        gtk_widget_set_device_events)
       
        _WRAP_METHOD(void set_device_events(Gdk::EventMask events{events},
        const Glib::RefPtr&lt;const Gdk::Device&gt;&amp; device{device}),
        gtk_widget_set_device_events)
       
        gboolean gtk_icon_view_get_cell_rect(GtkIconView* icon_view,
        GtkTreePath* path, GtkCellRenderer* cell, GdkRectangle* rect);
       
        void gtk_widget_set_device_events(GtkWidget* widget, GdkDevice* device,
        GdkEventMask events);
       
    ...
    button.signal_clicked().connect(sigc::ptr_fun(&amp;on_button_clicked));
    ...
 
  $ git clone git://git.gnome.org/mm-common
  $ cp -a mm-common/skeletonmm libsomethingmm
 
# ./configure
# make
# make install
 
#include &lt;gtkmm/bin.h&gt;
#include &lt;gtkmm/activatable.h&gt;
_DEFS(gtkmm,gtk)
_PINCLUDE(gtkmm/private/bin_p.h)

namespace Gtk
{

class Button
  : public Bin,
    public Activatable
{
  _CLASS_GTKOBJECT(Button,GtkButton,GTK_BUTTON,Gtk::Bin,GtkBin)
  _IMPLEMENTS_INTERFACE(Activatable)
public:

  _CTOR_DEFAULT
  explicit Button(const Glib::ustring&amp; label, bool mnemonic = false);

  _WRAP_METHOD(void set_label(const Glib::ustring&amp; label), gtk_button_set_label)

  ...

  _WRAP_SIGNAL(void clicked(), "clicked")

  ...

  _WRAP_PROPERTY("label", Glib::ustring)
};

} // namespace Gtk
 
#include &lt;gtkmm/button.h&gt;

class OverriddenButton : public Gtk::Button
{
protected:
    virtual void on_clicked();
}

void OverriddenButton::on_clicked()
{
    std::cout &lt;&lt; "Hello World" &lt;&lt; std::endl;

    // call the base class's version of the method:
    Gtk::Button::on_clicked();
}
 
#include &lt;gtkmm/button.h&gt;

void on_button_clicked()
{
    std::cout &lt;&lt; "Hello World" &lt;&lt; std::endl;
}

main()
{
    Gtk::Button button("Hello World");
    button.signal_clicked().connect(sigc::ptr_fun(&amp;on_button_clicked));
}
 
#include &lt;gtkmm/button.h&gt;
#include &lt;gtkmm/window.h&gt;
class Foo : public Gtk::Window
{
private:
  Gtk::Button theButton;
  // will be destroyed when the Foo object is destroyed
};
 
#include &lt;libsomething.h&gt;

int main(int, char**)
{
  something_init();

  std::cout &lt;&lt; get_defs(SOME_TYPE_WIDGET)
            &lt;&lt; get_defs(SOME_TYPE_STUFF);
  return 0;
}
 
$ ./enum.pl /usr/include/gtk-3.0/gtk/*.h &gt; gtk_enums.defs
 
$ ./h2def.py /usr/include/gtk-3.0/gtk/*.h &gt; gtk_methods.defs
 
$ cd gtk/src
$ /usr/lib/glibmm-2.4/proc/gmmproc -I ../../tools/m4 --defs . button . ./../gtkmm
 
$ cd tools/extra_defs_gen
$ ./generate_extra_defs &gt; gtk_signals.defs
 
$ for f in $(find libsomethingmm -depth -name '*skeleton*'); do \
    d="${f%/*}"; b="${f##*/}"; mv "$f" "$d/${b//skeleton/libsomething}"; \
  done
 
&gt; gdb with_signal
(gdb) catch throw
Catchpoint 1 (throw)
(gdb) run
Catchpoint 1 (exception thrown), 0x00714ff0 in __cxa_throw ()
(gdb) backtrace
#0  0x00714ff0 in __cxa_throw () from /usr/lib/i386-linux-gnu/libstdc++.so.6
#1  0x08048bd4 in throwSomething () at with_signal.cc:6
(gdb) continue
Continuing.
(with_signal:2375): glibmm-ERROR **
unhandled exception (type unknown) in signal handler

Program received signal SIGTRAP, Trace/breakpoint trap.
 
&gt; gdb with_signal
(gdb) run
(with_signal:2703): glibmm-ERROR **:
unhandled exception (type unknown) in signal handler

Program received signal SIGTRAP, Trace/breakpoint trap.
(gdb) backtrace
#2  0x0063c6ab in glibmm_unexpected_exception () at exceptionhandler.cc:77
#3  Glib::exception_handlers_invoke () at exceptionhandler.cc:150
#4  0x0063d370 in glibmm_source_callback (data=0x804d620) at main.cc:212
#13 0x002e1b31 in Gtk::Application::run (this=0x804f300) at application.cc:178
#14 0x08048ccc in main (argc=1, argv=0xbfffecd4) at with_signal.cc:16
 
&gt; gdb without_signal
(gdb) run
terminate called after throwing an instance of 'char const*'

Program received signal SIGABRT, Aborted.
(gdb) backtrace
#7  0x08048864 in throwSomething () at without_signal.cc:6
#8  0x0804887d in main (argc=1, argv=0xbfffecd4) at without_signal.cc:12
 
(gdb) catch throw
(gdb) commands
(gdb)   backtrace
(gdb)   continue
(gdb)   end
(gdb) set pagination off
(gdb) run
 
// _MEMBER_GET_PTR(engine_lang, lang_engine, EngineLang*, PangoEngineLang*)
// It's just a comment. It's difficult to find a real-world example.
 
// in a class that inherits from Gtk::Window...
Glib::RefPtr&lt;PrintOperation&gt; op = PrintOperation::create();
// ...set up op...
op-&gt;run(Gtk::PRINT_OPERATION_ACTION_PREVIEW, *this);
 
// in class ExampleWindow's method...
Glib::RefPtr&lt;PrintOperation&gt; op = PrintOperation::create();
// ...set up op...
op-&gt;signal_done().connect(sigc::bind(sigc::mem_fun(*this, &amp;ExampleWindow::on_printoperation_done), op));
// run the op
 
// with_signal.cc
#include &lt;gtkmm.h&gt;

bool throwSomething()
{
  throw "Something";
  return true;
}

int main(int argc, char** argv)
{
  Glib::signal_timeout().connect(sigc::ptr_fun(throwSomething), 500);
  Glib::RefPtr&lt;Gtk::Application&gt; app =
    Gtk::Application::create(argc, argv, "org.gtkmm.with_signal");
  app-&gt;hold();
  return app-&gt;run();
}
 
// without_signal.cc
#include &lt;gtkmm.h&gt;

bool throwSomething()
{
  throw "Something";
  return true;
}

int main(int argc, char** argv)
{
  throwSomething();
  Glib::RefPtr&lt;Gtk::Application&gt; app =
    Gtk::Application::create(argc, argv, "org.gtkmm.without_signal");
  return app-&gt;run();
}
 
//Within a class that inherits from Gtk::Window and keeps m_refPageSetup and m_refSettings as members...
Glib::RefPtr&lt;Gtk::PageSetup&gt; new_page_setup = Gtk::run_page_setup_dialog(*this, m_refPageSetup, m_refSettings);
m_refPageSetup = new_page_setup;
 
Button::Button(const Glib::ustring&amp; label, bool mnemonic)
:
  _CONSTRUCT("label", label.c_str(), "use_underline", gboolean(mnemonic))
{}
 
DerivedDialog* pDialog = 0;
builder-&gt;get_widget_derived("DialogBasic", pDialog);
 
DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Dialog(cobject)
{
}
 
DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Dialog(cobject),
  m_builder(builder),
  m_pButton(0)
{
  //Get the Glade-instantiated Button, and connect a signal handler:
  m_builder-&gt;get_widget("quit_button", m_pButton);
  if(m_pButton)
  {
    m_pButton-&gt;signal_clicked().connect( sigc::mem_fun(*this, &amp;DerivedDialog::on_button_quit) );
  }
}
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; pixbuf = Gdk::Pixbuf::create_from_file(filename);
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; pixbuf2 = pixbuf;
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
Gdk::Pixbuf&amp; underlying = *refPixbuf; //Syntax error - will not compile.
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf2 = refPixbuf;
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
int width = refPixbuf-&gt;get_width();
 
Glib::RefPtr&lt;Gtk::Builder&gt; builder = Gtk::Builder::create_from_file("basic.glade");
 
Glib::RefPtr&lt;Gtk::Builder&gt; builder = Gtk::Builder::create_from_file("basic.glade", "treeview_products");
 
Glib::RefPtr&lt;Gtk::PrintOperation&gt; op = Gtk::PrintOperation::create();
// ...set up op...
op-&gt;set_export_filename("test.pdf");
Gtk::PrintOperationResult res = op-&gt;run(Gtk::PRINT_OPERATION_ACTION_EXPORT);
 
Glib::RefPtr&lt;Gtk::TreeModel&gt; refModel = m_TreeView.get_model();
if(refModel)
{
  int cols_count = refModel-&gt;get_n_columns();
  ...
}
 
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore =
Glib::RefPtr&lt;Gtk::TreeStore&gt;::cast_dynamic(refModel);
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore2 =
Glib::RefPtr&lt;Gtk::TreeStore&gt;::cast_static(refModel);
 
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore = Gtk::TreeStore::create(columns);
Glib::RefPtr&lt;Gtk::TreeModel&gt; refModel = refStore;
 
Glib::SignalProxy1&lt;bool, Gtk::DirectionType&gt; signal_focus()
 
Glib::SignalProxy3&lt;void, const TextBuffer::iterator&amp;, const Glib::ustrin&amp;, int&gt; signal_insert();
 
Gtk::Button* button = new Gtk::Button("example");
gtk_button_do_something_new(button-&gt;gobj());
 
Gtk::Button* pButton = new Gtk::Button("Test");

// do something useful with pButton

delete pButton;
 
Gtk::Dialog* pDialog = 0;
builder-&gt;get_widget("DialogBasic", pDialog);
 
Gtk::ToolButton* button = Gtk::manage(new Gtk::ToolButton(icon, "Big"));
button-&gt;set_tooltip_text("Big Brush);
group_brushes-&gt;insert(*button);
 
Gtk::ToolItemGroup* group_brushes =
  Gtk::manage(new Gtk::ToolItemGroup("Brushes"));
m_ToolPalette.add(*group_brushes);
 
Gtk::Widget* CustomPrintOperation::on_create_custom_widget()
{
  set_custom_tab_label("My custom tab");

  Gtk::Box* hbox = new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 8);
  hbox-&gt;set_border_width(6);

  Gtk::Label* label = Gtk::manage(new Gtk::Label("Enter some text: "));
  hbox-&gt;pack_start(*label, false, false);
  label-&gt;show();

  hbox-&gt;pack_start(m_Entry, false, false);
  m_Entry.show();

  return hbox;
}

void CustomPrintOperation::on_custom_widget_apply(Gtk::Widget* /* widget */)
{
  Glib::ustring user_input = m_Entry.get_text();
  //...
}
 
GtkButton* cbutton = get_a_button();
Gtk::Button* button = Glib::wrap(cbutton);
 
GtkWidget* example_widget_new(int something, const char* thing)
{
        ExampleWidget* widget;
        widget = g_object_new (EXAMPLE_TYPE_WIDGET, NULL);
        example_widget_construct(widget, "something", something, "thing", thing);
}

void example_widget_construct(ExampleWidget* widget, int something, const char* thing)
{
        //Do stuff that uses private API:
        widget-&gt;priv-&gt;thing = thing;
        do_something(something);
}
 
GtkWidget* example_widget_new(int something, const char* thing)
{
        return g_object_new (EXAMPLE_TYPE_WIDGET, "something", something, "thing", thing, NULL);
}
 
MyContainer::MyContainer()
{
  Gtk::Button* pButton = Gtk::manage(new Gtk::Button("Test"));
  add(*pButton); //add *pButton to MyContainer
}
 
_CLASS_BOXEDTYPE(RGBA, GdkRGBA, NONE, gdk_rgba_copy, gdk_rgba_free)
 
_CLASS_BOXEDTYPE_STATIC(Rectangle, GdkRectangle)
 
_CLASS_GENERIC(AttrIter, PangoAttrIterator)
 
_CLASS_GOBJECT(AccelGroup, GtkAccelGroup, GTK_ACCEL_GROUP, Glib::Object, GObject)
 
_CLASS_GTKOBJECT(Button, GtkButton, GTK_BUTTON, Gtk::Bin, GtkBin)
 
_CLASS_INTERFACE(CellEditable, GtkCellEditable, GTK_CELL_EDITABLE, GtkCellEditableIface)
 
_CLASS_INTERFACE(LoadableIcon, GLoadableIcon, G_LOADABLE_ICON, GLoadableIconIface, Icon, GIcon)
 
_CLASS_OPAQUE_COPYABLE(Checksum, GChecksum, NONE, g_checksum_copy, g_checksum_free)
 
_CLASS_OPAQUE_REFCOUNTED(Coverage, PangoCoverage, pango_coverage_new, pango_coverage_ref, pango_coverage_unref)
 
_CONVERSION(`GtkTreeView*',`TreeView*',`Glib::wrap($3)')
 
_CONVERSION(`PrintSettings&amp;',`GtkPrintSettings*',__FR2P)
_CONVERSION(`const PrintSettings&amp;',`GtkPrintSettings*',__FCR2P)
_CONVERSION(`const Glib::RefPtr&lt;Printer&gt;&amp;',`GtkPrinter*',__CONVERT_REFPTR_TO_P($3))
 
_IGNORE(gtk_button_box_set_spacing, gtk_button_box_get_spacing)
 
_IMPLEMENTS_INTERFACE(Activatable)
 
_INITIALIZATION(`Gtk::Widget&amp;',`GtkWidget*',`$3 = Glib::wrap($4)')
 
_MEMBER_GET_GOBJECT(layout, layout, Pango::Layout, PangoLayout*)
 
_WRAP_ENUM(IconLookupFlags, GtkIconLookupFlags, NO_GTYPE)
 
_WRAP_ENUM(WindowType, GtkWindowType)
 
_WRAP_GERROR(PixbufError, GdkPixbufError, GDK_PIXBUF_ERROR)
 
_WRAP_METHOD(void set_text(const Glib::ustring&amp; text), gtk_entry_set_text)
 
_WRAP_METHOD_DOCS_ONLY(gtk_container_remove)
 
_WRAP_PROPERTY("label", Glib::ustring)
 
_WRAP_SIGNAL(void clicked(),"clicked")
 
_WRAP_VFUNC(SizeRequestMode get_request_mode() const, get_request_mode)
 
bool MyCallback() { std::cout &lt;&lt; "Hello World!\n" &lt;&lt; std::endl; return true; }
 
bool idleFunc();
 
bool input_callback(Glib::IOCondition condition);
 
bool on_button_press(GdkEventButton* event);
Gtk::Button button("label");
button.signal_button_press_event().connect( sigc::ptr_fun(&amp;on_button_press) );
 
bool on_key_press_or_release_event(GdkEventKey* event)
{
  if (event-&gt;type == GDK_KEY_PRESS &amp;&amp;
    event-&gt;keyval == GDK_KEY_1 &amp;&amp;
    (event-&gt;state &amp; (GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK)) == GDK_MOD1_MASK)
  {
    handle_alt_1_press(); // GDK_MOD1_MASK is normally the Alt key
    return true;
  }
  return false;
}

Gtk::Entry m_entry; // in a class definition

// in the class constructor
m_entry.signal_key_press_event().connect( sigc::ptr_fun(&amp;on_key_press_or_release_event) );
m_entry.signal_key_release_event().connect( sigc::ptr_fun(&amp;on_key_press_or_release_event) );
m_entry.add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);
 
button.signal_button_press_event().connect( sigc::ptr_fun(&amp;on_mywindow_button_press), false );
 
class Server
{
public:
  //signal accessor:
  typedef sigc::signal&lt;void, bool, int&gt; type_signal_something;
  type_signal_something signal_something();

protected:
  type_signal_something m_signal_something;
};

Server::type_signal_something Server::signal_something()
{
  return m_signal_something;
}
 
class TextMark : public Glib::Object
{
  _CLASS_GOBJECT(TextMark, GtkTextMark, GTK_TEXT_MARK, Glib::Object, GObject)

protected:
  _WRAP_CTOR(TextMark(const Glib::ustring&amp; name, bool left_gravity = true), gtk_text_mark_new)

public:
  _WRAP_CREATE(const Glib::ustring&amp; name, bool left_gravity = true)
 
example-widget.h:56: error: using typedef-name 'ExampleWidget' after 'struct'
../../libexample/libexamplemm/example-widget.h:34: error: 'ExampleWidget' has a previous declaration here
make[4]: *** [example-widget.lo] Error 1
 
example-widget.h:60: error: '_ExampleWidget ExampleWidget' redeclared as different kind of symbol
../../libexample/libexamplemm/example-widget.h:34: error: previous declaration of 'typedef struct _ExampleWidget ExampleWidget'
 
int width = 0;
if(pixbuf)
{
  width = pixbuf-&gt;get_width();
}
 
m_button1.signal_clicked().connect( sigc::bind&lt;Glib::ustring&gt;( sigc::mem_fun(*this, &amp;HelloWorld::on_button_clicked), "button 1") );
 
my_connection.disconnect();
 
server.signal_something().connect(
  sigc::mem_fun(client, &amp;Client::on_server_something) );
 
sigc::connection  Glib::SignalIdle::connect(const sigc::slot&lt;bool&gt;&amp; slot,
                                    int priority = Glib::PRIORITY_DEFAULT_IDLE);
 
sigc::connection Glib::SignalIO::connect(const sigc::slot&lt;bool,Glib::IOCondition&gt;&amp; slot,
                                 int fd, Glib::IOCondition condition,
                                 int priority = Glib::PRIORITY_DEFAULT);
 
sigc::connection Glib::SignalTimeout::connect(const sigc::slot&lt;bool&gt;&amp; slot,
                                      unsigned int interval, int priority = Glib::PRIORITY_DEFAULT);
 
sigc::signal&lt;void, bool, int&gt; signal_something;
 
sigc::signal&lt;void,int&gt;::iterator signal&lt;void,int&gt;::connect( const sigc::slot&lt;void,int&gt;&amp; );
 
std::list&lt; Glib::RefPtr&lt;Gdk::Pixbuf&gt; &gt; listPixbufs;
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
listPixbufs.push_back(refPixbuf);
 
typedef struct _ExampleWidget ExampleWidget;

struct _ExampleWidget
{
  ...
};
 
virtual void on_button_clicked(Glib::ustring data);
 
void ExampleWindow::on_printoperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr&lt;PrintOperation&gt;&amp; op)
{
  if (result == Gtk::PRINT_OPERATION_RESULT_ERROR)
    //notify user
  else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY)
    //Update PrintSettings with the ones used in this PrintOperation

  if (! op-&gt;is_finished())
    op-&gt;signal_status_changed().connect(sigc::bind(sigc::mem_fun(*this, &amp;ExampleWindow::on_printoperation_status_changed), op));
}
 
void ExampleWindow::on_printoperation_status_changed(const Glib::RefPtr&lt;PrintFormOperation&gt;&amp; op)
{
  if (op-&gt;is_finished())
    //the print job is finished
  else
    //get the status with get_status() or get_status_string()

  //update UI
}
 
void init()
{
  Gtk::Main::init_gtkmm_internals(); //Sets up the g type system and the Glib::wrap() table.
  wrap_init(); //Tells the Glib::wrap() table about the libsomethingmm classes.
}
 
void on_button_clicked();

class some_class
{
    void on_button_clicked();
};

some_class some_object;

main()
{
    Gtk::Button button;
    button.signal_clicked().connect( sigc::ptr_fun(&amp;on_button_clicked) );
    button.signal_clicked().connect( sigc::mem_fun(some_object, &amp;some_class::on_button_clicked) );
}
 
void on_insert(const TextBuffer::iterator&amp; pos, const Glib::ustring&amp; text, int bytes)
 
{
  Gtk::Button aButton;
  aButton.show();
  ...
  app-&gt;run();
}
 "Hidden" Columns # keep this file sorted alphabetically, one language code per line
de
ja #include &lt;gtkmm.h&gt; #m4 _CONVERSION(`GSList*',`std::vector&lt;Widget*&gt;',`Glib::SListHandler&lt;Widget*&gt;::slist_to_vector($3, Glib::OWNERSHIP_SHALLOW)') $ ./plug &amp; $ ./socket ./docextract_to_xml.py -s ~/checkout/gnome/gtk+/gtk/ &gt; gtk_docs.xml
 // creates its own adjustments
Gtk::TextView textview;
// uses the newly-created adjustment for the scrollbar as well
Gtk::Scrollbar vscrollbar (textview.get_vadjustment(), Gtk::ORIENTATION_VERTICAL); // note to translators: don't translate the "[noun]" part - it is
// just here to distinguish the string from another "jumps" string
text = strip(gettext("jumps[noun]"), "[noun]"); //compiler error - no conversion from ustring to int.
int number = row[m_Columns.m_col_text]; 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010 <application>gtkmm</application> 3 added some new classes: <application>gtkmm</application> and Win32 <application>gtkmm</application> compared to Qt <application>gtkmm</application> is a wrapper <application>gtkmm</application> provides four basic types of buttons: <classname>Gtk::ToolItem</classname>s can then be added to the group. For instance, like so: <filename>libsomethingmm</filename>: Contains generated and hand-written .h and .cc files. <filename>libsomethingmm</filename>: The top-level directory. <filename>private</filename>: Contains generated <filename>*_p.h</filename> files. <filename>src</filename>: Contains .hg and .ccg source files. <function>_WRAP_METHOD()</function>, <function>_WRAP_SIGNAL()</function>, and <function>_WRAP_PROPERTY()</function> <literal>drag_begin</literal>: Provides DragContext. <literal>drag_end</literal>: Provides DragContext. <literal>lower</literal>: lower range value <literal>page_increment</literal>: value to increment/decrement when pressing mouse button 2 on a button <literal>page_size</literal>: unused <literal>step_increment</literal>: value to increment/decrement when pressing mouse button 1 on a button <literal>upper</literal>: upper range value <methodname>get_preferred_height_for_width_vfunc()</methodname>: Calculate the minimum and natural height of the container, if it would be given the specified width. <methodname>get_preferred_height_vfunc()</methodname>: Calculate the minimum and natural height of the container. <methodname>get_preferred_height_vfunc()</methodname>: Calculate the minimum and natural height of the widget. <methodname>get_preferred_width_for_height_vfunc()</methodname>: Calculate the minimum and natural width of the container, if it would be given the specified height. <methodname>get_preferred_width_vfunc()</methodname>: Calculate the minimum and natural width of the container. <methodname>get_preferred_width_vfunc()</methodname>: Calculate the minimum and natural width of the widget. <methodname>get_request_mode_vfunc()</methodname>: (optional) Return what <literal>Gtk::SizeRequestMode</literal> is preferred by the widget. <methodname>get_request_mode_vfunc()</methodname>: Return what <literal>Gtk::SizeRequestMode</literal> is preferred by the container. <methodname>on_add()</methodname>: Add a child widget to the container. <methodname>on_map()</methodname>: (optional) <methodname>on_remove()</methodname>: Remove a child widget from the container. <methodname>on_unmap()</methodname>: (optional) <placeholder-1/> (or <placeholder-2/> for filenames) <placeholder-1/> example package <varname>description</varname>: A short description of the resource as a UTF-8 encoded string <varname>mime_type</varname>: The MIME type of the resource A <classname>ProgressBar</classname> is horizontal and left-to-right by default, but you can change it to a vertical progress bar by using the <methodname>set_orientation()</methodname> method. A smartpointer acts much like a normal pointer. Here are a few examples. AM_CPPFLAGS AM_CPPFLAGS = ... -DPROGRAMNAME_LOCALEDIR=\"${PROGRAMNAME_LOCALEDIR}\" AM_CXXFLAGS ATK AboutDialog Accessing widgets Actions Activity Mode Adding Items to the List of Recent Files Adding Rows Adding child rows Adding widgets Adjustments After starting <filename>socket</filename>, you should see the following output in the terminal: After which you should see something like the following: Ahlstedt Alignment An improved Hello World Anastasov Application Lifetime Applying Tags AspectFrame Assistant Assuming the displayed size of strings Asynchronous operations Basic Type equivalents Basic Types Basics Before attempting to install <application>gtkmm</application> 3.0, you might first need to install these other packages. Bernhard Binding extra arguments Bjarne Stroustrup, "The C++ Programming Language" Forth Edition - section 34.3 Boxes Building <application>gtkmm</application> on Win32 Button ButtonBox ButtonBoxes Buttons C programmers use <function>sprintf()</function> to compose and concatenate strings. C++ favours streams, but unfortunately, this approach makes translation difficult, because each fragment of text is translated separately, without allowing the translators to rearrange them according to the grammar of the language. C type C++ type Cairo and Pango Call <methodname>show()</methodname> to display the widget. Changes in <application>gtkmm</application> 3 Changing the selection Chapter on "Drawing with Cairo". Chapter on "Printing". Chapter on Keyboard Events. CheckButton Checkboxes Chris Class macros Clipboard - Ideal Clipboard - Simple ColorChooserDialog Combo Boxes ComboBox ComboBox with Entry ComboBox with an Entry ComboBoxText ComboBoxText with Entry Comparison with other signalling systems Composition of strings Connecting Plugs and Sockets Connecting signal handlers Constructor macros Constructors Container Widgets Copy Copying Copying the skeleton project Creating .hg and .ccg files Creating your own signals Cumming Currently, <application>gettext</application> does not support non-ASCII characters (i.e. any characters with a code above 127) in source code. For instance, you cannot use the copyright sign (©). Custom Container Custom Containers Custom Widget Custom Widgets DISTCLEANFILES = ... intltool-extract \
                 intltool-merge \
                 intltool-update \
                 po/.intltool-merge-cache Daniel David Default formatting Dependencies Dereferencing Designers without programming skills can create and edit UIs. Dialogs Different join types in Cairo Disconnecting signal handlers Documentation Documentation build structure Drag and Drop DragContext Drawing Arcs and Circles Drawing Area - Arcs Drawing Area - Image Drawing Area - Lines Drawing Area - Text Drawing Area - Thin Lines Drawing Curved Lines Drawing Images Drawing Straight Lines Drawing Text Drawing Text with Pango Drawing thin lines Drawing with relative coordinates Dynamic allocation with manage() and add() Dynamic allocation with new and delete Editable Cells Elstner Emitted when the button is pressed and released. Emitted when the button is pressed. Emitted when the button is released. Emitted when the mouse pointer leaves the button's window. Emitted when the mouse pointer moves over the button's window. Entry Entry Completion Entry Completion Example Entry with Icon Entry with Progress Bar Enumerations. EventBox Example Example Application: Creating a Clock with Cairo Examples Exceptions in signal handlers Export to PDF Extending the print dialog Ferreira FileChooser FileChooserDialog Filtering Recent Files Finally, check the status. For instance, <placeholder-1/> FontChooserDialog For each page that needs to be rendered, the following signals are emitted: <placeholder-1/> For example, for <classname>Pango::Analysis</classname> in <filename>item.hg</filename>: <placeholder-1/> For example, from <filename>icontheme.hg</filename>: <placeholder-1/> For example, in <filename>rectangle.hg</filename>: <placeholder-1/> For example, in Pangomm, <filename>layoutline.hg</filename>: <placeholder-1/> For instance, For instance, <placeholder-1/> For instance, from <classname>Gdk::RGBA</classname>: <placeholder-1/> For instance, from <classname>Glib::Checksum</classname>: <placeholder-1/> For instance, from <filename>accelgroup.hg</filename>: <placeholder-1/> For instance, from <filename>entry.hg</filename>: <placeholder-1/> For instance, from <filename>enums.hg</filename>: <placeholder-1/> For instance, this code would be problematic: For instance: For more detailed information about signals, see the <link linkend="chapter-signals">appendix</link>. For single-selection, you can just call <methodname>get_selected()</methodname>, like so: Frame Full Example GTK+ 3.0 Generating the .defs files. Generating the enums .defs Generating the methods .defs Getting help with translations Getting values Glade and Gtk::Builder Glib::RefPtr&lt;Gtk::Adjustment&gt; Gtk::Adjustment::create(
  double value,
  double lower,
  double upper,
  double step_increment = 1,
  double page_increment = 10,
  double page_size = 0); Glib::RefPtr&lt;Gtk::Application&gt; app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base"); Glib::RefPtr&lt;Gtk::Clipboard&gt; refClipboard = Gtk::Clipboard::get();

//Targets:
std::vector&lt;Gtk::TargetEntry&gt; targets;
targets.push_back( Gtk::TargetEntry("example_custom_target") );
targets.push_back( Gtk::TargetEntry("UTF8_STRING") );

refClipboard-&gt;set( targets,
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_get),
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_clear) ); Glib::RefPtr&lt;Gtk::ListStore&gt; refListStore =
    Gtk::ListStore::create(m_Columns); Glib::RefPtr&lt;Gtk::RecentInfo&gt; info;
try
{
  info = recent_manager-&gt;lookup_item(uri);
}
catch(const Gtk::RecentManagerError&amp; ex)
{
  std::cerr &lt;&lt; "RecentManagerError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
}
if (info)
{
  // item was found
} Glib::RefPtr&lt;Gtk::RecentManager&gt; recent_manager = Gtk::RecentManager::get_default();
recent_manager-&gt;add_item(uri); Glib::RefPtr&lt;Gtk::TextBuffer::Mark&gt; refMark =
    refBuffer-&gt;create_mark(iter); Glib::RefPtr&lt;Gtk::TextBuffer::Tag&gt; refTagMatch =
    Gtk::TextBuffer::Tag::create();
refTagMatch-&gt;property_background() = "orange"; Glib::RefPtr&lt;Gtk::TextBuffer::TagTable&gt; refTagTable =
    Gtk::TextBuffer::TagTable::create();
refTagTable-&gt;add(refTagMatch);
//Hopefully a future version of <application>gtkmm</application> will have a set_tag_table() method,
//for use after creation of the buffer.
Glib::RefPtr&lt;Gtk::TextBuffer&gt; refBuffer =
    Gtk::TextBuffer::create(refTagTable); Glib::RefPtr&lt;Gtk::TextChildAnchor&gt; refAnchor =
    refBuffer-&gt;create_child_anchor(iter); Glib::RefPtr&lt;Gtk::TreeModelSort&gt; sorted_model =
    Gtk::TreeModelSort::create(model);
sorted_model-&gt;set_sort_column(columns.m_col_name, Gtk::SORT_ASCENDING);
treeview.set_model(sorted_model); Glib::RefPtr&lt;Gtk::TreeSelection&gt; refTreeSelection =
    m_TreeView.get_selection(); Glib::RefPtr&lt;Gtk::UIManager&gt; m_refUIManager =
    Gtk::UIManager::create();
m_refUIManager-&gt;insert_action_group(m_refActionGroup);
add_accel_group(m_refUIManager-&gt;get_accel_group()); Glib::ustring Glib::ustring and std::iostreams Glib::ustring strText = row[m_Columns.m_col_text];
int number = row[m_Columns.m_col_number]; Glib::ustring ui_info =
    "&lt;ui&gt;"
    "  &lt;menubar name='MenuBar'&gt;"
    "    &lt;menu action='MenuFile'&gt;"
    "      &lt;menuitem action='New'/&gt;"
    "      &lt;menuitem action='Open'/&gt;"
    "      &lt;separator/&gt;"
    "      &lt;menuitem action='Quit'/&gt;"
    "    &lt;/menu&gt;"
    "    &lt;menu action='MenuEdit'&gt;"
    "      &lt;menuitem action='Cut'/&gt;"
    "      &lt;menuitem action='Copy'/&gt;"
    "      &lt;menuitem action='Paste'/&gt;"
    "    &lt;/menu&gt;"
    "  &lt;/menubar&gt;"
    "  &lt;toolbar  name='ToolBar'&gt;"
    "    &lt;toolitem action='Open'/&gt;"
    "    &lt;toolitem action='Quit'/&gt;"
    "  &lt;/toolbar&gt;"
    "&lt;/ui&gt;";

m_refUIManager-&gt;add_ui_from_string(ui_info); Glib::ustring ui_info =
    "&lt;ui&gt;"
    "  &lt;popup name='PopupMenu'&gt;"
    "    &lt;menuitem action='ContextEdit'/&gt;"
    "    &lt;menuitem action='ContextProcess'/&gt;"
    "    &lt;menuitem action='ContextRemove'/&gt;"
    "  &lt;/popup&gt;"
    "&lt;/ui&gt;";

m_refUIManager-&gt;add_ui_from_string(ui_info); Groups Gtk::Alignment
Gtk::Arrow
Gtk::AspectFrame
Gtk::Bin
Gtk::Box
Gtk::Button
Gtk::CheckButton
Gtk::Fixed
Gtk::Frame
Gtk::Grid
Gtk::Image
Gtk::Label
Gtk::MenuItem
Gtk::Notebook
Gtk::Paned
Gtk::RadioButton
Gtk::Range
Gtk::ScrolledWindow
Gtk::Separator
Gtk::Table (deprecated from <application>gtkmm</application> version 3.4)
Gtk::Toolbar Gtk::Application and command-line options Gtk::Box(Gtk::Orientation orientation = Gtk::ORIENTATION_HORIZONTAL, int spacing = 0);
void set_spacing(int spacing);
void set_homogeneous(bool homogeneous = true); Gtk::Button* pButton = new Gtk::Button("_Something", true); Gtk::CellRendererToggle* pRenderer =
    Gtk::manage( new Gtk::CellRendererToggle() );
pRenderer-&gt;signal_toggled().connect(
    sigc::bind( sigc::mem_fun(*this,
        &amp;Example_TreeView_TreeStore::on_cell_toggled), m_columns.dave)
); Gtk::DrawingArea myArea;
Cairo::RefPtr&lt;Cairo::Context&gt; myContext = myArea.get_window()-&gt;create_cairo_context();
myContext-&gt;set_source_rgb(1.0, 0.0, 0.0);
myContext-&gt;set_line_width(2.0); Gtk::Entry* entry = m_Combo.get_entry();
if (entry)
{
  // The Entry shall receive focus-out events.
  entry-&gt;add_events(Gdk::FOCUS_CHANGE_MASK);

  // Alternatively you can connect to m_Combo.signal_changed().
  entry-&gt;signal_changed().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_changed) );

  entry-&gt;signal_activate().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_activate) );

  entry-&gt;signal_focus_out_event().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_focus_out_event) );
} Gtk::EventBox(); Gtk::TreeModel::Children children = row.children(); Gtk::TreeModel::Row row = *iter; Gtk::TreeModel::Row row = m_refModel-&gt;children()[5]; //The fifth row.
if(row)
  refTreeSelection-&gt;select(row); Gtk::TreeModel::iterator iter = m_Combo.get_active();
if(iter)
{
  Gtk::TreeModel::Row row = *iter;

  //Get the data for the selected row, using our knowledge
  //of the tree model:
  int id = row[m_Columns.m_col_id];
  set_something_id_chosen(id); //Your own function.
}
else
  set_nothing_chosen(); //Your own function. Gtk::TreeModel::iterator iter = m_refListStore-&gt;append(); Gtk::TreeModel::iterator iter = m_refModel-&gt;children().begin()
if(iter)
  refTreeSelection-&gt;select(iter); Gtk::TreeModel::iterator iter_child =
    m_refTreeStore-&gt;append(row.children()); Gtk::TreeView::Column* pColumn =
  Gtk::manage(new Gtk::TreeView::Column("Icon Name"));

// m_columns.icon and m_columns.iconname are columns in the model.
// pColumn is the column in the TreeView:
pColumn-&gt;pack_start(m_columns.icon, /* expand= */ false);
pColumn-&gt;pack_start(m_columns.iconname);

m_TreeView.append_column(*pColumn); Gtk::TreeView::Column* pColumn = treeview.get_column(0);
if(pColumn)
  pColumn-&gt;set_sort_column(m_columns.m_col_id); Gtk::Widget* pMenubar = m_refUIManager-&gt;get_widget("/MenuBar");
pBox-&gt;add(*pMenuBar, Gtk::PACK_SHRINK); Gtk::Window window;
window.set_default_size(200, 200); Hand-coded source files Hand-coding constructors Handling <literal>button_press_event</literal> Hello World Hello World 2 Hello World in <application>gtkmm</application> HelloWorld::HelloWorld()
:
  m_button ("Hello World")
{
  set_border_width(10);
  m_button.signal_clicked().connect(sigc::mem_fun(*this,
    &amp;HelloWorld::on_button_clicked));
  add(m_button);.
  m_button.show();
} Here is a list of some of these Widgets: Here is an example callback method: Here's a simple example: <placeholder-1/> Here's a slightly larger example of slots in action: Here's an example of a <classname>SpinButton</classname> in action: Here's an example of this technique: How gettext works How to use Git for GNOME translators INTLTOOL_FILES = intltool-extract.in \
                 intltool-merge.in \
                 intltool-update.in IT_PROG_INTLTOOL([0.35.0])

GETTEXT_PACKAGE=programname
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"],
                   [The domain to use with gettext])
AM_GLIB_GNU_GETTEXT

PROGRAMNAME_LOCALEDIR=[${datadir}/locale]
AC_SUBST(PROGRAMNAME_LOCALEDIR) Ideal Idle Functions In <filename>configure.ac</filename>, <placeholder-1/> In the top-level Makefile.am: <placeholder-1/> Includes the other files. Independently sorted views of the same model InfoBar Initialization Installation Installing <application>gtkmm</application> with <application>jhbuild</application> Installing From Source Installing and Using the git version of <application>gtkmm</application> Internationalization and Localization Intltool README Introduction It also takes an optional extra argument: <placeholder-1/> Jonathon Jongsma Keyboard Events Keyboard Events - Simple King Kjell L10N Guidelines for Developers Label Lack of properties Laursen Less C++ code is required. Line styles ListStore ListStore, for rows Loading the .glade file Main Menu Main Menu example Makefile.am files Managed Widgets Marking strings for translation Marko Marks Memory management Menus and Toolbars MessageDialog Method macros Methods Microsoft Windows Miscellaneous Widgets Mixing C and C++ APIs ModelColumns()
{ add(m_col_id); add(m_col_name); }

  Gtk::TreeModelColumn&lt;int&gt; m_col_id;
  Gtk::TreeModelColumn&lt;Glib::ustring&gt; m_col_name;
};

ModelColumns m_columns; Modifying build files Modifying the List of Recent Files Monitoring I/O Most of our examples use this technique. Move Multiple-item widgets Murray Murray Cumming Nicolai M. Josuttis, "The C++ Standard Library" - section 4.2 Non-modal AboutDialog Normal C++ memory management Note that you must specify actions for sub menus as well as menu items. Notebook Now let's look at the connection again: Objects and functions. Of course this means that you can store <classname>RefPtr</classname>s in standard containers, such as <classname>std::vector</classname> or <classname>std::list</classname>. Ole Other macros Overriding default signal handlers Overview PKG_CHECK_MODULES([MYAPP], [gtkmm-3.0 &gt;= 3.8.0]) Pack the widget into a container using the appropriate call, e.g. <methodname>Gtk::Container::add()</methodname> or <methodname>pack_start()</methodname>. Packing Page setup Pango Parts of chapter on "Internationalization". Parts of the update from gtkmm 2 to gtkmm 3. Paste Pedro Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. You may obtain a copy of the GNU Free Documentation License from the Free Software Foundation by visiting their Web site or by writing to: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Pitfalls Please see <ulink url="https://wiki.gnome.org/Projects/gtkmm/MSWindows/BuildingGtkmm"> https://wiki.gnome.org/Projects/gtkmm/MSWindows/BuildingGtkmm</ulink> for instructions on how to build gtkmm on Windows. Plugs Plugs and Sockets Plugs and Sockets Example Popup Context Menu Popup Menu Popup Menu example Popup Menus Prebuilt Packages Preparing your project Preventing row selection Preview PrintOperation Printing Printing - Simple Problems in the C API. Programming with <application>gtkmm</application> 3 Progress bars are used to show the status of an ongoing operation. For instance, a <classname>ProgressBar</classname> can show how much of a task has been completed. ProgressBar Puts the generated code in #ifdef blocks. Radio buttons RadioButton RecentChooser RecentManager Recently Used Documents Recommended Techniques Reference Rendering text Reorderable rows Resources Responding to changes Reusing C documentation Rieder Same strings, different semantics Scrollbar Widgets ScrolledWindow Scrolling Section on Gtk::Grid. Setting a prefix Setting up jhbuild Setting values Shared resources Signal Handler sequence Signals Signals and properties. Simple Simple Example Simple RecentChooserDialog example Simple Text Example Single or multiple selection So you should either avoid this situation or use <ulink url="http://developer.gnome.org/glibmm/unstable/classGlib_1_1ustring.html"><function>Glib::ustring::compose()</function></ulink> which supports syntax such as: <placeholder-1/> Sockets Sorting Sorting by clicking on columns Source Code Sources and Destinations Specifying CellRenderer details SpinButton Table TagTable Tags Tags and Formatting Targets Testing and adding translations TextView The "changed" signal The .hg and .ccg files The <classname>Gtk::Button</classname> widget has the following signals, but most of the time you will just handle the <literal>clicked</literal> signal: The <parameter>options</parameter> argument can take one of these three options: <placeholder-1/> The Buffer The Cairo Drawing Model The Clipboard The Drawing Area Widget The Model The RefPtr smartpointer The Selection The TreeView widget The View The arguments to <methodname>Action::create()</methodname> specify the action's name and how it will appear in menus and toolbars. The build structure The chosen item The command line options passed to the C preprocessor. The command line options passed to the C++ compiler. The constraints The entry The following example demonstrates the use of <classname>RadioButton</classname>s: The following example program requires a command-line option. The source code shows two ways of handling command-line options in combination with <classname>Gtk::Application</classname>. The macros are explained in more detail in the following sections. The macros in this example do the following: <placeholder-1/> The model The name of the library, such as libsomethingmm. The native GTK+ print dialog has a preview button, but you may also start a preview directly from an application: <placeholder-1/> The next two lines of code create a window and set its default (initial) size: The rules The selected rows The text column The window ID is: 69206019 Then start the <filename>socket</filename> program: There are a few common mistakes that you would discover eventually yourself. But this section might help you to avoid them. There are basically five different styles, as shown in this picture: There are some optional extra arguments: <placeholder-1/> There are two basic strategies that can be used: <placeholder-1/> There is a more complex example in examples/others/dnd. There is one optional extra argument: <placeholder-1/> These dependencies have their own dependencies, including the following applications and libraries: This book This book assumes a good understanding of C++, and how to create C++ programs. This example creates a button with a picture and a label. This example implements a widget which draws a Penrose triangle. This has the following advantages: <placeholder-1/> This is a full working example that defines and uses custom signals. This is demonstrated in the drag_and_drop example. This macro will be used when you initialize <literal>gettext</literal> in your source code. To add a new file to the list of recent documents, in the simplest case, you only need to provide the URI. For example: To begin our introduction to <application>gtkmm</application>, we'll start with the simplest program possible. This program will create an empty 200 x 200 pixel window. To change the selection, specify a <classname>Gtk::TreeModel::iterator</classname> or <classname>Gtk::TreeModel::Row</classname>, like so: To do this, you need to call the <methodname>pulse()</methodname> method at regular intervals. You can also choose the step size, with the <methodname>set_pulse_step()</methodname> method. Toggle buttons ToggleButton ToolItem Reference ToolItemGroup Reference ToolPalette ToolPalette Example ToolPalette Reference Tooltip Tooltip Reference Tooltips TreeModel::iterator iter = refTreeSelection-&gt;get_selected();
if(iter) //If anything is selected
{
  TreeModel::Row row = *iter;
  //Do something with the row.
} TreeModelSort Reference TreeSortable Reference TreeStore TreeStore, for a hierarchy TreeView - Drag And Drop TreeView - Editable Cells TreeView - ListStore TreeView - Popup Context Menu TreeView - TreeStore UIManager Unix and Linux Unusual words Update your <literal>DISTCLEANFILES</literal>: <placeholder-1/> Use <methodname>Gtk::Builder::get_widget_derived()</methodname> like so: <placeholder-1/> Useful methods Using Glib::Dispatcher Using a <application>gtkmm</application> widget Using a Model Using derived widgets Using non-ASCII characters in strings Using the git version of <application>gtkmm</application> Vine We will now explain each line of the example When the Button emits its <literal>clicked</literal> signal, <methodname>on_button_clicked()</methodname> will be called. Why use <application>gtkmm</application> instead of GTK+? Widget Reference Widgets Widgets Without X-Windows Widgets and ChildAnchors Working with gtkmm's Source Code Wrapping C Libraries with gmmproc Writing signal handlers Writing the vfuncs .defs X Event signals You can also specify a signal handler when calling <methodname>ActionGroup::add()</methodname>. This signal handler will be called when the action is activated via either a menu item or a toolbar button. You can copy <classname>RefPtr</classname>s, just like normal pointers. But unlike normal pointers, you don't need to worry about deleting the underlying instance. You never know how much space a string will take on screen when translated. It might very possibly be twice the size of the original English string. Luckily, most <application>gtkmm</application> widgets will expand at runtime to the required size. You should avoid cryptic abbreviations, slang, or jargon. They are usually difficult to translate, and are often difficult for even native speakers to understand. For instance, prefer "application" to "app" _CLASS_BOXEDTYPE _CLASS_BOXEDTYPE_STATIC _CLASS_BOXEDTYPE_STATIC( C++ class, C class ) _CLASS_GENERIC _CLASS_GENERIC( C++ class, C class ) _CLASS_GOBJECT _CLASS_GTKOBJECT _CLASS_GTKOBJECT() _CLASS_INTERFACE _CLASS_OPAQUE_COPYABLE _CLASS_OPAQUE_REFCOUNTED _CTOR_DEFAULT _DEFS() _IGNORE / _IGNORE_SIGNAL _IGNORE(C function name 1, C function name2, etc) _IGNORE_SIGNAL(C signal name 1, C signal name2, etc) _IMPLEMENTS_INTERFACE _IMPLEMENTS_INTERFACE() _IMPLEMENTS_INTERFACE(C++ interface name) _MEMBER_GET / _MEMBER_SET _MEMBER_GET(C++ name, C name, C++ type, C type) _MEMBER_GET(x, x, int, int) _MEMBER_GET_GOBJECT / _MEMBER_SET_GOBJECT _MEMBER_GET_GOBJECT(C++ name, C name, C++ type, C type) _MEMBER_GET_PTR / _MEMBER_SET_PTR _MEMBER_GET_PTR(C++ name, C name, C++ type, C type) _MEMBER_SET(C++ name, C name, C++ type, C type) _MEMBER_SET_GOBJECT(C++ name, C name, C++ type, C type) _MEMBER_SET_PTR(C++ name, C name, C++ type, C type) _PINCLUDE() _WRAP_CTOR _WRAP_ENUM _WRAP_ENUM_DOCS_ONLY _WRAP_GERROR _WRAP_METHOD _WRAP_METHOD( C++ method signature, C function name) _WRAP_METHOD_DOCS_ONLY _WRAP_METHOD_DOCS_ONLY(C function name) _WRAP_PROPERTY _WRAP_PROPERTY(C property name, C++ type) _WRAP_SIGNAL _WRAP_SIGNAL( C++ signal handler signature, C signal name) _WRAP_VFUNC _WRAP_VFUNC( C++ method signature, C function name) adj-&gt;signal_value_changed().connect(sigc::bind&lt;MyPicture*&gt;(sigc::mem_fun(*this,
    &amp;cb_rotate_picture), picture)); adjustment-&gt;signal_changed(); atkmm binding_name bindtextdomain(GETTEXT_PACKAGE, PROGRAMNAME_LOCALEDIR);
bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
textdomain(GETTEXT_PACKAGE); bool bool DemoWindow::select_function(
    const Glib::RefPtr&lt;Gtk::TreeModel&gt;&amp; model,
    const Gtk::TreeModel::Path&amp; path, bool)
{
  const Gtk::TreeModel::iterator iter = model-&gt;get_iter(path);
  return iter-&gt;children().empty(); // only allow leaf nodes to be selected
} bool ExampleWindow::on_button_press_event(GdkEventButton* event)
{
  if( (event-&gt;type == GDK_BUTTON_PRESS) &amp;&amp;
      (event-&gt;button == 3) )
  {
    m_Menu_Popup-&gt;popup(event-&gt;button, event-&gt;time);
    return true; //It has been handled.
  }
  else
    return false;
} bool MyArea::on_draw(const Cairo::RefPtr&lt;Cairo::Context&gt;&amp; cr)
{
  Glib::RefPtr&lt;Gdk::Pixbuf&gt; image = Gdk::Pixbuf::create_from_file("myimage.png");
  // Draw the image at 110, 90, except for the outermost 10 pixels.
  Gdk::Cairo::set_source_pixbuf(cr, image, 100, 80);
  cr-&gt;rectangle(110, 90, image-&gt;get_width()-20, image-&gt;get_height()-20);
  cr-&gt;fill();
  return true;
} buttons example cairo cairomm cell.property_editable() = true; class HelloWorld : public Gtk::Window
{

public:
  HelloWorld();
  virtual ~HelloWorld();

protected:
  //Signal handlers:
  virtual void on_button_clicked();

  //Member widgets:
  Gtk::Button m_button;
}; class RadioButtons : public Gtk::Window
{
public:
    RadioButtons();

protected:
    Gtk::RadioButton m_rb1, m_rb2, m_rb3;
};

RadioButtons::RadioButtons()
  : m_rb1("button1"),
    m_rb2("button2"),
    m_rb3("button3")
{
    Gtk::RadioButton::Group group = m_rb1.get_group();
    m_rb2.set_group(group);
    m_rb3.set_group(group);
} class RadioButtons : public Gtk::Window
{
public:
    RadioButtons();
};

RadioButtons::RadioButtons()
{
    Gtk::RadioButton::Group group;
    Gtk::RadioButton *m_rb1 = Gtk::manage(
      new Gtk::RadioButton(group,"button1"));
    Gtk::RadioButton *m_rb2 = manage(
      new Gtk::RadioButton(group,"button2"));
      Gtk::RadioButton *m_rb3 = manage(
        new Gtk::RadioButton(group,"button3"));
} clicked configure.ac constversion context-&gt;save();
context-&gt;translate(x, y);
context-&gt;scale(width / 2.0, height / 2.0);
context-&gt;arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
context-&gt;restore(); custom_c_callback custom_default_handler custom_vfunc custom_vfunc_callback deprecated display_message("Getting ready for i18n."); display_message(_("Getting ready for i18n.")); double enter enums errthrow event_box.add(child_widget); functions g++ simple.cc -o simple `pkg-config gtkmm-3.0 --cflags --libs` gboolean gchar* gdk-pixbuf gdouble gettext manual gint glib glibmm gmmproc Parameter Processing gnomemm_hello gtk.defs gtk_enums.defs gtk_methods.defs gtk_signals.defs gtk_vfuncs.defs gtkmm gtkmm_hello guint gunichar ifdef int int cols_count = m_TreeView.append_column_editable("Alex", m_columns.alex);
Gtk::TreeViewColumn* pColumn = m_TreeView.get_column(cols_count-1);
if(pColumn)
{
  Gtk::CellRendererToggle* pRenderer =
    static_cast&lt;Gtk::CellRendererToggle*&gt;(pColumn-&gt;get_first_cell());
  pColumn-&gt;add_attribute(pRenderer-&gt;property_visible(), m_columns.visible);
  pColumn-&gt;add_attribute(pRenderer-&gt;property_activatable(), m_columns.world); int main(int argc, char** argv)
{
  Glib::RefPtr&lt;Gtk::Application&gt; app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  HelloWorld helloworld;
  return app-&gt;run(helloworld);
} intltool-update --pot leave lib_LTLIBRARIES libsigc++ 2.0 m4 Conversions m4 Initializations m_TextView.add_child_at_anchor(m_Button, refAnchor); m_TreeView.append_column("Messages", m_Columns.m_col_text); m_TreeView.set_model(m_refListStore); m_box.pack_start(m_Button1);
m_box.pack_start(m_Button2); m_button1.signal_clicked().connect( sigc::mem_fun(*this,
  &amp;HelloWorld::on_button_clicked) ); m_combo.set_entry_text_column(m_columns.m_col_name); m_combo.signal_changed().connect( sigc::mem_fun(*this,
      &amp;ExampleWindow::on_combo_changed) ); m_frame.add(m_box); m_rb2.set_group(m_rb1.get_group()); //doesn't work m_refActionGroup = Gtk::ActionGroup::create();

m_refActionGroup-&gt;add( Gtk::Action::create("MenuFile", "_File") );
m_refActionGroup-&gt;add( Gtk::Action::create("New", "_New"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_new) );
m_refActionGroup-&gt;add( Gtk::Action::create("ExportData", "Export Data"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_open) );
m_refActionGroup-&gt;add( Gtk::Action::create("Quit", "_Quit"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_quit) ); m_refTreeSelection-&gt;set_select_function( sigc::mem_fun(*this,
    &amp;DemoWindow::select_function) ); modules = [ 'gtkmm' ] moduleset = 'gnome-suites-core-deps-3.12' no_default_handler no_slot_copy or pangomm pkg-config pressed properties refBuffer-&gt;apply_tag(refTagMatch, iterRangeStart, iterRangeStop); refBuffer-&gt;insert_with_tag(iter, "Some text", refTagMatch); refClipboard-&gt;request_contents("example_custom_target",
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_received) ); refClipboard-&gt;request_targets( sigc::mem_fun(*this,
    &amp;ExampleWindow::on_clipboard_received_targets) ); refTreeSelection-&gt;selected_foreach_iter(
    sigc::mem_fun(*this, &amp;TheClass::selected_row_callback) );

void TheClass::selected_row_callback(
    const Gtk::TreeModel::iterator&amp; iter)
{
  TreeModel::Row row = *iter;
  //Do something with the row.
} refTreeSelection-&gt;set_mode(Gtk::SELECTION_MULTIPLE); refTreeSelection-&gt;signal_changed().connect(
    sigc::mem_fun(*this, &amp;Example_IconTheme::on_selection_changed)
); refreturn refreturn_ctype released return app-&gt;run(window); row[m_Columns.m_col_text] = "sometext"; signals slot_callback slot_name src/main.cc
src/other.cc std::cout &lt;&lt; Glib::ustring::compose(
             _("Current amount: %1 Future: %2"), amount, future) &lt;&lt; std::endl;

label.set_text(Glib::ustring::compose(_("Really delete %1 now?"), filename)); std::cout &lt;&lt; _("Current amount: ") &lt;&lt; amount
          &lt;&lt; _(" Future: ") &lt;&lt; future &lt;&lt; std::endl;

label.set_text(_("Really delete ") + filename + _(" now?")); std::ostringstream output;
output.imbue(std::locale("")); // use the user's locale for this stream
output &lt;&lt; percentage &lt;&lt; " % done";
label-&gt;set_text(Glib::locale_to_utf8(output.str())); std::string std::vector&lt; Glib::RefPtr&lt;Gtk::RecentInfo&gt; &gt; info_list = recent_manager-&gt;get_items(); translator-credits typedef Gtk::TreeModel::Children type_children; //minimise code length.
type_children children = refModel-&gt;children();
for(type_children::iterator iter = children.begin();
    iter != children.end(); ++iter)
{
  Gtk::TreeModel::Row row = *iter;
  //Do something with the row - see above for set/get.
} vfuncs void ExampleWindow::on_button_delete()
{
  Glib::RefPtr&lt;Gtk::TreeSelection&gt; refTreeSelection =
      m_treeview.get_selection();
  if(refTreeSelection)
  {
    Gtk::TreeModel::iterator sorted_iter =
        m_refTreeSelection-&gt;get_selected();
    if(sorted_iter)
    {
      Gtk::TreeModel::iterator iter =
          m_refModelSort-&gt;convert_iter_to_child_iter(sorted_iter);
      m_refModel-&gt;erase(iter);
    }
  }
} void ExampleWindow::on_clipboard_get(
    Gtk::SelectionData&amp; selection_data, guint /* info */)
{
  const std::string target = selection_data.get_target();

  if(target == "example_custom_target")
    selection_data.set("example_custom_target", m_ClipboardStore);
} void ExampleWindow::on_clipboard_received(
    const Gtk::SelectionData&amp; selection_data)
{
  Glib::ustring clipboard_data = selection_data.get_data_as_string();
  //Do something with the pasted data.
} void ExampleWindow::on_clipboard_received_targets(
  const std::vector&lt;Glib::ustring&gt;&amp; targets)
{
  const bool bPasteIsPossible =
    std::find(targets.begin(), targets.end(),
      example_target_custom) != targets.end();

  // Enable/Disable the Paste button appropriately:
  m_Button_Paste.set_sensitive(bPasteIsPossible);
} void cb_rotate_picture (MyPicture* picture)
{
  picture-&gt;set_rotation(adj-&gt;get_value());
... void doSomething(const Cairo::RefPtr&lt;Cairo::Context&gt;&amp; context, int x)
{
    context-&gt;save();
    // change graphics state
    // perform drawing operations
    context-&gt;restore();
} void drag_dest_set(const std::vector&lt;Gtk::TargetEntry&gt;&amp; targets,
    Gtk::DestDefaults flags, Gdk::DragAction actions); void drag_source_set(const std::vector&lt;Gtk::TargetEntry&gt;&amp; targets,
      Gdk::ModifierType start_button_mask, Gdk::DragAction actions); void pack_start(Gtk::Widget&amp; child,
                Gtk::PackOptions options = Gtk::PACK_EXPAND_WIDGET,
                guint padding = 0); wrap_init_flags xgettext -a -o my-strings --omit-header *.cc *.h Project-Id-Version: gtkmm-documentation master
PO-Revision-Date: 2017-10-24 11:11+0200
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
 
        GtkSizeRequestMode gtk_widget_get_request_mode(GtkWidget* widget);
       
        GtkToolItem* gtk_tool_button_new(GtkWidget* icon_widget, const gchar*
        label);
       
        _INITIALIZATION(`Gdk::Rectangle&amp;',`GdkRectangle', `$3 =
        Glib::wrap(&amp;($4))')
       
        _INITIALIZATION(`SizeRequestMode&amp;',`GtkSizeRequestMode',`$3 =
        ($1)($4)')
       
        _INITIALIZATION(`SizeRequestMode&amp;',`GtkSizeRequestMode',`$3 =
        (SizeRequestMode)($4)')
       
        _WRAP_CTOR(ToolButton(Widget&amp; icon_widget, const Glib::ustring&amp;
        label{?}), gtk_tool_button_new)
       
        _WRAP_METHOD(bool get_cell_rect(const TreeModel::Path&amp; path, const
        CellRenderer&amp; cell, Gdk::Rectangle&amp; rect{&gt;&gt;}) const,
        gtk_icon_view_get_cell_rect)
       
        _WRAP_METHOD(void get_request_mode(SizeRequestMode&amp; mode{OUT})
        const, gtk_widget_get_request_mode)
       
        _WRAP_METHOD(void set_device_events(Gdk::EventMask events{.}, const
        Glib::RefPtr&lt;const Gdk::Device&gt;&amp; device{.}),
        gtk_widget_set_device_events)
       
        _WRAP_METHOD(void set_device_events(Gdk::EventMask events{events},
        const Glib::RefPtr&lt;const Gdk::Device&gt;&amp; device{device}),
        gtk_widget_set_device_events)
       
        gboolean gtk_icon_view_get_cell_rect(GtkIconView* icon_view,
        GtkTreePath* path, GtkCellRenderer* cell, GdkRectangle* rect);
       
        void gtk_widget_set_device_events(GtkWidget* widget, GdkDevice* device,
        GdkEventMask events);
       
    ...
    button.signal_clicked().connect(sigc::ptr_fun(&amp;on_button_clicked));
    ...
 
  $ git clone git://git.gnome.org/mm-common
  $ cp -a mm-common/skeletonmm libsomethingmm
 
# ./configure
# make
# make install
 
#include &lt;gtkmm/bin.h&gt;
#include &lt;gtkmm/activatable.h&gt;
_DEFS(gtkmm,gtk)
_PINCLUDE(gtkmm/private/bin_p.h)

namespace Gtk
{

class Button
  : public Bin,
    public Activatable
{
  _CLASS_GTKOBJECT(Button,GtkButton,GTK_BUTTON,Gtk::Bin,GtkBin)
  _IMPLEMENTS_INTERFACE(Activatable)
public:

  _CTOR_DEFAULT
  explicit Button(const Glib::ustring&amp; label, bool mnemonic = false);

  _WRAP_METHOD(void set_label(const Glib::ustring&amp; label), gtk_button_set_label)

  ...

  _WRAP_SIGNAL(void clicked(), "clicked")

  ...

  _WRAP_PROPERTY("label", Glib::ustring)
};

} // namespace Gtk
 
#include &lt;gtkmm/button.h&gt;

class OverriddenButton : public Gtk::Button
{
protected:
    virtual void on_clicked();
}

void OverriddenButton::on_clicked()
{
    std::cout &lt;&lt; "Hello World" &lt;&lt; std::endl;

    // call the base class's version of the method:
    Gtk::Button::on_clicked();
}
 
#include &lt;gtkmm/button.h&gt;

void on_button_clicked()
{
    std::cout &lt;&lt; "Hello World" &lt;&lt; std::endl;
}

main()
{
    Gtk::Button button("Hello World");
    button.signal_clicked().connect(sigc::ptr_fun(&amp;on_button_clicked));
}
 
#include &lt;gtkmm/button.h&gt;
#include &lt;gtkmm/window.h&gt;
class Foo : public Gtk::Window
{
private:
  Gtk::Button theButton;
  // will be destroyed when the Foo object is destroyed
};
 
#include &lt;libsomething.h&gt;

int main(int, char**)
{
  something_init();

  std::cout &lt;&lt; get_defs(SOME_TYPE_WIDGET)
            &lt;&lt; get_defs(SOME_TYPE_STUFF);
  return 0;
}
 
$ ./enum.pl /usr/include/gtk-3.0/gtk/*.h &gt; gtk_enums.defs
 
$ ./h2def.py /usr/include/gtk-3.0/gtk/*.h &gt; gtk_methods.defs
 
$ cd gtk/src
$ /usr/lib/glibmm-2.4/proc/gmmproc -I ../../tools/m4 --defs . button . ./../gtkmm
 
$ cd tools/extra_defs_gen
$ ./generate_extra_defs &gt; gtk_signals.defs
 
$ for f in $(find libsomethingmm -depth -name '*skeleton*'); do \
    d="${f%/*}"; b="${f##*/}"; mv "$f" "$d/${b//skeleton/libsomething}"; \
  done
 
&gt; gdb with_signal
(gdb) catch throw
Catchpoint 1 (throw)
(gdb) run
Catchpoint 1 (exception thrown), 0x00714ff0 in __cxa_throw ()
(gdb) backtrace
#0  0x00714ff0 in __cxa_throw () from /usr/lib/i386-linux-gnu/libstdc++.so.6
#1  0x08048bd4 in throwSomething () at with_signal.cc:6
(gdb) continue
Continuing.
(with_signal:2375): glibmm-ERROR **
unhandled exception (type unknown) in signal handler

Program received signal SIGTRAP, Trace/breakpoint trap.
 
&gt; gdb with_signal
(gdb) run
(with_signal:2703): glibmm-ERROR **:
unhandled exception (type unknown) in signal handler

Program received signal SIGTRAP, Trace/breakpoint trap.
(gdb) backtrace
#2  0x0063c6ab in glibmm_unexpected_exception () at exceptionhandler.cc:77
#3  Glib::exception_handlers_invoke () at exceptionhandler.cc:150
#4  0x0063d370 in glibmm_source_callback (data=0x804d620) at main.cc:212
#13 0x002e1b31 in Gtk::Application::run (this=0x804f300) at application.cc:178
#14 0x08048ccc in main (argc=1, argv=0xbfffecd4) at with_signal.cc:16
 
&gt; gdb without_signal
(gdb) run
terminate called after throwing an instance of 'char const*'

Program received signal SIGABRT, Aborted.
(gdb) backtrace
#7  0x08048864 in throwSomething () at without_signal.cc:6
#8  0x0804887d in main (argc=1, argv=0xbfffecd4) at without_signal.cc:12
 
(gdb) catch throw
(gdb) commands
(gdb)   backtrace
(gdb)   continue
(gdb)   end
(gdb) set pagination off
(gdb) run
 
// _MEMBER_GET_PTR(engine_lang, lang_engine, EngineLang*, PangoEngineLang*)
// It's just a comment. It's difficult to find a real-world example.
 
// in a class that inherits from Gtk::Window...
Glib::RefPtr&lt;PrintOperation&gt; op = PrintOperation::create();
// ...set up op...
op-&gt;run(Gtk::PRINT_OPERATION_ACTION_PREVIEW, *this);
 
// in class ExampleWindow's method...
Glib::RefPtr&lt;PrintOperation&gt; op = PrintOperation::create();
// ...set up op...
op-&gt;signal_done().connect(sigc::bind(sigc::mem_fun(*this, &amp;ExampleWindow::on_printoperation_done), op));
// run the op
 
// with_signal.cc
#include &lt;gtkmm.h&gt;

bool throwSomething()
{
  throw "Something";
  return true;
}

int main(int argc, char** argv)
{
  Glib::signal_timeout().connect(sigc::ptr_fun(throwSomething), 500);
  Glib::RefPtr&lt;Gtk::Application&gt; app =
    Gtk::Application::create(argc, argv, "org.gtkmm.with_signal");
  app-&gt;hold();
  return app-&gt;run();
}
 
// without_signal.cc
#include &lt;gtkmm.h&gt;

bool throwSomething()
{
  throw "Something";
  return true;
}

int main(int argc, char** argv)
{
  throwSomething();
  Glib::RefPtr&lt;Gtk::Application&gt; app =
    Gtk::Application::create(argc, argv, "org.gtkmm.without_signal");
  return app-&gt;run();
}
 
//Within a class that inherits from Gtk::Window and keeps m_refPageSetup and m_refSettings as members...
Glib::RefPtr&lt;Gtk::PageSetup&gt; new_page_setup = Gtk::run_page_setup_dialog(*this, m_refPageSetup, m_refSettings);
m_refPageSetup = new_page_setup;
 
Button::Button(const Glib::ustring&amp; label, bool mnemonic)
:
  _CONSTRUCT("label", label.c_str(), "use_underline", gboolean(mnemonic))
{}
 
DerivedDialog* pDialog = 0;
builder-&gt;get_widget_derived("DialogBasic", pDialog);
 
DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Dialog(cobject)
{
}
 
DerivedDialog::DerivedDialog(BaseObjectType* cobject, const Glib::RefPtr&lt;Gtk::Builder&gt;&amp; builder)
: Gtk::Dialog(cobject),
  m_builder(builder),
  m_pButton(0)
{
  //Get the Glade-instantiated Button, and connect a signal handler:
  m_builder-&gt;get_widget("quit_button", m_pButton);
  if(m_pButton)
  {
    m_pButton-&gt;signal_clicked().connect( sigc::mem_fun(*this, &amp;DerivedDialog::on_button_quit) );
  }
}
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; pixbuf = Gdk::Pixbuf::create_from_file(filename);
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; pixbuf2 = pixbuf;
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
Gdk::Pixbuf&amp; underlying = *refPixbuf; //Syntax error - will not compile.
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf2 = refPixbuf;
 
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
int width = refPixbuf-&gt;get_width();
 
Glib::RefPtr&lt;Gtk::Builder&gt; builder = Gtk::Builder::create_from_file("basic.glade");
 
Glib::RefPtr&lt;Gtk::Builder&gt; builder = Gtk::Builder::create_from_file("basic.glade", "treeview_products");
 
Glib::RefPtr&lt;Gtk::PrintOperation&gt; op = Gtk::PrintOperation::create();
// ...set up op...
op-&gt;set_export_filename("test.pdf");
Gtk::PrintOperationResult res = op-&gt;run(Gtk::PRINT_OPERATION_ACTION_EXPORT);
 
Glib::RefPtr&lt;Gtk::TreeModel&gt; refModel = m_TreeView.get_model();
if(refModel)
{
  int cols_count = refModel-&gt;get_n_columns();
  ...
}
 
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore =
Glib::RefPtr&lt;Gtk::TreeStore&gt;::cast_dynamic(refModel);
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore2 =
Glib::RefPtr&lt;Gtk::TreeStore&gt;::cast_static(refModel);
 
Glib::RefPtr&lt;Gtk::TreeStore&gt; refStore = Gtk::TreeStore::create(columns);
Glib::RefPtr&lt;Gtk::TreeModel&gt; refModel = refStore;
 
Glib::SignalProxy1&lt;bool, Gtk::DirectionType&gt; signal_focus()
 
Glib::SignalProxy3&lt;void, const TextBuffer::iterator&amp;, const Glib::ustrin&amp;, int&gt; signal_insert();
 
Gtk::Button* button = new Gtk::Button("example");
gtk_button_do_something_new(button-&gt;gobj());
 
Gtk::Button* pButton = new Gtk::Button("Test");

// do something useful with pButton

delete pButton;
 
Gtk::Dialog* pDialog = 0;
builder-&gt;get_widget("DialogBasic", pDialog);
 
Gtk::ToolButton* button = Gtk::manage(new Gtk::ToolButton(icon, "Big"));
button-&gt;set_tooltip_text("Big Brush);
group_brushes-&gt;insert(*button);
 
Gtk::ToolItemGroup* group_brushes =
  Gtk::manage(new Gtk::ToolItemGroup("Brushes"));
m_ToolPalette.add(*group_brushes);
 
Gtk::Widget* CustomPrintOperation::on_create_custom_widget()
{
  set_custom_tab_label("My custom tab");

  Gtk::Box* hbox = new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 8);
  hbox-&gt;set_border_width(6);

  Gtk::Label* label = Gtk::manage(new Gtk::Label("Enter some text: "));
  hbox-&gt;pack_start(*label, false, false);
  label-&gt;show();

  hbox-&gt;pack_start(m_Entry, false, false);
  m_Entry.show();

  return hbox;
}

void CustomPrintOperation::on_custom_widget_apply(Gtk::Widget* /* widget */)
{
  Glib::ustring user_input = m_Entry.get_text();
  //...
}
 
GtkButton* cbutton = get_a_button();
Gtk::Button* button = Glib::wrap(cbutton);
 
GtkWidget* example_widget_new(int something, const char* thing)
{
        ExampleWidget* widget;
        widget = g_object_new (EXAMPLE_TYPE_WIDGET, NULL);
        example_widget_construct(widget, "something", something, "thing", thing);
}

void example_widget_construct(ExampleWidget* widget, int something, const char* thing)
{
        //Do stuff that uses private API:
        widget-&gt;priv-&gt;thing = thing;
        do_something(something);
}
 
GtkWidget* example_widget_new(int something, const char* thing)
{
        return g_object_new (EXAMPLE_TYPE_WIDGET, "something", something, "thing", thing, NULL);
}
 
MyContainer::MyContainer()
{
  Gtk::Button* pButton = Gtk::manage(new Gtk::Button("Test"));
  add(*pButton); //add *pButton to MyContainer
}
 
_CLASS_BOXEDTYPE(RGBA, GdkRGBA, NONE, gdk_rgba_copy, gdk_rgba_free)
 
_CLASS_BOXEDTYPE_STATIC(Rectangle, GdkRectangle)
 
_CLASS_GENERIC(AttrIter, PangoAttrIterator)
 
_CLASS_GOBJECT(AccelGroup, GtkAccelGroup, GTK_ACCEL_GROUP, Glib::Object, GObject)
 
_CLASS_GTKOBJECT(Button, GtkButton, GTK_BUTTON, Gtk::Bin, GtkBin)
 
_CLASS_INTERFACE(CellEditable, GtkCellEditable, GTK_CELL_EDITABLE, GtkCellEditableIface)
 
_CLASS_INTERFACE(LoadableIcon, GLoadableIcon, G_LOADABLE_ICON, GLoadableIconIface, Icon, GIcon)
 
_CLASS_OPAQUE_COPYABLE(Checksum, GChecksum, NONE, g_checksum_copy, g_checksum_free)
 
_CLASS_OPAQUE_REFCOUNTED(Coverage, PangoCoverage, pango_coverage_new, pango_coverage_ref, pango_coverage_unref)
 
_CONVERSION(`GtkTreeView*',`TreeView*',`Glib::wrap($3)')
 
_CONVERSION(`PrintSettings&amp;',`GtkPrintSettings*',__FR2P)
_CONVERSION(`const PrintSettings&amp;',`GtkPrintSettings*',__FCR2P)
_CONVERSION(`const Glib::RefPtr&lt;Printer&gt;&amp;',`GtkPrinter*',__CONVERT_REFPTR_TO_P($3))
 
_IGNORE(gtk_button_box_set_spacing, gtk_button_box_get_spacing)
 
_IMPLEMENTS_INTERFACE(Activatable)
 
_INITIALIZATION(`Gtk::Widget&amp;',`GtkWidget*',`$3 = Glib::wrap($4)')
 
_MEMBER_GET_GOBJECT(layout, layout, Pango::Layout, PangoLayout*)
 
_WRAP_ENUM(IconLookupFlags, GtkIconLookupFlags, NO_GTYPE)
 
_WRAP_ENUM(WindowType, GtkWindowType)
 
_WRAP_GERROR(PixbufError, GdkPixbufError, GDK_PIXBUF_ERROR)
 
_WRAP_METHOD(void set_text(const Glib::ustring&amp; text), gtk_entry_set_text)
 
_WRAP_METHOD_DOCS_ONLY(gtk_container_remove)
 
_WRAP_PROPERTY("label", Glib::ustring)
 
_WRAP_SIGNAL(void clicked(),"clicked")
 
_WRAP_VFUNC(SizeRequestMode get_request_mode() const, get_request_mode)
 
bool MyCallback() { std::cout &lt;&lt; "Hello World!\n" &lt;&lt; std::endl; return true; }
 
bool idleFunc();
 
bool input_callback(Glib::IOCondition condition);
 
bool on_button_press(GdkEventButton* event);
Gtk::Button button("label");
button.signal_button_press_event().connect( sigc::ptr_fun(&amp;on_button_press) );
 
bool on_key_press_or_release_event(GdkEventKey* event)
{
  if (event-&gt;type == GDK_KEY_PRESS &amp;&amp;
    event-&gt;keyval == GDK_KEY_1 &amp;&amp;
    (event-&gt;state &amp; (GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK)) == GDK_MOD1_MASK)
  {
    handle_alt_1_press(); // GDK_MOD1_MASK is normally the Alt key
    return true;
  }
  return false;
}

Gtk::Entry m_entry; // in a class definition

// in the class constructor
m_entry.signal_key_press_event().connect( sigc::ptr_fun(&amp;on_key_press_or_release_event) );
m_entry.signal_key_release_event().connect( sigc::ptr_fun(&amp;on_key_press_or_release_event) );
m_entry.add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK);
 
button.signal_button_press_event().connect( sigc::ptr_fun(&amp;on_mywindow_button_press), false );
 
class Server
{
public:
  //signal accessor:
  typedef sigc::signal&lt;void, bool, int&gt; type_signal_something;
  type_signal_something signal_something();

protected:
  type_signal_something m_signal_something;
};

Server::type_signal_something Server::signal_something()
{
  return m_signal_something;
}
 
class TextMark : public Glib::Object
{
  _CLASS_GOBJECT(TextMark, GtkTextMark, GTK_TEXT_MARK, Glib::Object, GObject)

protected:
  _WRAP_CTOR(TextMark(const Glib::ustring&amp; name, bool left_gravity = true), gtk_text_mark_new)

public:
  _WRAP_CREATE(const Glib::ustring&amp; name, bool left_gravity = true)
 
example-widget.h:56: error: using typedef-name 'ExampleWidget' after 'struct'
../../libexample/libexamplemm/example-widget.h:34: error: 'ExampleWidget' has a previous declaration here
make[4]: *** [example-widget.lo] Error 1
 
example-widget.h:60: error: '_ExampleWidget ExampleWidget' redeclared as different kind of symbol
../../libexample/libexamplemm/example-widget.h:34: error: previous declaration of 'typedef struct _ExampleWidget ExampleWidget'
 
int width = 0;
if(pixbuf)
{
  width = pixbuf-&gt;get_width();
}
 
m_button1.signal_clicked().connect( sigc::bind&lt;Glib::ustring&gt;( sigc::mem_fun(*this, &amp;HelloWorld::on_button_clicked), "button 1") );
 
my_connection.disconnect();
 
server.signal_something().connect(
  sigc::mem_fun(client, &amp;Client::on_server_something) );
 
sigc::connection  Glib::SignalIdle::connect(const sigc::slot&lt;bool&gt;&amp; slot,
                                    int priority = Glib::PRIORITY_DEFAULT_IDLE);
 
sigc::connection Glib::SignalIO::connect(const sigc::slot&lt;bool,Glib::IOCondition&gt;&amp; slot,
                                 int fd, Glib::IOCondition condition,
                                 int priority = Glib::PRIORITY_DEFAULT);
 
sigc::connection Glib::SignalTimeout::connect(const sigc::slot&lt;bool&gt;&amp; slot,
                                      unsigned int interval, int priority = Glib::PRIORITY_DEFAULT);
 
sigc::signal&lt;void, bool, int&gt; signal_something;
 
sigc::signal&lt;void,int&gt;::iterator signal&lt;void,int&gt;::connect( const sigc::slot&lt;void,int&gt;&amp; );
 
std::list&lt; Glib::RefPtr&lt;Gdk::Pixbuf&gt; &gt; listPixbufs;
Glib::RefPtr&lt;Gdk::Pixbuf&gt; refPixbuf = Gdk::Pixbuf::create_from_file(filename);
listPixbufs.push_back(refPixbuf);
 
typedef struct _ExampleWidget ExampleWidget;

struct _ExampleWidget
{
  ...
};
 
virtual void on_button_clicked(Glib::ustring data);
 
void ExampleWindow::on_printoperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr&lt;PrintOperation&gt;&amp; op)
{
  if (result == Gtk::PRINT_OPERATION_RESULT_ERROR)
    //notify user
  else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY)
    //Update PrintSettings with the ones used in this PrintOperation

  if (! op-&gt;is_finished())
    op-&gt;signal_status_changed().connect(sigc::bind(sigc::mem_fun(*this, &amp;ExampleWindow::on_printoperation_status_changed), op));
}
 
void ExampleWindow::on_printoperation_status_changed(const Glib::RefPtr&lt;PrintFormOperation&gt;&amp; op)
{
  if (op-&gt;is_finished())
    //the print job is finished
  else
    //get the status with get_status() or get_status_string()

  //update UI
}
 
void init()
{
  Gtk::Main::init_gtkmm_internals(); //Sets up the g type system and the Glib::wrap() table.
  wrap_init(); //Tells the Glib::wrap() table about the libsomethingmm classes.
}
 
void on_button_clicked();

class some_class
{
    void on_button_clicked();
};

some_class some_object;

main()
{
    Gtk::Button button;
    button.signal_clicked().connect( sigc::ptr_fun(&amp;on_button_clicked) );
    button.signal_clicked().connect( sigc::mem_fun(some_object, &amp;some_class::on_button_clicked) );
}
 
void on_insert(const TextBuffer::iterator&amp; pos, const Glib::ustring&amp; text, int bytes)
 
{
  Gtk::Button aButton;
  aButton.show();
  ...
  app-&gt;run();
}
 »Verborgene« Spalten # keep this file sorted alphabetically, one language code per line
de
ja #include &lt;gtkmm.h&gt; #m4 _CONVERSION(`GSList*',`std::vector&lt;Widget*&gt;',`Glib::SListHandler&lt;Widget*&gt;::slist_to_vector($3, Glib::OWNERSHIP_SHALLOW)') $ ./plug &amp; $ ./socket ./docextract_to_xml.py -s ~/checkout/gnome/gtk+/gtk/ &gt; gtk_docs.xml
 // creates its own adjustments
Gtk::TextView textview;
// uses the newly-created adjustment for the scrollbar as well
Gtk::Scrollbar vscrollbar (textview.get_vadjustment(), Gtk::ORIENTATION_VERTICAL); // note to translators: don't translate the "[noun]" part - it is
// just here to distinguish the string from another "jumps" string
text = strip(gettext("jumps[noun]"), "[noun]"); //compiler error - no conversion from ustring to int.
int number = row[m_Columns.m_col_text]; 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010 <application>gtkmm</application> 3 fügt einige neue Klassen hinzu: <application>gtkmm</application> und Win32 <application>gtkmm</application> im Vergleich zu Qt <application>gtkmm</application> ist ein Wrapper <application>gtkmm</application> stellt vier grundlegende Knopftypen bereit: <classname>Gtk::ToolItem</classname>s können dann zur Gruppe hinzugefügt werden, zum Beispiel so: <filename>libsomethingmm</filename>: Enthält generierte und handgeschriebene .h- und .cc-Dateien. <filename>libsomethingmm</filename>: Der Ordner der obersten Ebene. <filename>private</filename>: Enthält generierte <filename>*_p.h</filename>-Dateien. <filename>src</filename>: Enthält .hg- und .ccg-Quelldateien. <function>_WRAP_METHOD()</function>, <function>_WRAP_SIGNAL()</function> und <function>_WRAP_PROPERTY()</function> <literal>drag_begin</literal>: Stellt DragContext bereit. <literal>drag_end</literal>: Stellt DragContext bereit. <literal>lower</literal>: unterer Grenzwert <literal>page_increment</literal>: Erhöhung/Verringerung des Wertes, wenn auf einem Knopf die Maustaste 2 gedrückt wird <literal>page_size</literal>: nicht genutzt <literal>step_increment</literal>: Erhöhung/Verringerung des Wertes, wenn auf einem Knopf die Maustaste 1 gedrückt wird <literal>upper</literal>: oberer Grenzwert <methodname>get_preferred_height_for_width_vfunc()</methodname>: Berechnet die minimale und natürliche Höhe des Containers, falls die Breite angegeben wurde. <methodname>get_preferred_heigth_vfunc()</methodname>: Berechnet die minimale und natürliche Höhe des Containers. <methodname>get_preferred_height_vfunc()</methodname>: Berechnet die minimale und natürliche Höhe des Widgets. <methodname>get_preferred_width_for_height_vfunc()</methodname>: Berechnet die minimale und natürliche Breite des Containers, falls die Höhe angegeben wurde. <methodname>get_preferred_width_vfunc()</methodname>: Berechnet die minimale und natürliche Breite des Containers. <methodname>get_preferred_width_vfunc()</methodname>: Berechnet die minimale und natürliche Breite des Widgets. <methodname>get_request_mode_vfunc()</methodname>: (optional) Gibt den vom Widget bevorzugten <literal>Gtk::SizeRequestMode</literal> zurück. <methodname>get_request_mode_vfunc()</methodname>: Gibt den vom Container bevorzugten <literal>Gtk::SizeRequestMode</literal> zurück. <methodname>on_add()</methodname>: Ein Kind-Widget zum Container hinzufügen. <methodname>on_map()</methodname>: (optional) <methodname>on_remove()</methodname>: Ein Kind-Widget aus dem Container entfernen. <methodname>on_unmap()</methodname>: (optional) <placeholder-1/> (oder <placeholder-2/> für Dateinamen) <placeholder-1/>-Beispielpaket <varname>description</varname>: Eine Kurzbeschreibung der Ressource als UTF-8-kodierte Zeichenkette <varname>mime_type</varname>: Der MIME-Type der Ressource Ein <classname>ProgressBar</classname> ist standardmäßig horizontal und von links nach rechts angeordnet, aber Sie können die vertikale Ausrichtung mit der Methode <methodname>set_orientation()</methodname> erreichen. Ein Smartpointer agiert wie ein normaler Zeiger. Hier sind einige Beispiele. AM_CPPFLAGS AM_CPPFLAGS = ... -DPROGRAMNAME_LOCALEDIR=\"${PROGRAMNAME_LOCALEDIR}\" AM_CXXFLAGS ATK AboutDialog Zugriff auf Widgets Aktionen Aktivitätsmodus Hinzufügen von Objekten zur Liste der kürzlich geöffneten Dateien Hinzufügen von Zeilen Hinzufügen von Unterzeilen Hinzufügen von Widgets Anpassungen Nach dem Starten von <filename>socket</filename> sollte Folgendes im Terminal ausgegeben werden: Danach sollten Sie etwa Folgendes sehen: Ahlstedt Alignment Ein verbessertes »Hello World« Anastasov Lebensdauer einer Anwendung Anwenden von Tags AspectFrame Assistent Einschätzen der Anzeigegröße von Zeichenketten Asynchrone Vorgänge Grundlegende Typ-Äquivalente Grundlegende Typen Grundlagen Bevor Sie versuchen, <application>gtkmm</application> 3.0 zu installieren, müssen Sie zuerst diese anderen Pakete installieren. Bernhard Binden weiterer Argumente Bjarne Stroustrup, »The C++ Programming Language«, 4. Ausgabe – Abschnitt 34.3 Boxen Erstellen von <application>gtkmm</application> auf Win32 Knopf ButtonBox ButtonBoxes Knöpfe C-Programmierer nutzen die Funktion <function>sprintf()</function> zum Erstellen und Verketten von Strings. C++ favorisiert Streams, aber leider erschwert dieser Ansatz die Übersetzungen, da jedes Textfragment separat übersetzt wird und Übersetzer keine Möglichkeiten haben, die Fragmente neu anzuordnen, um der Grammatik ihrer Sprache gerecht zu werden. C-Typ C++-Typ Cairo und Pango Rufen Sie <methodname>show()</methodname> auf, um das Widget anzuzeigen. Änderungen in <application>gtkmm</application> 3 Ändern der Auswahl Kapitel zu »Zeichnen mit Cairo« Kapitel zum »Drucken«. Kapitel zu Tastaturereignissen. CheckButton Ankreuzfelder Chris Klassen-Makros Zwischenablage - Ideal Zwischenablage - Einfach ColorChooserDialog Kombinierte Auswahlfelder ComboBox ComboBox mit Eintrag ComboBox mit Eingabe ComboBoxText ComboBoxText mit Eintrag Vergleich mit anderen Signalsystemen Zusammenstellung der Zeichenketten Verbinden von Plugs und Sockets Verbinden von Signal-Handlern Konstruktor-Makros Konstruktoren Container-Widgets Kopieren Kopieren Kopieren des Projektgerüsts Erstellen der .hg- und .ccg-Dateien Erzeugen Ihrer eigenen Signale Cumming Gegenwärtig unterstützt <application>gettext</application> keine Nicht-ASCII-Zeichen im Quellcode (also Zeichen mit einem Code über 127). Zum Beispiel können Sie kein Copyright-Zeichen verwenden (©). Benutzerdefinierter Container Benutzerdefinierte Container Benutzerdefiniertes Widget Benutzerdefinierte Widgets DISTCLEANFILES = ... intltool-extract \
                 intltool-merge \
                 intltool-update \
                 po/.intltool-merge-cache Daniel David Standardformatierung Abhängigkeiten Dereferenzierung Designer ohne Programmierkenntnisse können grafische Benutzeroberflächen entwerfen und bearbeiten. Dialoge Verschiedene Verbindungstypen in Cairo Verbindungen von Signal-Handlern trennen Dokumentation Struktur zur Dokumentationserstellung Ziehen und Ablegen DragContext Zeichnen von Kreisbögen und Kreisen Zeichenbereich - Bögen Zeichenbereich - Bild Zeichenbereich - Linien Zeichenbereich - Text Zeichenbereich - Dünne Linien Zeichnen von gebogenen Linien Zeichnen von Grafiken Zeichnen gerader Linien Zeichnen von Text Zeichnen von Text mit Pango Zeichnen dünner Linien Zeichnen mit relativen Koordinaten Dynamische Zuweisung mit manage() und add() Dynamische Zuweisung mit new und delete Bearbeitbare Zellen Elstner Wird ausgegeben, wenn der Knopf angeklickt und losgelassen wird. Wird ausgegeben, wenn der Knopf gedrückt wird. Wird ausgegeben, wenn der Knopf losgelassen wird. Wird ausgegeben, wenn der Mauszeiger das Fenster mit dem Knopf verlässt. Wird ausgegeben, wenn sich der Mauszeiger über dem Fenster mit dem Knopf befindet. Eintrag Vervollständigung des Eintrags Beispiel für die Vervollständigung eines Eintrags Eintrag mit Symbol Eintrag mit Fortschrittsbalken Aufzählungen. EventBox Beispiel Beispielanwendung: Eine Uhr mit Cairo Beispiele Ausnahmen in Signal-Handlern PDF-Export Erweitern des Druckdialogs Ferreira FileChooser FileChooserDialog Filtern kürzlich geöffneter Dateien Zum Schluss überprüfen Sie den Status. Zum Beispiel <placeholder-1/> FontChooserDialog Für jede darzustellende Seite werden folgende Signale ausgegeben: <placeholder-1/> Beispiel für <classname>Pango::Analysis</classname> in <filename>item.hg</filename>: <placeholder-1/> Beispiel aus <filename>icontheme.hg</filename>: <placeholder-1/> Beispielsweise in <filename>rectangle.hg</filename>: <placeholder-1/> Beispiel in Pangomm für <filename>layoutline.hg</filename>: <placeholder-1/> Zum Beispiel, Zum Beispiel, <placeholder-1/> Beispiel aus <classname>Gdk::RGBA</classname>: <placeholder-1/> Beispiel aus <classname>Glib::Checksum</classname>: <placeholder-1/> Beispiel aus <filename>accelgroup.hg</filename>: <placeholder-1/> Beispiel aus <filename>entry.hg</filename>: <placeholder-1/> Beispiel aus <filename>enums.hg</filename>: <placeholder-1/> Folgendes könnte beispielsweise problematisch sein: Zum Beispiel: Detailliertere Informationen über Signale finden Sie im <link linkend="chapter-signals">Anhang</link>. Für Einfachauswahlen können Sie einfach <methodname>get_selected()</methodname> folgendermaßen aufrufen: Rahmen Vollständiges Beispiel GTK+ 3.0 Erstellen der .defs-Dateien. Erzeugen der enums.defs Erzeugen der methods.defs Hilfe zu Übersetzungen erhalten Ermitteln von Werten Glade und Gtk::Builder Glib::RefPtr&lt;Gtk::Adjustment&gt; Gtk::Adjustment::create(
  double value,
  double lower,
  double upper,
  double step_increment = 1,
  double page_increment = 10,
  double page_size = 0); Glib::RefPtr&lt;Gtk::Application&gt; app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base"); Glib::RefPtr&lt;Gtk::Clipboard&gt; refClipboard = Gtk::Clipboard::get();

//Targets:
std::vector&lt;Gtk::TargetEntry&gt; targets;
targets.push_back( Gtk::TargetEntry("example_custom_target") );
targets.push_back( Gtk::TargetEntry("UTF8_STRING") );

refClipboard-&gt;set( targets,
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_get),
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_clear) ); Glib::RefPtr&lt;Gtk::ListStore&gt; refListStore =
    Gtk::ListStore::create(m_Columns); Glib::RefPtr&lt;Gtk::RecentInfo&gt; info;
try
{
  info = recent_manager-&gt;lookup_item(uri);
}
catch(const Gtk::RecentManagerError&amp; ex)
{
  std::cerr &lt;&lt; "RecentManagerError: " &lt;&lt; ex.what() &lt;&lt; std::endl;
}
if (info)
{
  // item was found
} Glib::RefPtr&lt;Gtk::RecentManager&gt; recent_manager = Gtk::RecentManager::get_default();
recent_manager-&gt;add_item(uri); Glib::RefPtr&lt;Gtk::TextBuffer::Mark&gt; refMark =
    refBuffer-&gt;create_mark(iter); Glib::RefPtr&lt;Gtk::TextBuffer::Tag&gt; refTagMatch =
    Gtk::TextBuffer::Tag::create();
refTagMatch-&gt;property_background() = "orange"; Glib::RefPtr&lt;Gtk::TextBuffer::TagTable&gt; refTagTable =
    Gtk::TextBuffer::TagTable::create();
refTagTable-&gt;add(refTagMatch);
//Hopefully a future version of <application>gtkmm</application> will have a set_tag_table() method,
//for use after creation of the buffer.
Glib::RefPtr&lt;Gtk::TextBuffer&gt; refBuffer =
    Gtk::TextBuffer::create(refTagTable); Glib::RefPtr&lt;Gtk::TextChildAnchor&gt; refAnchor =
    refBuffer-&gt;create_child_anchor(iter); Glib::RefPtr&lt;Gtk::TreeModelSort&gt; sorted_model =
    Gtk::TreeModelSort::create(model);
sorted_model-&gt;set_sort_column(columns.m_col_name, Gtk::SORT_ASCENDING);
treeview.set_model(sorted_model); Glib::RefPtr&lt;Gtk::TreeSelection&gt; refTreeSelection =
    m_TreeView.get_selection(); Glib::RefPtr&lt;Gtk::UIManager&gt; m_refUIManager =
    Gtk::UIManager::create();
m_refUIManager-&gt;insert_action_group(m_refActionGroup);
add_accel_group(m_refUIManager-&gt;get_accel_group()); Glib::ustring Glib::ustring und std::iostreams Glib::ustring strText = row[m_Columns.m_col_text];
int number = row[m_Columns.m_col_number]; Glib::ustring ui_info =
    "&lt;ui&gt;"
    "  &lt;menubar name='MenuBar'&gt;"
    "    &lt;menu action='MenuFile'&gt;"
    "      &lt;menuitem action='New'/&gt;"
    "      &lt;menuitem action='Open'/&gt;"
    "      &lt;separator/&gt;"
    "      &lt;menuitem action='Quit'/&gt;"
    "    &lt;/menu&gt;"
    "    &lt;menu action='MenuEdit'&gt;"
    "      &lt;menuitem action='Cut'/&gt;"
    "      &lt;menuitem action='Copy'/&gt;"
    "      &lt;menuitem action='Paste'/&gt;"
    "    &lt;/menu&gt;"
    "  &lt;/menubar&gt;"
    "  &lt;toolbar  name='ToolBar'&gt;"
    "    &lt;toolitem action='Open'/&gt;"
    "    &lt;toolitem action='Quit'/&gt;"
    "  &lt;/toolbar&gt;"
    "&lt;/ui&gt;";

m_refUIManager-&gt;add_ui_from_string(ui_info); Glib::ustring ui_info =
    "&lt;ui&gt;"
    "  &lt;popup name='PopupMenu'&gt;"
    "    &lt;menuitem action='ContextEdit'/&gt;"
    "    &lt;menuitem action='ContextProcess'/&gt;"
    "    &lt;menuitem action='ContextRemove'/&gt;"
    "  &lt;/popup&gt;"
    "&lt;/ui&gt;";

m_refUIManager-&gt;add_ui_from_string(ui_info); Gruppen Gtk::Alignment
Gtk::Arrow
Gtk::AspectFrame
Gtk::Bin
Gtk::Box
Gtk::Button
Gtk::CheckButton
Gtk::Fixed
Gtk::Frame
Gtk::Grid
Gtk::Image
Gtk::Label
Gtk::MenuItem
Gtk::Notebook
Gtk::Paned
Gtk::RadioButton
Gtk::Range
Gtk::ScrolledWindow
Gtk::Separator
Gtk::Table (deprecated from <application>gtkmm</application> version 3.4)
Gtk::Toolbar Gtk::Application und Befehlszeilenoptionen Gtk::Box(Gtk::Orientation orientation = Gtk::ORIENTATION_HORIZONTAL, int spacing = 0);
void set_spacing(int spacing);
void set_homogeneous(bool homogeneous = true); Gtk::Button* pButton = new Gtk::Button("_Something", true); Gtk::CellRendererToggle* pRenderer =
    Gtk::manage( new Gtk::CellRendererToggle() );
pRenderer-&gt;signal_toggled().connect(
    sigc::bind( sigc::mem_fun(*this,
        &amp;Example_TreeView_TreeStore::on_cell_toggled), m_columns.dave)
); Gtk::DrawingArea myArea;
Cairo::RefPtr&lt;Cairo::Context&gt; myContext = myArea.get_window()-&gt;create_cairo_context();
myContext-&gt;set_source_rgb(1.0, 0.0, 0.0);
myContext-&gt;set_line_width(2.0); Gtk::Entry* entry = m_Combo.get_entry();
if (entry)
{
  // The Entry shall receive focus-out events.
  entry-&gt;add_events(Gdk::FOCUS_CHANGE_MASK);

  // Alternatively you can connect to m_Combo.signal_changed().
  entry-&gt;signal_changed().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_changed) );

  entry-&gt;signal_activate().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_activate) );

  entry-&gt;signal_focus_out_event().connect(sigc::mem_fun(*this,
    &amp;ExampleWindow::on_entry_focus_out_event) );
} Gtk::EventBox(); Gtk::TreeModel::Children children = row.children(); Gtk::TreeModel::Row row = *iter; Gtk::TreeModel::Row row = m_refModel-&gt;children()[5]; //The fifth row.
if(row)
  refTreeSelection-&gt;select(row); Gtk::TreeModel::iterator iter = m_Combo.get_active();
if(iter)
{
  Gtk::TreeModel::Row row = *iter;

  //Get the data for the selected row, using our knowledge
  //of the tree model:
  int id = row[m_Columns.m_col_id];
  set_something_id_chosen(id); //Your own function.
}
else
  set_nothing_chosen(); //Your own function. Gtk::TreeModel::iterator iter = m_refListStore-&gt;append(); Gtk::TreeModel::iterator iter = m_refModel-&gt;children().begin()
if(iter)
  refTreeSelection-&gt;select(iter); Gtk::TreeModel::iterator iter_child =
    m_refTreeStore-&gt;append(row.children()); Gtk::TreeView::Column* pColumn =
  Gtk::manage(new Gtk::TreeView::Column("Icon Name"));

// m_columns.icon and m_columns.iconname are columns in the model.
// pColumn is the column in the TreeView:
pColumn-&gt;pack_start(m_columns.icon, /* expand= */ false);
pColumn-&gt;pack_start(m_columns.iconname);

m_TreeView.append_column(*pColumn); Gtk::TreeView::Column* pColumn = treeview.get_column(0);
if(pColumn)
  pColumn-&gt;set_sort_column(m_columns.m_col_id); Gtk::Widget* pMenubar = m_refUIManager-&gt;get_widget("/MenuBar");
pBox-&gt;add(*pMenuBar, Gtk::PACK_SHRINK); Gtk::Window window;
window.set_default_size(200, 200); Handgeschriebene Quelldateien Handgeschriebene Konstruktoren Umgang mit <literal>button_press_event</literal> Hello World Hello World 2 »Hello World« in <application>gtkmm</application> HelloWorld::HelloWorld()
:
  m_button ("Hello World")
{
  set_border_width(10);
  m_button.signal_clicked().connect(sigc::mem_fun(*this,
    &amp;HelloWorld::on_button_clicked));
  add(m_button);.
  m_button.show();
} Es folgt eine Liste einiger dieser Widgets: Hier ist ein Beispiel für eine Callback-Methode: Hier ist ein einfaches Beispiel: <placeholder-1/> Hier ist ein etwas umfangreicheres Beispiel von Slots in Aktion: Hier ist ein Beispiel für einen <classname>SpinButton</classname> in Aktion: Hier ein Beispiel für diese Technik: Funktionsweise von Gettext Nutzung des GNOME-Git als Übersetzer INTLTOOL_FILES = intltool-extract.in \
                 intltool-merge.in \
                 intltool-update.in IT_PROG_INTLTOOL([0.35.0])

GETTEXT_PACKAGE=programname
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"],
                   [The domain to use with gettext])
AM_GLIB_GNU_GETTEXT

PROGRAMNAME_LOCALEDIR=[${datadir}/locale]
AC_SUBST(PROGRAMNAME_LOCALEDIR) Ideal Untätigkeitsfunktionen In <filename>configure.ac</filename>, <placeholder-1/> In der Datei Makefile.am der obersten Ebene: <placeholder-1/> Beinhaltet die andren Dateien. Unabhängig sortierte Ansichten des gleichen Modells InfoBar Initialisierung Installation Installation von <application>gtkmm</application> mit <application>jhbuild</application> Installieren aus den Quellen Installation und Verwendung der Git-Version von <application>gtkmm</application> Internationalisierung und Lokalisierung Intltool-README Einführung Es akzeptiert optional ein zusätzliches Argument: <placeholder-1/> Jonathon Jongsma Tastaturereignisse Tastaturereignisse - Einfach King Kjell L10N-Richtlinien für Entwickler Bezeichnung Fehlende Eigenschaften Laursen Es wird weniger C++-Code benötigt. Linienstile ListStore ListStore, für Zeilen Laden der .glade-Datei Hauptmenü Hauptmenü-Beispiel Makefile.am-Dateien Verwaltete Widgets Markieren von Zeichenketten für die Übersetzung Marko Markierungen Speicherverwaltung Menüs und Werkzeugleisten MessageDialog Methoden-Makros Methoden Microsoft Windows Verschiedene Widgets Mischen der APIs von C und C++ ModelColumns()
{ add(m_col_id); add(m_col_name); }

  Gtk::TreeModelColumn&lt;int&gt; m_col_id;
  Gtk::TreeModelColumn&lt;Glib::ustring&gt; m_col_name;
};

ModelColumns m_columns; Anpassen der Erstellungsdateien Anpassen der Liste der kürzlich geöffneten Dateien Überwachung von Ein- und Ausgaben Die meisten unserer Beispiele verwenden diese Technik. Verschieben Widgets mit mehreren Objekten Murray Murray Cumming Nicolai M. Josuttis, »The C++ Standard Library« - Abschnitt 4.2 Nicht-modaler AboutDialog Normale C++-Speicherverwaltung Beachten Sie, dass Sie Aktionen für Untermenüs wie auch für Menüeinträge angeben müssen. Reitermappe Nun werfen wir wieder einen Blick auf die Verbindung: Objekte und Funktionen. Das bedeutet natürlich, dass Sie <classname>RefPtr</classname>s in Standardcontainern speichern können, wie <classname>std::vector</classname> oder <classname>std::list</classname>. Ole Weitere Makros Überschreiben von Standard-Signalhandlern Übersicht PKG_CHECK_MODULES([MYAPP], [gtkmm-3.0 &gt;= 3.8.0]) Packen Sie das Widget mit dem entsprechenden Aufruf in einen Container, zum Beispiel <methodname>Gtk::Container::add()</methodname> oder <methodname>pack_start()</methodname>. Packen Seite einrichten Pango Teile des Kapitels zur »Internationalisierung« Teile der Aktualisierung von gtkmm 2 auf gtkmm 3. Einfügen Pedro Das vorliegende Dokument kann gemäß den Bedingungen der GNU Free Documentation License (GFDL), Version 1.2 oder jeder späteren, von der Free Software Foundation veröffentlichten Version ohne unveränderbare Abschnitte sowie ohne Texte auf dem vorderen und hinteren Buchdeckel kopiert, verteilt und/oder modifiziert werden. Eine Kopie der GFDL erhalten Sie auf der Webseite der Free Software Foundation, oder schreiben Sie an: Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Fallstricke Auf <ulink url="https://wiki.gnome.org/Projects/gtkmm/MSWindows/BuildingGtkmm"> https://wiki.gnome.org/Projects/gtkmm/MSWindows/BuildingGtkmm</ulink> finden Sie Anweisungen, wie Sie gtkmm unter Windows erstellen können. Plugs Plugs und Sockets Beispiel für Plugs und Sockets Popup-Kontextmenü Kontextmenü Kontextmenü-Beispiel Kontextmenüs Vorkompilierte Pakete Einrichten Ihres Projekts Verhindern der Zeilenauswahl Vorschau PrintOperation Drucken Drucken - Einfach Probleme in der C-API. Programmierung mit <application>gtkmm</application> 3 Fortschrittsbalken werden verwendet, um den Status eines laufenden Vorgangs anzuzeigen. Zum Beispiel kann ein <classname>ProgressBar</classname> zeigen, wie viel von einer Aufgabe bereits abgearbeitet wurde. ProgressBar Setzt den erzeugten Code in #ifdef-Blöcke. Radioknöpfe RadioButton RecentChooser RecentManager Kürzlich geöffnete Dokumente Empfohlene Techniken Referenz Darstellen von Text Neu sortierbare Zeilen Ressourcen Reaktion auf Änderungen Die C-Dokumentation wiederverwenden Rieder Gleiche Zeichenketten, unterschiedliche Semantik Rollbalken-Widgets ScrolledWindow Rollen Abschnitt zu Gtk::Grid. Festlegen eines Präfix Einrichten von jhbuild Festlegen von Werten Gemeinsame Ressourcen Signalhandler-Sequenz Signale Signale und Eigenschaften. Einfach Einfaches Beispiel Einfaches Beispiel für RecentChooserDialog Einfaches Textbeispiel Einfache oder mehrfache Auswahl Daher sollten Sie diese Situation entweder gänzlich vermeiden oder <ulink url="http://developer.gnome.org/glibmm/unstable/classGlib_1_1ustring.html"><function>Glib::ustring::compose()</function></ulink> verwenden, das folgend Syntax unterstützt: <placeholder-1/> Sockets Sortierung Sortierung durch Anklicken von Spalten Quelltext Quellen und Ziele Angeben von CellRenderer-Details SpinButton Tabelle TagTable Tags Tags und Formatierung Ziele Testen und Hinzufügen von Übersetzungen TextView Das »changed«-Signal Die .hg- und .ccg-Dateien Das <classname>Gtk::Button</classname>-Widget gibt folgende Signale aus, wobei Sie jedoch meist nur das <literal>clicked</literal>-Signal verarbeiten müssen: Das Argument <parameter>options</parameter> akzeptiert eine dieser drei Optionen: <placeholder-1/> Der Puffer Das Cairo-Zeichenmodell Die Zwischenablage Das Zeichenbereich-Widget Das Modell Der RefPtr-Smartpointer Die Auswahl Das TreeView-Widget Die Ansicht Die Argumente zu <methodname>Action::create()</methodname> geben den Namen der Aktion an, und wie es in Menüs und Werkzeugleisten erscheint. Die Erstellungsstruktur Das gewählte Objekt Die an den C-Präprozessor übergebenen Befehlszeilenoptionen. Die an den C++-Compiler übergebenen Befehlszeilenoptionen. Die Einschränkungen Der Eintrag Das folgende Beispiel demonstriert die Verwendung von Radioknöpfen (<classname>RadioButton</classname>): Das folgende Beispielprogramm benötigt eine Befehlszeilenoption. Der Quellcode zeigt zwei Wege, wie in Kombination mit <classname>Gtk::Application</classname> mit Befehlszeilenoptionen umgegangen werden kann. Die Makros werden in den folgenden Abschnitten detaillierter beschrieben. Die Makros in diesem Beispiel bewirken Folgendes: <placeholder-1/> Das Modell Der Name der Bibliothek, wie libsomethingmm. Der native GTK+-Druckdialog verfügt über einen Vorschauknopf, aber Sie können die Vorschau auch direkt aus der Anwendung starten: <placeholder-1/> Die nächsten zwei Codezeilen erzeugen ein Fenster und legen dessen (initiale) Standardgröße fest: Die Regeln Die ausgewählten Zeilen Die Textspalte The window ID is: 69206019 Dann starten Sie das <filename>socket</filename>-Programm: Es gibt einige typische Fehler, denen Sie möglicherweise auch selbst begegnen werden. Dieser Abschnitt könnte Ihnen dabei helfen, dies zu vermeiden. Es gibt fünf verschiedene grundlegende Stile, wie in diesem Bild gezeigt: Es gibt einige zusätzliche optionale Argumente: <placeholder-1/> Zwei grundlegende Strategien können angewendet werden: <placeholder-1/> In examples/others/dnd finden Sie ein komplexeres Beispiel. Es gibt nur ein optionales zusätzliches Argument: <placeholder-1/> Diese Abhängigkeiten haben wiederum ihre eigenen Abhängigkeiten, wie die folgenden Anwendungen und Bibliotheken: Dieses Buch Dieses Buch setzt gutes Verständnis von C++ und in der Erstellung von C++-Programmen voraus. Dieses Beispiel erzeugt einen Knopf mit einer Grafik und einer Beschriftung. Dieses Beispiel implementiert ein Widget, das ein Penrose-Dreieck zeichnet. Dies hat folgende Vorteile: <placeholder-1/> Dies ist ein voll funktionsfähiges Beispiel, welches benutzerdefinierte Signale definiert und nutzt. Dies wird im drag_and_drop-Beispiel demonstriert. Dieses Makro wird verwendet, wenn Sie <literal>gettext</literal> in Ihrem Quellcode initialisieren. Im einfachsten Fall müssen Sie zum Hinzufügen einer Datei zur Liste der kürzlich geöffneten Dateien nur deren Adresse angeben. Zum Beispiel: Am Anfang unserer Einführung in <application>gtkmm</application> beginnen wir mit dem einfachsten möglichen Programm. Es erzeugt ein leeres, 200 mal 200 Pixel großes Fenster. Um die Auswahl zu ändern, geben Sie einen <classname>Gtk::TreeModel::iterator</classname> oder <classname>Gtk::TreeModel::Row</classname> folgendermaßen an: Um das zu erreichen, müssen Sie die Methode <methodname>pulse()</methodname> in regelmäßigen Abständen aufrufen. Außerdem können Sie mit der Methode <methodname>set_pulse_step()</methodname> die Schrittweite wählen. Umschaltknöpfe ToggleButton ToolItem-Referenz ToolItemGroup-Referenz ToolPalette Beispiel für ToolPalette ToolPalette-Referenz Minihilfe Minihilfen-Referenz Minihilfen TreeModel::iterator iter = refTreeSelection-&gt;get_selected();
if(iter) //If anything is selected
{
  TreeModel::Row row = *iter;
  //Do something with the row.
} TreeModelSort-Referenz TreeSortable-Referenz TreeStore TreeStore, für eine Hierarchie TreeView - Ziehen und Ablegen TreeView - Bearbeitbare Zellen TreeView - ListStore TreeView - Aufklappendes Kontextmenü TreeView - TreeStore UIManager Unix und Linux Unübliche Wörter Aktualisieren Sie Ihre <literal>DISTCLEANFILES</literal>: <placeholder-1/> Verwenden Sie <methodname>Gtk::Builder::get_widget_derived()</methodname> folgendermaßen: <placeholder-1/> Nützliche Methoden Verwendung von Glib::Dispatcher Verwendung eines <application>gtkmm</application>-Widgets Verwendung eines Modells Verwenden abgeleiteter Widgets Verwendubg von Nicht-ASCII-Zeichen in Zeichenketten Verwendung der Git-Version von <application>gtkmm</application> Vine Erklärung der einzelnen Zeilen des Beispiels Wenn der Knopf sein <literal>clicked</literal>-Signal ausgibt, wird <methodname>on_button_clicked()</methodname> aufgerufen. Warum sollte ich <application>gtkmm</application> anstelle von GTK+ verwenden? Widget-Referenz Widgets Widgets ohne X-Windows Widgets und ChildAnchors Arbeiten mit dem Quelltext von gtkmm Wrapping von C-Bibliotheken mit gmmproc Schreiben von Signal-Handlern Schreiben der vfuncs.defs X-Ereignissignale Sie können auch einen Signalhandler beim Aufruf von <methodname>ActionGroup::add()</methodname> angeben. Dieser Signalhandler wird aufgerufen, wenn eine Aktion über das Menü oder einen Werkzeugleistenknopf aktiviert wird. Sie können <classname>RefPtr</classname>s ganz einfach wie normale Zeiger kopieren. Aber im Gegensatz zu normalen Zeigern müssen Sie sich keine Gedanken um die darunterliegenden Instanzen machen. Sie wissen nie, wie viel Platz ein String auf dem Bildschirm einnimmt, wenn er erst einmal übersetzt ist. Es kann durchaus die doppelte Größe der englischen Originalversion sein. Glücklicherweise expandiert <application>gtkmm</application> die Widgets zur Laufzeit auf die erforderliche Größe. Sie sollten kryptische Abkürzungen, Slang oder Jargon vermeiden. So etwas ist recht schwierig zu übersetzen und oft selbst für Muttersprachler nur schwer verständlich. Beispielsweise sollten Sie »application« gegenüber »app« bevorzugen. _CLASS_BOXEDTYPE _CLASS_BOXEDTYPE_STATIC _CLASS_BOXEDTYPE_STATIC( C++-Klasse, C-Klasse ) _CLASS_GENERIC _CLASS_GENERIC( C++-Klasse, C-Klasse ) _CLASS_GOBJECT _CLASS_GTKOBJECT _CLASS_GTKOBJECT() _CLASS_INTERFACE _CLASS_OPAQUE_COPYABLE _CLASS_OPAQUE_REFCOUNTED _CTOR_DEFAULT _DEFS() _IGNORE / _IGNORE_SIGNAL _IGNORE(C-Funktionsname 1, C-Funktionsname2, usw.) _IGNORE_SIGNAL(C signal name 1, C signal name2, etc) _IMPLEMENTS_INTERFACE _IMPLEMENTS_INTERFACE() _IMPLEMENTS_INTERFACE(C++ interface name) _MEMBER_GET / _MEMBER_SET _MEMBER_GET(C++-Name, C-Name, C++-Typ, C-Typ) _MEMBER_GET(x, x, int, int) _MEMBER_GET_GOBJECT / _MEMBER_SET_GOBJECT _MEMBER_GET_GOBJECT(C++-Name, C-Name, C++-Typ, C-Typ) _MEMBER_GET_PTR / _MEMBER_SET_PTR _MEMBER_GET_PTR(C++-Name, C-Name, C++-Typ, C-Typ) _MEMBER_SET(C++-Name, C-Name, C++-Typ, C-Typ) _MEMBER_SET_GOBJECT(C++ name, C name, C++ type, C type) _MEMBER_SET_PTR(C++ name, C name, C++ type, C type) _PINCLUDE() _WRAP_CTOR _WRAP_ENUM _WRAP_ENUM_DOCS_ONLY _WRAP_GERROR _WRAP_METHOD _WRAP_METHOD( C++ method signature, C function name) _WRAP_METHOD_DOCS_ONLY _WRAP_METHOD_DOCS_ONLY(C-Funktionsname) _WRAP_PROPERTY _WRAP_PROPERTY(C-Eigenschaftsname, C++-Typ) _WRAP_SIGNAL _WRAP_SIGNAL( C++ signal handler signature, C signal name) _WRAP_VFUNC _WRAP_VFUNC( C++ method signature, C function name) adj-&gt;signal_value_changed().connect(sigc::bind&lt;MyPicture*&gt;(sigc::mem_fun(*this,
    &amp;cb_rotate_picture), picture)); adjustment-&gt;signal_changed(); atkmm binding_name bindtextdomain(GETTEXT_PACKAGE, PROGRAMNAME_LOCALEDIR);
bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
textdomain(GETTEXT_PACKAGE); bool bool DemoWindow::select_function(
    const Glib::RefPtr&lt;Gtk::TreeModel&gt;&amp; model,
    const Gtk::TreeModel::Path&amp; path, bool)
{
  const Gtk::TreeModel::iterator iter = model-&gt;get_iter(path);
  return iter-&gt;children().empty(); // only allow leaf nodes to be selected
} bool ExampleWindow::on_button_press_event(GdkEventButton* event)
{
  if( (event-&gt;type == GDK_BUTTON_PRESS) &amp;&amp;
      (event-&gt;button == 3) )
  {
    m_Menu_Popup-&gt;popup(event-&gt;button, event-&gt;time);
    return true; //It has been handled.
  }
  else
    return false;
} bool MyArea::on_draw(const Cairo::RefPtr&lt;Cairo::Context&gt;&amp; cr)
{
  Glib::RefPtr&lt;Gdk::Pixbuf&gt; image = Gdk::Pixbuf::create_from_file("myimage.png");
  // Draw the image at 110, 90, except for the outermost 10 pixels.
  Gdk::Cairo::set_source_pixbuf(cr, image, 100, 80);
  cr-&gt;rectangle(110, 90, image-&gt;get_width()-20, image-&gt;get_height()-20);
  cr-&gt;fill();
  return true;
} Beispiel für Knöpfe Cairo cairomm cell.property_editable() = true; class HelloWorld : public Gtk::Window
{

public:
  HelloWorld();
  virtual ~HelloWorld();

protected:
  //Signal handlers:
  virtual void on_button_clicked();

  //Member widgets:
  Gtk::Button m_button;
}; class RadioButtons : public Gtk::Window
{
public:
    RadioButtons();

protected:
    Gtk::RadioButton m_rb1, m_rb2, m_rb3;
};

RadioButtons::RadioButtons()
  : m_rb1("button1"),
    m_rb2("button2"),
    m_rb3("button3")
{
    Gtk::RadioButton::Group group = m_rb1.get_group();
    m_rb2.set_group(group);
    m_rb3.set_group(group);
} class RadioButtons : public Gtk::Window
{
public:
    RadioButtons();
};

RadioButtons::RadioButtons()
{
    Gtk::RadioButton::Group group;
    Gtk::RadioButton *m_rb1 = Gtk::manage(
      new Gtk::RadioButton(group,"button1"));
    Gtk::RadioButton *m_rb2 = manage(
      new Gtk::RadioButton(group,"button2"));
      Gtk::RadioButton *m_rb3 = manage(
        new Gtk::RadioButton(group,"button3"));
} clicked configure.ac constversion context-&gt;save();
context-&gt;translate(x, y);
context-&gt;scale(width / 2.0, height / 2.0);
context-&gt;arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
context-&gt;restore(); custom_c_callback custom_default_handler custom_vfunc custom_vfunc_callback deprecated display_message("Getting ready for i18n."); display_message(_("Getting ready for i18n.")); double enter enums errthrow event_box.add(child_widget); functions g++ simple.cc -o simple `pkg-config gtkmm-3.0 --cflags --libs` gboolean gchar* gdk-pixbuf gdouble Gettext-Handbuch gint glib glibmm gmmproc-Parameterverarbeitung gnomemm_hello gtk.defs gtk_enums.defs gtk_methods.defs gtk_signals.defs gtk_vfuncs.defs gtkmm gtkmm_hello guint gunichar ifdef int int cols_count = m_TreeView.append_column_editable("Alex", m_columns.alex);
Gtk::TreeViewColumn* pColumn = m_TreeView.get_column(cols_count-1);
if(pColumn)
{
  Gtk::CellRendererToggle* pRenderer =
    static_cast&lt;Gtk::CellRendererToggle*&gt;(pColumn-&gt;get_first_cell());
  pColumn-&gt;add_attribute(pRenderer-&gt;property_visible(), m_columns.visible);
  pColumn-&gt;add_attribute(pRenderer-&gt;property_activatable(), m_columns.world); int main(int argc, char** argv)
{
  Glib::RefPtr&lt;Gtk::Application&gt; app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  HelloWorld helloworld;
  return app-&gt;run(helloworld);
} intltool-update --pot leave lib_LTLIBRARIES libsigc++ 2.0 m4-Umwandlungen m4-Initialisierungen m_TextView.add_child_at_anchor(m_Button, refAnchor); m_TreeView.append_column("Messages", m_Columns.m_col_text); m_TreeView.set_model(m_refListStore); m_box.pack_start(m_Button1);
m_box.pack_start(m_Button2); m_button1.signal_clicked().connect( sigc::mem_fun(*this,
  &amp;HelloWorld::on_button_clicked) ); m_combo.set_entry_text_column(m_columns.m_col_name); m_combo.signal_changed().connect( sigc::mem_fun(*this,
      &amp;ExampleWindow::on_combo_changed) ); m_frame.add(m_box); m_rb2.set_group(m_rb1.get_group()); //doesn't work m_refActionGroup = Gtk::ActionGroup::create();

m_refActionGroup-&gt;add( Gtk::Action::create("MenuFile", "_File") );
m_refActionGroup-&gt;add( Gtk::Action::create("New", "_New"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_new) );
m_refActionGroup-&gt;add( Gtk::Action::create("ExportData", "Export Data"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_open) );
m_refActionGroup-&gt;add( Gtk::Action::create("Quit", "_Quit"),
  sigc::mem_fun(*this, &amp;ExampleWindow::on_action_file_quit) ); m_refTreeSelection-&gt;set_select_function( sigc::mem_fun(*this,
    &amp;DemoWindow::select_function) ); modules = [ 'gtkmm' ] moduleset = 'gnome-suites-core-deps-3.12' no_default_handler no_slot_copy oder pangomm pkg-config pressed properties refBuffer-&gt;apply_tag(refTagMatch, iterRangeStart, iterRangeStop); refBuffer-&gt;insert_with_tag(iter, "Some text", refTagMatch); refClipboard-&gt;request_contents("example_custom_target",
    sigc::mem_fun(*this, &amp;ExampleWindow::on_clipboard_received) ); refClipboard-&gt;request_targets( sigc::mem_fun(*this,
    &amp;ExampleWindow::on_clipboard_received_targets) ); refTreeSelection-&gt;selected_foreach_iter(
    sigc::mem_fun(*this, &amp;TheClass::selected_row_callback) );

void TheClass::selected_row_callback(
    const Gtk::TreeModel::iterator&amp; iter)
{
  TreeModel::Row row = *iter;
  //Do something with the row.
} refTreeSelection-&gt;set_mode(Gtk::SELECTION_MULTIPLE); refTreeSelection-&gt;signal_changed().connect(
    sigc::mem_fun(*this, &amp;Example_IconTheme::on_selection_changed)
); refreturn refreturn_ctype released return app-&gt;run(window); row[m_Columns.m_col_text] = "sometext"; signals slot_callback slot_name src/main.cc
src/other.cc std::cout &lt;&lt; Glib::ustring::compose(
             _("Current amount: %1 Future: %2"), amount, future) &lt;&lt; std::endl;

label.set_text(Glib::ustring::compose(_("Really delete %1 now?"), filename)); std::cout &lt;&lt; _("Current amount: ") &lt;&lt; amount
          &lt;&lt; _(" Future: ") &lt;&lt; future &lt;&lt; std::endl;

label.set_text(_("Really delete ") + filename + _(" now?")); std::ostringstream output;
output.imbue(std::locale("")); // use the user's locale for this stream
output &lt;&lt; percentage &lt;&lt; " % done";
label-&gt;set_text(Glib::locale_to_utf8(output.str())); std::string std::vector&lt; Glib::RefPtr&lt;Gtk::RecentInfo&gt; &gt; info_list = recent_manager-&gt;get_items(); Mario Blättermann <mario.blaettermann@gmail.com>, 2010-2012 typedef Gtk::TreeModel::Children type_children; //minimise code length.
type_children children = refModel-&gt;children();
for(type_children::iterator iter = children.begin();
    iter != children.end(); ++iter)
{
  Gtk::TreeModel::Row row = *iter;
  //Do something with the row - see above for set/get.
} vfuncs void ExampleWindow::on_button_delete()
{
  Glib::RefPtr&lt;Gtk::TreeSelection&gt; refTreeSelection =
      m_treeview.get_selection();
  if(refTreeSelection)
  {
    Gtk::TreeModel::iterator sorted_iter =
        m_refTreeSelection-&gt;get_selected();
    if(sorted_iter)
    {
      Gtk::TreeModel::iterator iter =
          m_refModelSort-&gt;convert_iter_to_child_iter(sorted_iter);
      m_refModel-&gt;erase(iter);
    }
  }
} void ExampleWindow::on_clipboard_get(
    Gtk::SelectionData&amp; selection_data, guint /* info */)
{
  const std::string target = selection_data.get_target();

  if(target == "example_custom_target")
    selection_data.set("example_custom_target", m_ClipboardStore);
} void ExampleWindow::on_clipboard_received(
    const Gtk::SelectionData&amp; selection_data)
{
  Glib::ustring clipboard_data = selection_data.get_data_as_string();
  //Do something with the pasted data.
} void ExampleWindow::on_clipboard_received_targets(
  const std::vector&lt;Glib::ustring&gt;&amp; targets)
{
  const bool bPasteIsPossible =
    std::find(targets.begin(), targets.end(),
      example_target_custom) != targets.end();

  // Enable/Disable the Paste button appropriately:
  m_Button_Paste.set_sensitive(bPasteIsPossible);
} void cb_rotate_picture (MyPicture* picture)
{
  picture-&gt;set_rotation(adj-&gt;get_value());
... void doSomething(const Cairo::RefPtr&lt;Cairo::Context&gt;&amp; context, int x)
{
    context-&gt;save();
    // change graphics state
    // perform drawing operations
    context-&gt;restore();
} void drag_dest_set(const std::vector&lt;Gtk::TargetEntry&gt;&amp; targets,
    Gtk::DestDefaults flags, Gdk::DragAction actions); void drag_source_set(const std::vector&lt;Gtk::TargetEntry&gt;&amp; targets,
      Gdk::ModifierType start_button_mask, Gdk::DragAction actions); void pack_start(Gtk::Widget&amp; child,
                Gtk::PackOptions options = Gtk::PACK_EXPAND_WIDGET,
                guint padding = 0); wrap_init_flags xgettext -a -o my-strings --omit-header *.cc *.h 